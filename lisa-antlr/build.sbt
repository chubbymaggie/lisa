name := "lisa-antlr"

version := "0.2.5"

organization := "ch.uzh.ifi.seal"

scalacOptions ++= Seq("-Ywarn-unused-import", "-feature", "-unchecked", "-deprecation", "-Xlint:-adapted-args")

parallelExecution in Test := false

enablePlugins(Antlr4Plugin)

antlr4PackageName in Antlr4 := Some("ch.uzh.ifi.seal.lisa.antlr.parser")

antlr4GenVisitor in Antlr4 := true

antlr4GenListener in Antlr4 := false

libraryDependencies += "org.antlr" % "antlr4" % "4.7.1"

libraryDependencies in assembly ~= { _ map {
  case m if m.organization == "org.antlr" =>
    m.exclude("org.antlr", "antlr4")
  case m => m
}}

resolvers ++= Seq(
  "Maven Central Server" at "http://repo1.maven.org/maven2"
)

