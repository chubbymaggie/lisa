package ch.uzh.ifi.seal.lisa.antlr

import com.signalcollect._
import org.antlr.v4.runtime.tree.RuleNode
import org.antlr.v4.runtime.tree.TerminalNode
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor
import org.antlr.v4.runtime.Lexer
import org.antlr.v4.runtime.{Parser => RuntimeParser}
import org.antlr.v4.runtime.atn.PredictionMode
import java.lang.reflect.ParameterizedType
import sun.reflect.generics.reflectiveObjects.TypeVariableImpl
import java.lang.reflect.Method
import java.lang.reflect.Type
import java.util.{List => JavaList}
import ch.uzh.ifi.seal.lisa.core._
import scala.reflect.ClassTag
import scala.collection.JavaConverters._
import com.typesafe.scalalogging.LazyLogging
import ch.uzh.ifi.seal.lisa.core.computation.FileMeta

/** A generic parser/visitor combo for use with ANTLR generated parsers
  *
  * This class is used to add support for additional languages through ANTLR
  * generated lexers and parsers. It provides a generic visitor that will
  * traverse arbitrary parse trees and generate vertices and edges in the graph
  * based on the provided Domain such that only those vertices which have a
  * mapping in the Domain will be parsed into the graph.
  *
  * @tparam P the ANTLR parser class wrapped by this AntlrParser
  * @param domain the Domain (parse tree) used with this parser
  * @param filter if true, parse only those vertices having a mapping in domain
  * @param parallel if true, parse multiple files in parallel
  */
abstract class AntlrParser[P <: RuntimeParser : ClassTag](val domain: Domain, filter: Boolean = true, parallel: Boolean = true) extends Parser with LazyLogging {
  def lex(input: ANTLRInputStream): Lexer
  def parse(tokens: CommonTokenStream): P
  def enter(parser: P): ParserRuleContext
  def postprocess(state: AnalysisState, vertex: BaseVertex): AnalysisState = state
  val predictionMode = PredictionMode.SLL

  // To accelerate the parsing, all reflection is done once beforehand. There
  // are three caches, one for class -> class.getSimpleName lookups, one for
  // class -> class.getMethods lookups and one for method name ->
  // method.getGenericReturnType lookups
  val typeCache: (Map[Class[_],String],
                  Map[Class[_],Array[Method]],
                  Map[Method,Type]) = {
    // retrieve the parser class
    val parserClass = implicitly[ClassTag[P]].runtimeClass
    // retrieve all the inner (*Context) classes defined by the parser
    val contextClasses = parserClass.getDeclaredClasses()
    // for each of those classes...
    contextClasses.foldLeft((Map[Class[_],String](),
                             Map[Class[_],Array[Method]](),
                             Map[Method,Type]())) { case (acc, ctx) =>
      // store a mapping from the class to its string name
      val contextName = ctx.getSimpleName match {
        case s if s.endsWith("Context") => s.dropRight(7)
        case s => s
      }
      val methods = ctx.getMethods.filter(_.getName != "getTokens")
      (acc._1 + (ctx -> contextName),
      acc._2 + (ctx -> methods),
      // and store the return types of all methods defined by the class
      methods.foldLeft(acc._3) { case (acc, m) =>
        acc + (m -> m.getGenericReturnType)
      })
    }
  }

  import java.nio.ByteBuffer
  override def parse(files: List[(String,ByteBuffer)],
                     graph: GraphEditor[Any, Any], rev: Revision): ParseStats = {
    val inputs = files.map { case (path, codeBytes) =>
      val code = bytesToChars(codeBytes)
      (path, new ANTLRInputStream(code.array, code.length))
    }

    val revisionRange = RevisionRange(rev, rev)
    def parseFile(p: String, input: ANTLRInputStream): (Long, Long, Long) = {
      val path = "/" + p // must match the path used by Git service
      if (!parallel) { logger.debug(s"... parsing $path") }
      val lexer = lex(input)
      lexer.removeErrorListeners()
      val tokens = new CommonTokenStream(lexer)
      val parser = parse(tokens)
      parser.removeErrorListeners()
      // two stage parsing (for speed) as explained in various locations
      // https://github.com/antlr/antlr4/issues/374#issuecomment-30952357
      // http://www.antlr.org/api/JavaTool/org/antlr/v4/runtime/atn/ParserATNSimulator.html
      parser.getInterpreter().setPredictionMode(predictionMode)
      val tree = if (predictionMode == PredictionMode.SLL) {
        try { enter(parser) }
        catch {
          case e: Exception =>
            tokens.seek(0)
            parser.reset()
            parser.getInterpreter().setPredictionMode(PredictionMode.LL)
            enter(parser)
        }
      } else { enter(parser) }
      val visitor = new UniversalVisitor(path, revisionRange, graph, domain, filter, typeCache, postprocess)
      val vertex = new AstVertex(path, 0, revisionRange, path, domain)
      val state = vertex.state.rangeStates(revisionRange)
      vertex.state = vertex.state.copy(
        rangeStates = vertex.state.rangeStates + (revisionRange -> (state + 
             (TypeLabel(false, "META-FILE")) +
             (FileMeta(true, tree.stop.getLine, input.size)))))
      graph.addVertex(vertex)

      // print tree
      // logger.debug(tree.toStringTree(parser))
      val (addCount, ignoreCount) = visitor.visit(tree)
      (tree.stop.getLine, addCount, ignoreCount)
    }

    val stats = inputs.foldLeft((0L,0L,0L)) { case (acc, (p, input)) =>
      val res = parseFile(p, input)
      (acc._1 + res._1, acc._2 + res._2, acc._3 + res._3)
    }

    ParseStats(stats._1, stats._2, stats._3)
  }
}

/** A generic visitor for arbitrary ANTLR generated parse trees
  *
  * Normally, a user would implement visitor methods for each of the context
  * types defined by the parser. Since we need to work with arbitrary tree, only
  * one method, visitChildren, is implemented and called for all context types.
  * The visitor automatically extracts literals using reflection.
  *
  * @param fileName the file name of the file being parsed
  * @param revisionRange the (single-rev) revision range of the file
  * @param graph the graph to be populated
  * @param domain the domain of the newly created vertices
  * @param filter if true, parse only those vertices having a mapping in domain
  * @param typeCache the pre-generated cache used to avoid repeated reflection
  */
class UniversalVisitor(fileName: String, revisionRange: RevisionRange,
                       graph: GraphEditor[Any, Any], domain: Domain,
                       filter: Boolean = true,
                       typeCache: (Map[Class[_],String],
                                   Map[Class[_],Array[Method]],
                                   Map[Method,Type]),
                       postprocess: (AnalysisState, BaseVertex) => AnalysisState)
extends AbstractParseTreeVisitor[(Long,Long)] {

  var parentUri = fileName
  val chainAST = scala.collection.mutable.Stack[String](parentUri)
  var chainGraph = scala.collection.mutable.Stack[String](parentUri)
  val forksAST = scala.collection.mutable.Map[String, Int]()
  val forksGraph = scala.collection.mutable.Map[String, Int]()

  // graphDepth represents the traversal depth of the *filtered* graph. The
  // depth of the AST is retrieved from ctx.getRuleContext.depth
  var depthGraph = 0

  override def visitChildren(ctx: RuleNode): (Long,Long) = {
    val contextClass = ctx.getClass
    // get local vertex type label
    val name = typeCache._1.get(contextClass).get

    val d = ctx.getRuleContext.depth
    while (chainAST.size > d) {
      chainAST.pop
    }

    val parentUriAST = chainAST.lastOption.map {
      l => chainAST.init.foldRight(new StringBuilder(l)) {
        (e, acc) => acc.append("/").append(e)
      }.result
    }.getOrElse("")

    val uriNonIndexedAST = parentUriAST + "/" + name
    val index = forksAST.getOrElse(uriNonIndexedAST, 0)
    forksAST += (uriNonIndexedAST -> (index + 1))
    chainAST.push(name + index)

    val relevantNode = domain.labels.contains(name) || !filter
    if (relevantNode) {
      depthGraph += 1

      while (chainGraph.size > depthGraph) {
        chainGraph.pop
      }

      val parentUriGraph = chainGraph.lastOption.map {
        l => chainGraph.init.foldRight(new StringBuilder(l)) {
          (e, acc) => acc.append("/").append(e)
        }.result
      }.getOrElse("")

      val uriNonIndexedGraph = parentUriGraph + "/" + name
      val index = forksGraph.getOrElse(uriNonIndexedGraph, 0)
      forksGraph += (uriNonIndexedGraph -> (index + 1))
      chainGraph.push(name + index)
 
      val uri = uriNonIndexedGraph + index

      val vertex = new AstVertex(uri, index, revisionRange, fileName, domain)

      // Literals are terminal nodes
      typeCache._2.get(contextClass).get.foreach { m =>
        val returnType = typeCache._3.get(m)
        returnType.get match {
          case t: TypeVariableImpl[_] =>
          // find methods returning List[TerminalNode]
          case t: ParameterizedType =>
            val rawType = t.getRawType
            val typeArguments = t.getActualTypeArguments
            val isList = rawType match {
              case r: Class[_] => classOf[JavaList[_]].isAssignableFrom(r)
              case _ => false
            }
            if (isList) {
              val containsTerminalNodes = typeArguments.headOption match {
                case Some(tpe: TypeVariableImpl[_]) => false
                case Some(tpe) if classOf[TerminalNode].isAssignableFrom(tpe.asInstanceOf[Class[_]]) => true
                case _ => false
              }
              if (containsTerminalNodes) {
                val children = Option(m.invoke(ctx).asInstanceOf[JavaList[TerminalNode]])
                val state = vertex.state.rangeStates(revisionRange)
                children match {
                  case Some(children) =>
                    val literals = children.asScala.zipWithIndex
                        .foldLeft(state[Literal].map) { case (acc, (child, i)) =>
                      acc + (m.getName + i.toString -> child.toString)
                    }
                    vertex.state = vertex.state.copy(
                      rangeStates = vertex.state.rangeStates + (revisionRange ->
                      (state + (Literal(false, literals)))))
                  case None =>
                }
              }
            }
          // find methods returning TerminalNode
          case t if classOf[TerminalNode].isAssignableFrom(t.asInstanceOf[Class[_]]) =>
            if (m.getGenericParameterTypes().size == 0) {
              val result = m.invoke(ctx)
              if (result != null) {
                val state = vertex.state.rangeStates(revisionRange)
                val literals = state[Literal].map
                vertex.state = vertex.state.copy(
                  rangeStates = vertex.state.rangeStates + (revisionRange ->
                  (state + (Literal(false, literals + (m.getName -> result.toString))))))
              }
            }
          case _ =>
        }
      }

      val state = vertex.state.rangeStates(revisionRange)
      vertex.state = vertex.state.copy(
        rangeStates = vertex.state.rangeStates + (revisionRange ->
        (postprocess(state + (TypeLabel(false, name)), vertex) )))
      graph.addVertex(vertex)
      graph.addEdge(uri, new UpwardEdge("HAS-AST_PARENT", parentUriGraph))
      graph.addEdge(parentUriGraph, new DownwardEdge("HAS-AST_CHILD", uri))
    }
    val (addCount, ignoreCount) = super.visitChildren(ctx)
    if (domain.labels.contains(name) || !filter) {
      depthGraph -= 1
    }
    if (relevantNode) { (addCount+1, ignoreCount) }
    else { (addCount, ignoreCount+1) }
  }

  override def defaultResult(): (Long,Long) = { (0, 0) }
  override def aggregateResult(acc: (Long, Long), n: (Long, Long)): (Long,Long) = {
    (acc._1 + n._1, acc._2 + n._2)
  }
}

