package ch.uzh.ifi.seal.lisa

package object antlr {
  type ParseTree = org.antlr.v4.runtime.tree.ParseTree
  type ANTLRInputStream = org.antlr.v4.runtime.ANTLRInputStream
  type CommonTokenStream = org.antlr.v4.runtime.CommonTokenStream
  type Domain = ch.uzh.ifi.seal.lisa.core.Domain
}
