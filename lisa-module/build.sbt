name := "lisa-module"

version := "0.2.5"

parallelExecution in Test := false

test in assembly := {}

libraryDependencies ++= {
  Seq(
    "org.specs2" %% "specs2-core" % "3.9.5" % "test",
    "com.github.tototoshi" %% "scala-csv" % "1.3.5",
    "io.spray" %%  "spray-json" % "1.3.3",
    "com.typesafe" % "config" % "1.3.3"
  )
}

testOptions in Test ++= Seq(
  Tests.Filter(name => !(name contains "Medium")),
  Tests.Filter(name => !(name contains "Large")),
  Tests.Filter(name => !(name contains "Minified")),
  Tests.Filter(name => !(name contains "Readme"))
)

resolvers ++= Seq(
  "Scala-Tools Repository" at "https://oss.sonatype.org/content/groups/scala-tools/",
  "Maven Central Server" at "http://repo1.maven.org/maven2"
)

