#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This parser outputs Python ASTs using the following JSON representation:
#
# "stats": {
#   "lineCount": <total line count of the file>,
#   "nodeAddCount": <number of AST nodes included in the graph>,
#   "nodeIgnoreCount": <number of AST nodes filtered from the graph>
# },
# "nodes": <list of AST nodes>
#
# Every AST node is represented as follows:
#
# {
#   "uri": <unique identifier of the AST node>,
#   "parent_uri": <identifier of the parent AST node (used for edge creation)>
#   "type": <the AST node type as declared by the Python grammar>
#   "name": <the node type plus an index for telling apart siblings>
#   "meta": <data members extracted from the node, as provided by the parser>
# }
#

import sys
import os
import json
import ast
import argparse
import logging
import traceback

logging.basicConfig(stream=sys.stderr, level="INFO")
logger = logging.getLogger('python_parser')

# process arguments
argp = argparse.ArgumentParser()
argp.add_argument('--debug', dest='debug', action='store_true',
                    help="Write verbose debug data to stderr")
argp.add_argument('--types', default="", type=str,
                    help="comma-separated list of relevant node types")
argp.add_argument('--prefix', default="/", type=str,
                    help="AST path prefix (typically the file path)")
argp.set_defaults(debug=False)
args = argp.parse_args()
if args.debug: logger.setLevel("DEBUG")
if args.types: args.types = args.types.split(",")
if args.types: logger.debug(args.types)
if args.prefix: logger.debug(args.prefix)

# replace spurious null bytes
code = sys.stdin.read().replace('\x00', '')

class GenericNodeVisitor(object):
  def __init__(self, root):
    self.root = root
    self.lineCount = 0
    self.addCount = 0
    self.ignoreCount = 0

  def traverse(self):
    self.vertices = []
    self.visit(root, args.prefix + "/", args.prefix, 0)

  def retrieve(self):
    return {
      "stats": {
        "lineCount": self.lineCount,
        "nodeAddCount": self.addCount,
        "nodeIgnoreCount": self.ignoreCount
      },
      "nodes": self.vertices
    }

  def visit(self, node, prefix, parent_uri, sibling, proptype = ""):
    try: self.lineCount = node.lineno
    except: pass
    metadata = {}
    # Store non-AST fields in a dict 
    for x,y in ast.iter_fields(node):
      if isinstance(y, list): pass
      elif isinstance(y, ast.AST): pass
      elif y is None: pass
      else: metadata[type(y).__name__] = y
    # Create URI (id for the graph vertex)
    nodetype = type(node).__name__ + proptype
    relevant = len(args.types) == 0 or nodetype in args.types
    name = "%s%s" % (nodetype, sibling)
    uri = "%s%s/" % (prefix, name)
    node_data = {"uri": uri, "parent_uri": parent_uri, "name": name, "type": nodetype}
    if len(metadata) != 0:
      node_data["meta"] = metadata
    if relevant:
      self.addCount += 1
      self.vertices.append(node_data)
      logger.debug("%s -> %s - %s, %s" % (node_data["parent_uri"], node_data["uri"], node_data["type"], metadata))
    else: self.ignoreCount += 1
    parent = uri if relevant else parent_uri
    for i, (field, value) in enumerate(ast.iter_fields(node)):
      if isinstance(value, list):
        for j, item in enumerate(value):
          if isinstance(item, ast.AST):
            self.visit(item, uri, parent, j, field)
      elif isinstance(value, ast.AST):
        self.visit(value, uri, parent, i)

try:
  root = ast.parse(code, "<string>")
  visitor = GenericNodeVisitor(root)
  visitor.traverse()
  sys.stdout.write(json.dumps(visitor.retrieve()))
except SyntaxError as e:
  logger.error("SyntaxError in %s: %s" % (args.prefix, traceback.format_exc(0)))
except Exception as e:
  logger.error("Uncaught exception: %s" % e)

# vim: set shiftwidth=2 tabstop=2 expandtab:

