function drawNav(svg, revisions, graph) {
  const radius = 70;
  const piePos = [(svg.node().scrollWidth) / 2, (svg.node().scrollHeight) / 2];
  const timeNav = svg.append("g")
    .attr("class", "timeNav")
    .attr("transform", `translate(${piePos[0]},${piePos[1]})`);

  const timeNavTicks = Math.min(11, revisions.length);
  const revScale = d3.scaleLinear().domain([0, timeNavTicks]).rangeRound([0, revisions.length]);
  const circleScale = d3.scaleLinear().domain([0, revisions.length]).range([0, 2*Math.PI]);
  var lastStamp = "";
  for (var i = 1; i < timeNavTicks; i++) {
    const r = revScale(i);
    const stamp = d3.scaleTime().tickFormat(null, "%Y")(new Date(revisions[r].authorDate));
    if (lastStamp == stamp) {
      continue;
    }
    lastStamp = stamp;
    const x = (radius+12) * Math.cos(circleScale(r)-Math.PI/2);
    const y = (radius+12) * Math.sin(circleScale(r)-Math.PI/2);
    timeNav.append("text")
      .attr("class", "timeNavTick")
      .attr("x", x)
      .attr("y", y)
      .text(stamp);
  }

  function updateInfo() {
    d3.selectAll(".rangeSelect").style("background-color", null);
    const revFrom = revisions[graph.db.selectedRange[0]];
    const revFromSha = d3.select("#textRevFromSha");
    const revFromBy = d3.select("#textRevFromBy");
    const revFromDate = d3.select("#textRevFromDate");
    revFromSha.attr("value", `${revFrom.n}: ${revFrom.rev}`);
    revFromSha.property("value", `${revFrom.n}: ${revFrom.rev}`);
    revFromBy.attr("value", revFrom.authorName);
    revFromDate.attr("value", d3.scaleTime().tickFormat(null, timeFormat)(new Date(revFrom.authorDate)));
    const revTo = revisions[graph.db.selectedRange[1]];
    const revToSha = d3.select("#textRevToSha");
    const revToBy = d3.select("#textRevToBy");
    const revToDate = d3.select("#textRevToDate");
    revToSha.attr("value", `${revTo.n}: ${revTo.rev}`);
    revToSha.property("value", `${revTo.n}: ${revTo.rev}`);
    revToBy.attr("value", revTo.authorName);
    revToDate.attr("value", d3.scaleTime().tickFormat(null, timeFormat)(new Date(revTo.authorDate)));
  }
  updateInfo();

  const arcsData = function (d) {
    // arcs for drawing pie sectors
    const arcSizes = revisions.reduce( function (acc, revision) {
      const r = deepcopy(revision)
      r.r = 1
      r.authorDate = new Date(r.authorDate)
      acc.push(r);
      return acc;
    }, []); const pieValue = function (d) { return d.r; }
    const res =  d3.pie().value(pieValue).sortValues(null)(arcSizes);
    res.forEach(function(obj) { obj.nodeData = d; })
    return res;
  }

  const arc = function(d) {
    const res = d3.arc()
      .innerRadius(0)
      .outerRadius(radius)(d);
    return res;
  }

  const arcInner = function(d) {
    const res = d3.arc()
      .innerRadius(0)
      .outerRadius(radius/2)(d);
    return res;
  }

  const navMouse = function (d) {
    var infodata = []
    infodata.push(["SHA", `${d.data.rev.slice(0,9)}`]);
    infodata.push(["Author", `${d.data.authorName}`]);
    infodata.push(["Email", `${d.data.authorEmail}`]);
    infodata.push(["Date", `${d3.scaleTime().tickFormat(null, timeFormat)(new Date(d.data.authorDate))}`]);
    infodata.push(["Subject", `${d.data.subject}`]);
    if (d.data.message && d.data.message.length > 0) {
      infodata.push(["Message", `${d.data.message}`]);
    }
    tooltipShow(infodata);
  }

  timeNav.append("g")
    .attr("class", "pie")
    .selectAll("path")
    .data(arcsData)
    .enter()
    .append("path")
    .attr("class", "pieSector")
    .attr("d", arc)
    .attr("stroke-width", "0.5")
    .on("mouseover", navMouse)
    .on("mousemove", tooltipMove)
    .on("mouseout", tooltipHide);

  timeNav.append("g")
    .attr("class", "pieInner")
    .selectAll("path")
    .data(arcsData)
    .enter()
    .append("path")
    .attr("class", "pieSector")
    .attr("d", arcInner)
    .attr("stroke-width", "0.5")
    .on("mouseover", navMouse)
    .on("mousemove", tooltipMove)
    .on("mouseout", tooltipHide);

  timeNav.append("g")
    .attr("class", "navStartLine")
    .append("path")
    .attr("d", `M 0 0 l 0 -${radius}`)
    .attr("stroke-width", 1)
    .attr("stroke", '#000');

  function colorizeNav() {
    var colorScale = d3.scaleLinear().domain(graph.db.selectedRange).range([1,0.5]);
    const doColorize = function (selector, interpolator) {
      selector
        .attr("stroke", function(d, i) {
          if (i >= graph.db.selectedRange[0] && i <= graph.db.selectedRange[1]) {
            return interpolator(colorScale(i));
          }
          else { return "#777"; }
        })
        .attr("fill", function(d, i) {
          if (i >= graph.db.selectedRange[0]  && i <= graph.db.selectedRange[1]) {
            return interpolator(colorScale(i));
          }
          else { return "#aaa"; }
        })
    }
    doColorize(timeNav.selectAll(".pie .pieSector"), d3.interpolateBlues)
    doColorize(timeNav.selectAll(".pieInner .pieSector"), d3.interpolateGreens)
  }
  colorizeNav();

  const increment = 2*Math.PI / revisions.length;
  var handleRads = [0, Math.PI * 2];
  var refreshInterval = null;
  const handleFrom = timeNav.append("path")
    .attr("id", "handleFrom")
    .attr("d", `M 0 0 l 0 -18 l 15 0 l 0 8 l -15 10`)
    .attr("fill", "#acff90")
    .attr("stroke", "#000")
    .attr("transform", `translate(0 ${-radius})`)
    .on("dblclick", function (d) {
      const a = 0;
      handlePos(a, handleFrom, 0)
      graph.refresh();
    })
    .call(d3.drag().on('drag', function on_drag() {
      const a = roundToIncrement(Math.floor, angle(0, 0, d3.event.x, d3.event.y), increment)
      if (a >= handleRads[1]) return;
      handlePos(a, handleFrom, 0)
      clearInterval(refreshInterval);
      refreshInterval = setInterval(function () { clearInterval(refreshInterval); graph.refresh(); }, 30)
    }))

  const handleTo = timeNav.append("path")
    .attr("id", "handleTo")
    .attr("d", `M 0 0 l 0 -18 l -15 0 l 0 8 l 15 10`)
    .attr("fill", "#ffcd76")
    .attr("stroke", "#000")
    .attr("transform", `translate(0 ${-radius})`)
    .on("dblclick", function (d) {
      const a = 2*Math.PI;
      handlePos(a, handleTo, 1)
      graph.refresh();
    })
    .call(d3.drag().on('drag', function on_drag() {
      const a = roundToIncrement(Math.ceil, angle(0, 0, d3.event.x, d3.event.y), increment)
      if (a <= handleRads[0]) return;
      handlePos(a, handleTo, 1)
      clearInterval(refreshInterval);
      refreshInterval = setInterval(function () { clearInterval(refreshInterval); graph.refresh(); }, 30)
    }))


  function handlePos(a, handle, i) {
    const x = radius * Math.cos(a-Math.PI/2);
    const y = radius * Math.sin(a-Math.PI/2);
    const rotation = fmod((a-Math.PI/2)*180/Math.PI + 90, 360);
    handleRads[i] = a;
    const fromIndex = Math.round((handleRads[0]/(2*Math.PI)) * (revisions.length));
    const toIndex = Math.round((handleRads[1]/(2*Math.PI)) * (revisions.length)) - 1;
    graph.db.selectedRange = [fromIndex, toIndex];
    handle
      .attr("transform", `translate(${x} ${y}) rotate(${rotation}, 0 0)`);
    updateInfo();
    colorizeNav();
  }

  function roundToIncrement(rounding, value, increment){ return (rounding(value / increment) * increment); }
  function fmod(a,b) { return Number((a - (Math.floor(a / b) * b)).toPrecision(8)); }
  function angle(cx, cy, ex, ey) {
    var dy = ey - cy;
    var dx = ex - cx;
    return fmod(((-Math.atan2(dx, dy)) + Math.PI), 2*Math.PI);
  }

  var revSearch = null;
  const revFromText = d3.select("#textRevFromSha");
  const revToText = d3.select("#textRevToSha");
  const findRevCallback = function (element, constraint, handle, i) {
    const term = element.value;
    if (term == "") { return; }
    const matcher = /^([0-9]+)(: ?[a-zA-Z0-9]+| *)$/;
    const match = term.match(matcher);
    var found = -1;
    clearInterval(revSearch);
    revSearch = setInterval(function () {
      if (match && match.length > 0 && !isNaN(match[1])) {
        found = parseInt(match[1]);
      }
      else {
        found = graph.db.revisions.slice(constraint).findIndex(function (d, i) {
          return d.rev.indexOf(term) == 0
        });
      }
      if (found < constraint[0] || found > constraint[1]) {
        d3.select(element).style("background-color", '#ff3b3b');
        found = -1;
      }
      if (found != -1) {
        handlePos(increment*(found+i), handle, i)
        graph.refresh();
      }
      else {
        d3.select(element).style("background-color", '#ff3b3b');
      }
      clearInterval(revSearch);
    }, 500);
  };
  revFromText.on("keyup", function () {
    if (d3.event.key == "Enter") {
      findRevCallback(this, [0, graph.db.selectedRange[1]], handleFrom, 0);
    }
  });
  revToText.on("keyup", function () {
    if (d3.event.key == "Enter") {
      findRevCallback(this, [graph.db.selectedRange[0], graph.db.revisions.length -1], handleTo, 1);
    }
  });

}

function drawClass(selection, revisions, graph) {

  const arcsData = function (d) {
    // arcs for drawing pie sectors
    const arcSizes = d.ranges.map( function (r) {
      const rangeSize = r.end - r.start + 1;
      return {"r": rangeSize, "rev": r.rev, "start": r.start, "end": r.end,
              "startDisplay": r.startDisplay, "endDisplay": r.endDisplay,
              "metrics": r.metrics, "authorName": revisions[r.start].authorName,
              "authorDate": d3.scaleTime().tickFormat(null,
                timeFormat)(new Date(revisions[r.start].authorDate)) };
    }, []);
    const pieValue = function (d) { return d.r; }
    const res =  d3.pie().value(pieValue).sortValues(null)(arcSizes);
    res.forEach(function(obj) { obj.nodeData = d; })
    return res;
  }

  const arc = function(d) {
    const res = d3.arc()
      .innerRadius(0)
      .outerRadius(function (d) { return d.nodeData.nodeSize; })(d);
    return res;
  }

  var pie = selection
    .selectAll("path.pieSector")
    .data(arcsData, function (d) { return `${d.nodeData.name}/${d.data.rev}`;});
  pie
    .exit().remove();
  pie
    .enter()
    .append("svg:path")
    .attr("class", "pieSector")
    .append("title");

  selection
    .selectAll("path.pieSector")
    .attr("d", arc)
    .attr("stroke-width", "0.5")
    .attr("stroke", graph.sectorColor)
    .attr("fill", graph.sectorColor)
    .attr('d', arc)
    .on("mouseover", function (d) {
      const startN = d.data.startDisplay
      const startRev = revisions[startN]
      var infodata = []
      infodata.push(["ID", `${d.nodeData.name}`]);
      infodata.push(["Range", `[${d.data.startDisplay}-${d.data.endDisplay}]`]);
      infodata.push(["Date", `${d3.scaleTime().tickFormat(null, timeFormat)(new Date(startRev.authorDate))}`]);
      if (d.data.rev) {
        infodata.push(["SHA", `${startRev.rev.slice(0,9)}`]);
        infodata.push(["Author", `${startRev.authorName}`]);
        infodata = infodata.concat(Object.entries(d.data.metrics));
      }
      else { infodata.push(["", "node absent at this time"]); }
      if (d.nodeData.methods) {
        infodata.push(["Children", `${d.nodeData.methods.length}`]);
      }
      if (d.data.rev) {
        infodata.push(["Subject", `${startRev.subject}`]);
        if (startRev.message && startRev.message.length > 0) {
          infodata.push(["Message", `${startRev.message}`]);
        }
      }
      tooltipShow(infodata);
    })
    .on("mousemove", tooltipMove)
    .on("mouseout", tooltipHide);

}

