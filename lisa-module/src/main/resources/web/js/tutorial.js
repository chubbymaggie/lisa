function Tutorial (drawing) {
  this.run = function () {
    var clickEvent = new MouseEvent("click", {
      "view": window,
      "bubbles": true,
      "cancelable": false
    });
    var mouseoverEvent = new MouseEvent("mouseover", {
      "view": window,
      "bubbles": true,
      "cancelable": false
    });
    var mouseoutEvent = new MouseEvent("mouseout", {
      "view": window,
      "bubbles": true,
      "cancelable": false
    });
    const intro = introJs()
      .setOptions({
        scrollToElement: false,
        showStepNumbers: false,
        overlayOpacity: 1,
        disableInteraction: false,
        exitOnOverlayClick: false,
        hideNext: true,
        steps: [
          { intro: `<b>Welcome to Evo-Clocks!</b>
                   <br><br>This short tutorial will walk you through controlling and interpreting the visualizations.
                   This is only an exemplary project for getting familiar with the tool.
                   <br><br>After completing the tutorial, you can access to real-world projects.`,
            position: 'right',
          },
          { element: document.querySelector('#nav'),
            intro: `Software evolution is mapped onto the circumference of a circle, starting at the 12 o'clock position going clock-wise.
                    Older commits are represented by darker colors. Hovering the mouse reveals commit metadata.`,
            position: 'right',
            highlightClass: 'pointerTransparent'
          },
          { element: document.querySelector('#nav'),
            intro: `By default, the entire history is shown.
            To restrict the range of revisions being rendered, the <span style="background-color:#acff90;">green</span> and <span style="background-color:#ffcd76;">orange</span> handles can be dragged around the clock.`,
            position: 'right',
          },
          { element: document.querySelector('#navInput'),
            intro: `Sequence numbers or commit hashes can be entered into the <span style="background-color:#acff90;">From</span> and <span style="background-color:#ffcd76;">To</span> fields to set the handles to specific commits.`,
            position: 'right',
          },
          { element: document.querySelector('.classNodes'),
            intro: `The graph (which can be zoomed and panned) represents the histories of different classes and use the same circular representation of time. During times where a class did not exist, the circle is colored gray.
                    <ul>
                    <li>A blue center indicates <span style="background-color:#bee5ff;">concrete</span>, a red center <span style="background-color:#ffc0bc;">abstract</span> classes. <span style="background-color:#f9ff8b;">Interfaces</span> have a yellow center.</li>
                    <li>The number at the center shows the <i>total number of distinct methods</i> that this class had during the displayed history.</li>
                    </ul>`,
            position: 'right'
          },
          { element: document.querySelector('.classNodes'),
            intro: `Clicking on the <i>center</i> of a node will spawn its methods, shaded in green. They remain connected to their owning class via <i>transparent edges</i>.`,
            position: 'right'
          },
          { element: document.querySelector('.classNodes'),
            intro: `There are two additional kinds of edges:
                    <ul>
                    <li><span style="color:#083c7d;">Blue edges</span> indicate inheritance. A solid line <i>extends</i>, a dashed line <i>implements</i>.</li>
                    <li><span style="color:#00c521;">Green edges</span> indicate method calls. </li>
                    </ul>
                    The <i>direction</i> of each edge is indicated by a <i>clock-wise</i> curvature.
                    Incoming and outgoing method calls are aggregated for class nodes that are not expanded.
                    The thickness of method call edges corresponds to the number of method calls between given nodes.
                    `,
            position: 'right'
          },
          { element: document.querySelector('#edgeInput'),
            intro: `You can control wether inheritance and method calls should be drawn always, never, or only when hovering over a node.`,
            position: 'top'
          },
          { element: document.querySelector("#node-org\\/ifi\\/seal\\/example\\/GenericPair"),
            intro: `Some nodes have a small number above them. It shows how many <i>co-evolving nodes</i> are "hiding behind it".`,
            position: 'right'
          },
          { element: document.querySelector('.classNodes'),
            intro: `Clicking on that number shades the small circle with a random color and expands all co-evolving nodes.`,
            position: 'right'
          },
          { element: document.querySelector('#hidingButtons'),
            intro: `All expanded methods and co-evolving nodes can be hidden at once by clicking on these buttons.`,
            position: 'right'
          },
          { element: document.querySelector('.classNodes'),
            intro: `Nodes are grouped by packages, which are visualized using different background colors.`,
            position: 'right'
          },
          { element: document.querySelector('.classNodes'),
            intro: `It is possible to only view a few selected packages. This is done by holding SHIFT and clicking on one or more packages (i.e. the shaded background). When releasing SHIFT...`,
            position: 'right'
          },
          { element: document.querySelector('.classNodes'),
            intro: `...the selected packages are retained, and all others are hidden.`,
            position: 'right'
          },
          { element: document.querySelector('#buttonClearSelectedPackages'),
            intro: `Hidden packages can be restored by push of this button.`,
            position: 'right'
          },
          { element: document.querySelector('#textNodeIdContains'),
            intro: `You can also enter part of a class path to filter which nodes to display.`,
            position: 'right'
          },
          { element: document.querySelector('#coloringInput'),
            intro: `Clock sectors can show more than just commit age. Use these controls to visualize different numerical and categorical metrics.`,
            position: 'top'
          },
          { element: document.querySelector('#projects'),
            intro: `This concludes the tutorial!
                   <br><br>Continue exploring this demo project to get familiar with the tool. Use these links to visit real-world projects or re-run the tutorial.
                   <br><br><b>Tip:</b> It can be impractical to explore the entirety of large projects, like Avro, all at once. Select a number of packages (using SHIFT-click on the background), look at a shorter revision range or use the node ID filter to reduce clutter and improve performance.`,
            position: 'bottom'
          }
        ]
      })
    drawing.simulation.on("tick.intro", function () {
      if (intro._currentStep == undefined || intro._currentStep > 17) {
        intro.exit();
        cleanup();
      }
      else { intro.refresh(); }
    });
    function cleanup() {
      delete intro;
      drawing.simulation.on("tick.intro", null);
    }
    svg.call(d3.zoom().on("zoom", null));
    function methodsShown() {
      return d3.selectAll(`.centerDot.disabled`).node()
    }
    function coevolvingShown() {
      const res = d3.selectAll(`.coDot circle`)
        .filter(function (e) { return e.coevolvedShown; })
        .node();
      return res
    }
    function packageSelected() {
      return drawing.db.selectedPackages.size > 0;
    }
    function interact() {
      if ((intro._currentStep >= 5 && !methodsShown()) ||
          (intro._currentStep < 5 && methodsShown())) {
        const c = d3.selectAll("#center-org\\/ifi\\/seal\\/example\\/Main circle").node();
        if (c) { c.dispatchEvent(clickEvent); }
      }
      if (intro._currentStep >= 6 && intro._currentStep <= 7) {
        d3.selectAll('#center-org\\/ifi\\/seal\\/example\\/Main\\.main\\(\\[Ljava\\/lang\\/String\\;\\)V circle').node().dispatchEvent(mouseoverEvent)
      } else {
        const c = d3.selectAll('#center-org\\/ifi\\/seal\\/example\\/Main\\.main\\(\\[Ljava\\/lang\\/String\\;\\)V circle').node();
        if (c) { c.dispatchEvent(mouseoutEvent); }
      }
      if ((intro._currentStep == 9 && !coevolvingShown()) ||
          (intro._currentStep != 9 && coevolvingShown())) {
        const c = d3.selectAll("#node-org\\/ifi\\/seal\\/example\\/GenericPair .coDot").node()
        d3.select("#buttonToggleCoevolving").property("disabled", coevolvingShown());
        if (c) { c.dispatchEvent(clickEvent); }
      }
      if (intro._currentStep == 9) {
        drawing.initialZoom(0.35);
      }
      else {
        drawing.initialZoom();
      }
      if (intro._currentStep == 12) {
        drawing.db.selectedPackages = new Set();
        drawing.refresh();
        const f = "org/ifi/seal/example/codesmells"
        drawing.db.selectedPackages.add(f);
        d3.selectAll("#cells path")
          .attr("fill", function(d) {
            if (d) {
              if (drawing.db.selectedPackages.has(d.data.group)) {
                return d3.rgb(drawing.voronoiColor(d.data.group)).darker(0.7);
              }
              else {
                return drawing.voronoiColor(d.data.group);
              }
            }
          })
      }
      if ((intro._currentStep < 12 || intro._currentStep > 13) && packageSelected()) {
        drawing.db.selectedPackages = new Set();
        d3.selectAll("#cells path")
          .attr("fill",function(d) { return drawing.voronoiColor(d.data.group); })
        drawing.refresh();
      }
      if (intro._currentStep == 13) {
        const f = "org/ifi/seal/example/codesmells"
        drawing.db.selectedPackages.add(f);
        drawing.refresh();
      }
    }
    intro
      .onbeforechange(interact)
      .onexit(cleanup)
      .start()
  }
}

