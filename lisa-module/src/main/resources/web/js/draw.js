function Drawing (json) {
  const self = this;
  this.svg = svg;
  this.db = new Db(json);
  this.controls = new Controls(self);

  // simulation
  const forceLink = d3.forceLink(self.db.inheritanceSelected).id(function(d) {
    return d.id;
  });
  this.simulation = d3.forceSimulation()
    .nodes(self.db.classesSelected);
  self.simulation
    //.force("manybody", d3.forceManyBody().strength(1))
    .force("collide", d3.forceCollide().strength(0.5).radius( function (d) {
      if (d.isMethod) { return d.nodeSize + 15; }; return 115; }))
    .force("links", forceLink
      .strength( function (d) {
        if (d.count) { return 0.01; }
        if (d.target.isMethod) { return 1; }; return 0.1;
      })
      .distance( function (d) {
        if (d.count) { return 1000; }
        if (d.target.isMethod) { return 10; }; return 50;
      }))
    .force("x", d3.forceX(width / 2).strength(0.005))
    .force("y", d3.forceY(height / 2).strength(0.005))
    //.force("center", d3.forceCenter(width / 2, height / 2))
    .stop();
  var voronoi = d3.voronoi()
    .x(function(d) { return d.x; })
    .y(function(d) { return d.y; })
    .extent([[-width*10, -height*10], [width*10, height*10]]);
  this.voronoiColor = d3.scaleOrdinal(d3.schemePastel1);
  this.coevolvedColor = d3.scaleOrdinal(d3.schemePastel1);

  var biggestInPackage = {}
  const updateBiggest = function () {
    biggestInPackage = self.db.classesSelected.reduce( function (acc, d) {
      if (!acc[d.group] || acc[d.group].nodeSize < d.nodeSize) {
        if (isNaN(d.x)) { d.x = 0; d.y = 0; }
        acc[d.group] = d;
      }
      return acc;
    }, {});
  }
  updateBiggest();

  this.clustering = function (alpha) {
    self.db.classesSelected.forEach(function(d) {
      if (!biggestInPackage.hasOwnProperty(d.group)) { updateBiggest(); }
      const cluster = biggestInPackage[d.group];
      if (cluster === d) { return; }
      if (isNaN(cluster.x) || isNaN(cluster.y)) { cluster.x = cluster.y = 0; }
      if (isNaN(d.x) || isNaN(d.y)) { d.x = d.y = 0; }
      var x = d.x - cluster.x,
          y = d.y - cluster.y,
          l = Math.max(Math.sqrt(x * x + y * y), 1),
          r = d.nodeSize + cluster.nodeSize;
      if (l !== r) {
        l = (l - r) / l * (alpha);
        d.x -= x *= l;
        d.y -= y *= l;
        cluster.x += x;
        cluster.y += y;
      }
    });
  }
  self.simulation.force("cluster", self.clustering);

  // data containers
  const nodeLinks = graph.append("g").attr("class", "nodeLinks");
  const classNodes = graph.append("g").attr("class", "classNodes");
  const callLinks = graph.append("g").attr("class", "callLinks");
  const classNodeCenters = graph.append("g").attr("class", "classNodeCenters");
  const masks = background.append("g").attr("id", "point-masks");
  const cells = background.append("g").attr("id", "cells");

  const groupMaskGradient = svg.select("defs").append('radialGradient')
    .attr("id", "groupMaskGradient");
  groupMaskGradient.append("stop")
      .attr("offset", "80%")
      .attr("stop-color", "#ffffff")
      .attr("stop-opacity", "1");
  groupMaskGradient.append("stop")
      .attr("offset", "100%")
      .attr("stop-color", "#ffffff")
      .attr("stop-opacity", "0");

  // revision range selection tool
  drawNav(nav, self.db.revisions, this);

  this.sectorColorFun = self.controls.defaultColorFun;
  this.sectorColor = function(d, i) {
    if (d.data.metrics.size == 0) { return "#dddddd"; }
    else return self.sectorColorFun(d, i);
  };

  this.refresh = function () {
    self.db.selectRange();
    self.simulation.nodes(self.db.classesSelected);
    forceLink.links(self.db.inheritanceSelected)
    self.controls.drawLegend();
    self.draw();
  }

  // run simulation without rendering to save loading time
  for (var i = 0, n = 2000; i < n; ++i) {
    const percent = (i/n)*100;
    self.simulation.tick();
  }

  var first = true;
  this.draw = function (metric) {
    if (!first) {
      self.simulation.alpha(0.05).restart();
    }

    // masks for voronoi package cells
    const maskPut = masks.selectAll("mask")
      .data(self.simulation.nodes(), function (d) { return d.id; });
    maskPut.exit().remove();
    maskPut
      .enter().append("mask")
      .attr("id", function(d, i) { return `mask-${d.id}`; })
      .append("circle")
      .attr('fill', "url('#groupMaskGradient')")
      .attr('cx', function(d) { return d.x; })
      .attr('cy', function(d) { return d.y; })
      .attr('r', 300);
    const mask = masks.selectAll("mask circle");

    // voronoi package cells
    const cellPut = cells.selectAll("g")
      .data(self.simulation.nodes(), function (d) { return d.id; })
    cellPut.exit().remove();
    const cellPath = cellPut
      .enter().append("g")
      .attr("mask", function(d,i) { return `url("#mask-${d.id}")`; })
      .attr("fill",function(d) { return self.voronoiColor(d.group); })
      .attr("stroke-color",function(d) { return self.voronoiColor(d.group); })
      .attr("opacity", 0.4)
      .attr("class",function(d) { return d.group; })
      .append("path")
      .on("click", function (d) {
        if (d3.event.shiftKey) {
          const selection = d.data.group
          self.db.selectedPackages.add(d.data.group);
          cells.selectAll("#cells path")
            .attr("fill",function(d) {
              if (self.db.selectedPackages.has(d.data.group)) {
                return d3.rgb(self.voronoiColor(d.data.group)).darker(0.7);
              }
              else {
                return self.voronoiColor(d.data.group);
              }
            })
        }
      });
    cellPath
      .append("title")
      .text(function (d) { return `Package: ${d.group}`; });

    const cell = cells.selectAll("path")

    // node links
    var nodeLink = nodeLinks
      .selectAll(".nodeLink")
      .data(self.db.inheritanceSelected, function (d) { return `${d.source}->${d.target}`; });
    nodeLink
      .exit().remove();
    nodeLink
      .enter().append("path")
      .attr("class", "nodeLink")
    nodeLink = d3.selectAll(".nodeLink")
      .attr("stroke-width", function (d) {
        if (d.target.isMethod) { return 20; }
        return 3;
      })
      .style("fill", "none")
      .style("stroke-dasharray", function (d) {
        if (d.relation && d.relation == "IMPLEMENTS") { return "20,3"; }
        return "none";
      })
      .style("stroke", function (d) {
        if (d.target.isMethod) { return d3.interpolateGreens(1); }
        else { return "#083c7d"; }
      })
      .style("visibility", function (d) {
        if (!d.relation) { return "visible"; }
        if (self.controls.showInheritance == "always") return "visible";
        return "hidden";
      })
      .style("opacity", function (d) {
        if (!d.relation) { return 0.1; }
        if (self.controls.showInheritance == "always") return 1;
        return 0;
      });
    nodeLink
      .on("mouseover", function (d) {
        const info = [["Source", d.source.id]]
        if (d.relation) { info.push(["Relation", d.relation]) }
        info.push(["Target", d.target.id])
        tooltipShow(info)
      })
      .on("mousemove", tooltipMove)
      .on("mouseout", tooltipHide);

    // call links
    var callLink = callLinks
      .selectAll(".callLink")
      .data(self.db.callsSelected, function (d) { return `${d.source}->${d.target}`; });
    callLink
      .exit().remove();
    callLink
      .enter().append("path")
      .attr("class", "callLink")
    callLink = d3.selectAll(".callLink")
      .attr("stroke-width", function (d) {
        return Math.min(d.count, 25);
      })
      .style("fill", "none")
      .style("stroke", "#00c521")
      .style("visibility", function (d) {
        if (self.controls.showMethodCalls == "always") return "visible";
        return "hidden";
      })
      .style("opacity", function (d) {
        if (self.controls.showMethodCalls == "always") return 1;
        return 0;
      });
    callLink
      .on("mouseover", function (d) {
        const info = [["Source", d.source.id]]
        if (d.relation) { info.push(["Relation", d.relation]) }
        info.push(["Target", d.target.id])
        info.push(["Call count", d.count])
        tooltipShow(info)
      })
      .on("mousemove", tooltipMove)
      .on("mouseout", tooltipHide);

    // class nodes
    const classNodeUpdate = classNodes
      .selectAll(".classNode")
      .data(self.db.classesSelected, function (d) { return d.id; });
    classNodeUpdate
      .exit().remove();
    const classNodeEnter = classNodeUpdate.enter().append("g")
      .attr("class", "classNode")
      .attr("id", function (d) { return `node-${d.id}` })
      .on("mouseover", function (d) {
        if (d.isMethod) { nodeMouseOver(d); }
      })
      .on("mouseout", function (d) {
        if (d.isMethod) { nodeMouseOut(d); }
      });

    const classNodeCenterUpdate = classNodeCenters
      .selectAll(".classNodeCenter")
      .data(self.db.classesSelected, function (d) { return d.id; });
    classNodeCenterUpdate
      .exit().remove();
    const classNodeCenterEnter = classNodeCenterUpdate.enter().append("g")
      .attr("class", "classNodeCenter");

    // node labels should only show when zooming in
    /*var nodeLabels = classNodeEnter
      .append("text")
      .attr("class", "nodeLabel")
      .style("font-size", "9")
      .attr("text-anchor", "middle")
      .attr("y", function (d) { console.log(d); return d.nodeSize + 10; })
      .text(function (d) { return d.id; })*/

    var coDots = classNodeEnter
      .append("g")
      .attr("class", "coDot")
      .classed("disabled", function (d) {
        if (d.coevolvedWith) { return true; }
        return false;
      });
    coDots
      .append("circle")
      .attr("r", 18)
      .attr("cy", function (d) { return -6-d.nodeSize; })
      .attr("stroke", '#000');
    coDots
      .append("text")
      .attr("dy", function (d) { return -3-d.nodeSize; })
      .text(function (d) {
        if (!d.coevolvedWith && !d.isMethod) { return d.coevolved.length; }
      });

    coDots.on("click", function (d) {
      if (!d.coevolved) { return; }
      if (d.coevolvedExpanded) {
        d.coevolvedExpanded = false;
        self.db.classes = self.db.classes.filter(function (c) {
          if (d.coevolved.indexOf(c) > -1) {
            c.coevolvedShown = false;
            return false
          }
          return true
        });
      }
      else {
        d3.select("#buttonToggleCoevolving")
          .property("disabled", false);
        d.coevolved.forEach(function (c) {
          c.x = d.x;
          c.y = d.y;
          c.vx = d.vx;
          c.vy = d.vy;
          c.coevolvedShown = true;
          self.db.classes.push(c);
        });
        d.coevolvedExpanded = true;
      }
      self.refresh();
    });
    svg.selectAll(".coDot")
      .style("opacity", function (d) {
        if (d.isMethod) { return 0; }
        return (d.coevolvedShown || d.coevolved.length > 0) ? 1 : 0;
      })
    coDots = svg.selectAll(".coDot circle")
      .style("fill", function (d) {
        if (d.isMethod) { return d3.interpolateGreens(1); }
        if (d.coevolvedWith) {
          return self.coevolvedColor(d.coevolvedWith.id)
        }
        else if (!d.coevolvedExpanded) { return "#ffffff"; }
        else {
          return self.coevolvedColor(d.id)
        }
      });

    classNodeEnter.append("g")
      .attr("class", "pie")

    classNode = classNodes.selectAll(".classNode");
    classNode.selectAll(".pie")
      .call(drawClass, self.db.revisions, self)
    classNodes.selectAll(".classNode");

    var startLines = classNodeEnter
      .append("g")
      .attr("class", "startLine")
    startLines
      .append("path")
      .attr("d", function (d) { return `M 0 0 l 0 -${d.nodeSize}`; })
      .attr("stroke-width", 1)
      .attr("stroke", '#000')

    function nodeMouseOver (d) {
      // make node sticky
      d.fx = d.x;
      d.fy = d.y;
      // toggle edges
      if (self.controls.showInheritance == "hover") {
        nodeLinks.selectAll(".nodeLink")
          .filter(function(l){ return ((l.relation == "EXTENDS" || l.relation == "IMPLEMENTS") && l.source.id == d.id); })
          .interrupt()
          .style("opacity", "1")
          .style("visibility", "visible");
      }
      if (self.controls.showMethodCalls == "hover") {
        callLinks.selectAll(".callLink")
          .filter(function(l){ console.log(`${l.relation} ${l.source.id} == ${d.id}`); return (l.relation == "CALLS" && l.source.id == d.id); })
          .interrupt()
          .style("opacity", function (d) { console.log(d); return 1})
          .style("visibility", function (d) { console.log(d); return "visible"});
      }
    }
    function nodeMouseOut (d) {
      // make node non-sticky
      delete d.fx;
      delete d.fy;
      // hide 2nd-level edges
      if (self.controls.showInheritance == "hover") {
        nodeLinks.selectAll(".nodeLink")
          .filter(function(l){ return ((l.relation == "EXTENDS" || l.relation == "IMPLEMENTS") && l.source.id == d.id); })
          .interrupt()
          .transition().delay(2000).duration(1000)
          .style("opacity", 0)
          .transition()
          .delay(2000)
          .style("visibility", "hidden")
      }
      if (self.controls.showMethodCalls == "hover") {
        callLinks.selectAll(".callLink")
          .filter(function(l){ return (l.relation == "CALLS" && l.source.id == d.id); })
          .interrupt()
          .transition().delay(2000).duration(1000)
          .style("opacity", 0)
          .transition()
          .delay(2000)
          .style("visibility", "hidden")
      }
    }

    classNodeCenter = classNodeCenters.selectAll(".classNodeCenter");
    var centerDots = classNodeCenterEnter
      .append("g")
      .attr("class", "centerDot")
      .attr("id", function (d) { return `center-${d.id}` })
      .classed("disabled", function (d) {
        if (d.isMethod) { return true; }
        return false;
      });
    centerDots
      .append("circle")
      .attr("r", function (d) {
        if (d.isMethod) { return 5;}
        return 14
      })
      .attr("data-number", function (d) {
        if (d.hasOwnProperty("methods")) { return d.methods.length; }
      })
      .attr("stroke", '#000')
    centerDots
      .append("text")
      .attr("dy", "5")
      .text(function (d) {
        if (d.hasOwnProperty("methods")) { return d.methods.length; }
      })
    centerDots = classNodeCenter.selectAll(".centerDot circle")
      .style("fill", function (d) {
        if (d.isMethod) { return "#a8ffb7"; }
        if (d.access.indexOf("interface") > -1) { return colorInterface; }
        if (d.access.indexOf("abstract") > -1) { return colorAbstract; }
        return "#bee5ff";
      })
      .on("mouseover", nodeMouseOver)
      .on("mouseout", nodeMouseOut)
      .on("click", function (d) {
        if (d.isMethod) { return; }
        if (d.methods.length == 0) { return; }
        if (d.methodsExpanded) {
          d.methodsExpanded = false;
          self.db.classes = self.db.classes.filter(function (c) {
            if (d.methods.indexOf(c) > -1) {
              c.methodShown = false;
              return false
            }
            return true
          });
          self.db.inheritance = self.db.inheritance.filter(function (c) {
            return !(c.source == d.id || (c.source.id && c.source.id == d.id))
          });
        }
        else {
          d3.select("#buttonToggleMethods")
            .property("disabled", false);
          d.methods.forEach(function (c) {
            c.x = d.x;
            c.y = d.y;
            c.vx = d.vx;
            c.vy = d.vy;
            c.methodShown = true;
            self.db.classes.push(c);
            self.db.inheritance.push({source: d, target: c});
          });
          d.methodsExpanded = true;
        }
        self.refresh();
      });

    function bendyLink (d) {
      const x1 = d.source.x;
      const y1 = d.source.y;
      const x2 = d.target.x;
      const y2 = d.target.y;
      const mpx = (x1 + x2) * 0.5;
      const mpy = (y1 + y2) * 0.5;
      const distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2))
      const theta = Math.atan2(y2 - y1, x2 - x1) - Math.PI / 2;
      const bend = (!d.relation) ? 0 : edgeBend*distance;
      const xc = mpx + bend * Math.cos(theta);
      const yc = mpy + bend * Math.sin(theta);
      return "M" + x1 + " " + y1 + " Q " + xc + " " + yc + " " + x2 + " " + y2;
    }
    function tickActions() {
      var alpha = self.simulation.alpha();
      var nodes = self.simulation.nodes();

      cell.data(voronoi.polygons(nodes)).attr("d", function (d) {
        return d == null ? null : "M" + d.join("L") + "Z";
      });
      classNode.attr("transform", function (d) { return `translate(${d.x},${d.y})`; })
      classNodeCenter.attr("transform", function (d) { return `translate(${d.x},${d.y})`; })
      mask
        .attr('cx', function(d) { return d.x; })
        .attr('cy', function(d) { return d.y; });
      nodeLink
        .attr("d", bendyLink);
      callLink
        .attr("d", bendyLink)
    }
    self.simulation.on("tick", tickActions);
    tickActions();
    if (first) {
      self.initialZoom();
    }
    first = false;
  }

  this.initialZoom = function (scale = 0.5) {
    svg.call(self.zoom).on("dblclick.zoom", null);
    const svgWidth = d3.select("g.graph g.classNodes").node().getBBox().width
    const svgHeight = d3.select("g.graph g.classNodes").node().getBBox().height
    const initialScale = Math.min(scale, height/svgHeight)
    svg.transition().duration(300)
      .call(self.zoom.transform, d3.zoomIdentity.translate(width/2-initialScale*width/2+100, height/2-initialScale*height/2+100).scale(initialScale))
  }
  // zooming
  const zooming = function () {
    svg.select(".graph").attr("transform", d3.event.transform);
  };
  this.zoom = d3.zoom().on("zoom", zooming);

  d3.select("#buttonCCRevisions").on("click")();
}

