function Controls (graph) {
  const self = this;
  this.x = 210;
  this.y = 10;
  this.height = 450;
  this.width = 20;

  const legendDefs = svg.append('defs')
    .append('linearGradient')
    .attr('id', 'legendGrad')
    .attr('x1', '0%')
    .attr('x2', '0%')
    .attr('y1', '0%')
    .attr('y2', '100%');

  const legendLinear = svg.append('rect')
    .attr('x', self.x)
    .attr('y', self.y)
    .attr('width', self.width)
    .attr('height', self.height)
    .attr('fill', 'url(#legendGrad)')
    .style("opacity", 0);

  const legendCategory = svg.append('g')
    .attr("transform", `translate(210,10)`)
    .attr("class", "legendCategory")
    .style("opacity", 0);

  const legendTicks = svg.append('g')
    .attr("transform", `translate(210,10)`)
    .style("opacity", 0);

  const legendName = svg.append('text')
    .attr('x', self.x-1)
    .attr('y', self.y+self.height + 14);
  legendName
    .text("")
    .style("font-size", 10)
    .style("opacity", 0);

  this.defaultColorFun = function(d, i) {
    if (d.nodeData.isMethod) {
      return d3.interpolateGreens(1 - (d.startAngle/(Math.PI*2))/2)
    }
    return d3.interpolateBlues(1 - (d.startAngle/(Math.PI*2))/2)
  };
  this.valueScale = d3.scaleLinear().domain([0, 100]).range([1,0]);

  function updateScale(metricName) {
    if (metricName == undefined) { return; }
    metricMax = Math.max(...graph.db.classesSelected.map(function (c) {
      return Math.max(...c.ranges.map(function (r) {
        const v = r.metrics[metricName];
        if (v == null) { return 0; }
        else return v;
      }));
    }));
    self.linearScale = d3.scaleLinear().domain([0, metricMax]).range([1,0]);
  }
  updateScale("size");

  const drawLinearLegend = function (interpolator, metric) {
    updateScale(metric);
    legendCategory.style("opacity", 0);
    legendTicks.style("opacity", 1);
    legendName.style("opacity", 1);
    legendDefs.selectAll("stop")
      .data([...Array(101).keys()])
      .enter().append('stop')
      .attr('class', "legendStop")
      .attr('offset', function (d) { return d + '%'; });
    legendDefs.selectAll("stop")
      .attr('stop-color', function (d) { return interpolator(d/100); })
      .attr('stop-opacity', 0.9);
    legendLinear.style("opacity", 1);
    const legendTickScale = d3.scaleLinear().domain([0, metricMax]).range([self.height-1, 0])
    const axis = d3.axisRight(legendTickScale)
      .tickFormat(function(d) {
        if (d % 1 == 0) { return d } else { return "" }
      });
    legendTicks.select(".legendAxis").remove();
    legendTicks.append("g")
      .attr("transform", "translate(21,0)")
      .attr('class', "legendAxis")
      .call(axis);
  }

  const drawCategoryLegend = function (data, interpolator) {
    legendLinear.style("opacity", 0);
    legendTicks.style("opacity", 1);
    legendName.style("opacity", 1);
    const increment = self.height / data.length;
    const update = legendCategory.selectAll("rect")
      .data(data, function (d) { return d; });
    update.exit().remove();
    update.enter().append("rect")
    legendCategory.selectAll("rect")
      .attr("x", 0)
      .attr("y", function (d, i) { return increment*i; })
      .attr("width", self.width)
      .attr("height", function (d, i) { return increment; })
      .attr("fill", function (d, i) { return interpolator(d); })
      .attr("title", function (d, i) { return d; });
    legendCategory.style("opacity", 1);
    const legendTickScale = d3.scaleOrdinal().domain(data).range(data.map(function (d, i) {
      return increment * i + increment / 2.;
    }));
    const axis = d3.axisRight(legendTickScale)
      .tickSize(0);
    legendTicks.select(".legendAxis").remove();
    if (data.length == 0) { legendName.style("opacity", 0); return; }
    legendTicks.append("g")
      .attr("transform", "translate(21,0)")
      .attr('class', "legendAxis")
      .call(axis);
    legendTicks.select(".domain").remove();

  }

  this.drawLegend = function () {};

  const buttonBind = function (buttonId, colorfun, metric, legend) {
    const button = d3.select(buttonId);
    button.on("click", function () {
      if (metric != undefined) {
        updateScale(metric);
        legendName.text(metric.toUpperCase());
      }
      // color nodes
      d3.selectAll(".control").classed("active", false);
      d3.selectAll("#textCCAuthors").property("value", "");
      button.classed("active", true);
      graph.sectorColorFun = colorfun;
      d3.selectAll(".classNode .pieSector")
        .attr("stroke", graph.sectorColor)
        .attr("fill", graph.sectorColor);
      // draw legend
      if (legend == undefined) {
        self.drawLegend = function () {};
        legendLinear.style("opacity", 0);
        legendCategory.style("opacity", 0);
        legendTicks.style("opacity", 0);
        legendName.style("opacity", 0);
      }
      else { self.drawLegend = legend; }
      self.drawLegend();
    });
  };

  buttonBind("#buttonCCRevisions", self.defaultColorFun);

  this.authorScale = d3.scaleOrdinal()
      .domain(graph.db.authorsSelected)
      .range(ciecam02Colors);
  buttonBind("#buttonCCAuthors", function(d, i) {
    return self.authorScale(graph.db.revisions[d.data.startDisplay].authorName);
  }, undefined, function() {
    legendName.text("Authors");
    drawCategoryLegend(graph.db.authorsSelected, self.authorScale);
  });

  buttonBind("#buttonCCSize", function(d, i) {
    return d3.interpolateRdYlGn(self.linearScale(d.data.metrics.size));
  }, "size", function() { 
    drawLinearLegend(d3.interpolateRdYlGn, "size")
  });

  const ckMetricNames = ["CKWMC", "CKDIT", "CKNOC", "CKCBO", "CKRFC", "CKLCOM"];
  ckMetricNames.forEach(function (name) {
    buttonBind(`#button${name}`, function(d, i) {
        if (d.data.metrics[name] != undefined) {
          return d3.interpolateRdYlGn(self.linearScale(d.data.metrics[name]));
        }
        else { return "#aaaaaa"; }
      }, name, function() {
        drawLinearLegend(d3.interpolateRdYlGn, name)
      })
  });

  var authorSearch = null;
  const textCCAuthors = d3.select("#textCCAuthors");
  textCCAuthors.on("keyup", function () {
    d3.selectAll(".control").classed("active", false);
    textCCAuthors.classed("active", true);
    const term = this.value.toLowerCase();
    clearInterval(authorSearch);
    authorSearch = setInterval(function () {
      graph.sectorColorFun = function (d, i) {
        if (d.data.metrics && d.data.metrics.size != 0 &&
            d.data.authorName.toLowerCase().includes(term)) {
          return self.authorScale(graph.db.revisions[d.data.startDisplay].authorName)
        }
        else if (d.data.metrics && d.data.metrics.size > 0) { return "#aaaaaa"; }
        else { return "#dddddd"; }
      };
      d3.selectAll(".classNode .pieSector")
        .attr("stroke", graph.sectorColor)
        .attr("fill", graph.sectorColor);
      legendName.text("Authors");
      self.drawLegend = function () {
        drawCategoryLegend(graph.db.authorsSelected.filter(function (d) {
          return d.toLowerCase().includes(term)
        }), self.authorScale);
      };
      self.drawLegend();
      clearInterval(authorSearch);
    }, 500);
  });

  var idSearch = null;
  const textNodeIdContains = d3.select("#textNodeIdContains");
  textNodeIdContains.on("keyup", function () {
    clearInterval(idSearch);
    d3.selectAll(".control").classed("active", false);
    textNodeIdContains.classed("active", true);
    graph.db.nodeIdFilter = this.value;
    idSearch = setInterval(function () {
      graph.refresh();
      clearInterval(idSearch);
    }, 500);
  });
  d3.selectAll(".control").classed("active", false);

  d3.select(window).on("keydown", function (d) {
    if (d3.event.key == "Shift" && graph.db.selectedPackages.size > 0) { ;
      graph.db.selectedPackages = new Set();
    }
  });
  d3.select(window).on("keyup", function (d) {
    if (d3.event.key == "Shift" && graph.db.selectedPackages.size > 0) { ;
      d3.select("#buttonClearSelectedPackages")
        .text(`Restore ${graph.db.allPackages.size - graph.db.selectedPackages.size} packages`)
        .property("disabled", false);
      d3.selectAll("#cells path")
        .attr("fill",function(d) { return graph.voronoiColor(d.data.group); })
      graph.refresh();
    }
  })
  d3.select("#buttonClearSelectedPackages")
    .on("click", function () {
      graph.db.selectedPackages = new Set();
      graph.refresh();
      d3.select("#buttonClearSelectedPackages")
        .text(`Restore hidden packages`)
        .property("disabled", true);
    });
  d3.select("#buttonToggleCoevolving")
    .on("click", function () {
      d3.select(this)
        .property("disabled", true);
      graph.db.classes = graph.db.classes.filter(function (d) {
        if (d.coevolved) {
          d.coevolvedExpanded = false;
          return true;
        }
        if (d.coevolvedShown) {
          d.coevolvedShown = false;
          return false;
        }
        if (d.isMethod) {
          if (d.owner.coevolvedWith) {
            d.methodShown = false;
            d.owner.methodsShown = false;
            return false;
          }
          else return d.methodShown;
        }
        return false;
      });
      graph.refresh();
    });
  d3.select("#buttonToggleMethods")
    .on("click", function () {
      d3.select(this)
        .property("disabled", true);
      graph.db.classes = graph.db.classes.filter(function (d) {
        if (d.coevolvedExpanded) { d.coevolvedExpanded = false; }
        if (d.coevolved) {
          d.coevolvedExpanded = false;
          return true;
        }
        if (d.methodsExpanded) { d.methodsExpanded = false; }
        if (d.methodShown) { d.methodShown = false; }
        if (d.methods) { return true; }
        return false;
      });
      graph.refresh();
    });
  this.showInheritance = "always";
  this.showMethodCalls = "hover";
  d3.select("#inheritanceEdges")
    .property('value', self.showInheritance)
    .on("change", function () {
      self.showInheritance = d3.select(this).property('value')
      graph.refresh();
    });
  d3.select("#callEdges")
    .property('value', self.showMethodCalls)
    .on("change", function () {
      self.showMethodCalls = d3.select(this).property('value')
      graph.refresh();
    });
}

