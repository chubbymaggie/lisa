package ch.uzh.ifi.seal.lisa.module.persistence

import java.io.File
import com.github.tototoshi.csv._
import ch.uzh.ifi.seal.lisa.core.misc.RuntimeStats

trait CSVStats extends RuntimeStats {
  def storeStats(targetPath: String)(implicit uid: String) {
    var writer = CSVWriter.open(new File(targetPath))
    stats.toSeq.sortBy(_._1).foreach {
      case (k: String, v: List[Any]) => writer.writeRow(k :: v)
      case s => writer.writeRow(Seq(s._1, s._2))
    }
  }
}

