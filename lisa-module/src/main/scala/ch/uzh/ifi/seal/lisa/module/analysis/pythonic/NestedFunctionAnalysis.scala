package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** Detects functions containing nested functions */
object NestedFunctionAnalysis extends Analysis with ChildCountHelper {

  case class NestedFunction(persist: Boolean, hasNested: Boolean) extends Data {
    def this() = this(false, false)
  }

  override def start = { implicit domain => state =>
    if (state is 'method) state ! new NestedFunctionPacket else state
  }

  class NestedFunctionPacket extends AnalysisPacket {

    override def collect = { implicit domain => state =>
      if (state is 'method) {
        state + NestedFunction(false, true)
      } else state ! this
    }
  }
}


/** Counts functions containing nested functions */
object NestedFunctionCountAnalysis extends Analysis with ChildCountHelper {

  case class NestedFunctionCount(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 0)
  }

  override def start = { implicit domain => state =>
    val n = if (state is 'method) 1 else 0
    if (leaf(state)) state ! new NestedFunctionCountPacket(n) else state
  }

  class NestedFunctionCountPacket(n: Int) extends AnalysisPacket {

    import NestedFunctionAnalysis.NestedFunction

    override def collect = { implicit domain => state =>
      val persist = state is ('class, 'method, 'file)
      val count = state[NestedFunctionCount].n + n
      val newState = state + NestedFunctionCount(persist, count)

      allChildren[NestedFunctionCount](state)(
        incomplete = newState,
        complete = {
          val res = if (state[NestedFunction].hasNested) count + 1 else count
          newState ! new NestedFunctionCountPacket(res)
        }
      )

    }
  }
}


