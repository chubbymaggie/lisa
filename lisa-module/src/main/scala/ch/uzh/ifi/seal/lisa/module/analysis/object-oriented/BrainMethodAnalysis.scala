package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._
import MccAnalysis.Mcc
import VertexCountAnalysis.VertexCount
import ControlNestingAnalysis.ControlNesting
import VariableCountAnalysis.VariableCount
import ch.uzh.ifi.seal.lisa.module.mapping.BrainMethodThresholds

/** Detects Methods suffering from the "Brain Method" code smell */
object BrainMethodAnalysis extends Analysis {
  case class BrainMethod(persist: Boolean, b: Boolean) extends Data {
    def this() = this(false, false)
  }

  override def start = { implicit domain => state =>
    if (state.is('method)) {
      val mcc = state[Mcc].n.toDouble
      val vertexCount = state[VertexCount].n
      val controlNesting = state[ControlNesting].n
      val variableCount = state[VariableCount].n
      val mccPerVertex = mcc / vertexCount
      val b = if (
        vertexCount       > BrainMethodThresholds.vertexCount
        && mccPerVertex   > BrainMethodThresholds.mccPerVertex
        && controlNesting > BrainMethodThresholds.controlNesting
        && variableCount  > BrainMethodThresholds.variableCount
        ) { true } else { false }
      state + BrainMethod(true, b)
    }
    else { state }
  }
}
