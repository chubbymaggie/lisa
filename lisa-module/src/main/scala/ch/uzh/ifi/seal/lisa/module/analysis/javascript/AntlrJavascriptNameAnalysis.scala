package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavascriptParseTree

/** Copies part of the Identifier into a Name for Javascript Program nodes */
object AntlrJavascriptNameAnalysis extends Analysis {
  import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name

  override def start = { implicit domain => state =>
    if ((domain is AntlrJavascriptParseTree)) {
      if (state is 'parameter) {
        state + Name(true, state[Literal].map.getOrElse("Identifier", "unknown"))
      }
      else state
    }
    else { state }
  }

}

/** Transmits the identifier of var expression to its anon method */
object AntlrJavascriptAnonFunNameAnalysis extends Analysis {
  import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name

  override def start = { implicit domain => state =>
    if ((domain is AntlrJavascriptParseTree) && (state is 'identifier)) {
      val identifier = state[Literal].map.get("Identifier")
      if (identifier.nonEmpty) {
        state ! new IdentifierPacket(identifier.get)
      } else state
    }
    else state
  }

  class IdentifierPacket(identifier: String) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state is 'variable) {
        // forward signal downwards
        state ↓ this
      }
      else if (state is 'anonMethod) {
        state + Name(true, identifier)
      }
      else state
    }
  }

}

