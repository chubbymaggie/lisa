package ch.uzh.ifi.seal.lisa.module.parser

import com.typesafe.scalalogging.LazyLogging
import com.signalcollect.GraphEditor

import java.lang.reflect.Modifier
import java.nio.ByteBuffer
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.FieldVisitor
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Label

import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name
import ch.uzh.ifi.seal.lisa.module.analysis.AccessModifiers
import ch.uzh.ifi.seal.lisa.core.computation.FileMeta
import ch.uzh.ifi.seal.lisa.core.computation.Revision

// TODO: come up with a similar mapping for edge types
object JavaASMGraph extends Domain {
  override val mapping = Map(
    'class       -> Set("Class"),
    'interface   -> Set("Interface"), // TODO implement interface connections
    'method      -> Set("Method"),
    'methodCall  -> Set("MethodCall"),
    'field       -> Set("Field"),
    'fieldAccess -> Set("FieldAccess")
  )
}

object JavaASMParser extends Parser with LazyLogging {
  override val suffixes = List(".class")
  val domain = JavaASMGraph

  override def parse(files: List[(String,ByteBuffer)],
                     graph: GraphEditor[Any, Any], rev: Revision): ParseStats = {
    val revisionRange = RevisionRange(rev, rev)

    val bytecount = files.foldLeft(0) { case (acc, file) =>
      val bytes = file._2.array
      val cr = new ClassReader(bytes)
      val cw = new ClassWriter(0)
      val cc = new ClassExtractor(cw, graph, revisionRange, bytes.size)
      cr.accept(cc, 0)
      acc + bytes.size
    }

    ParseStats(bytecount,0,0)
  }

  override def postprocess(graph: GraphEditor[Any, Any], v: BaseVertex) = {
    v.outgoingEdges.values.foreach {
      case edge: InterObjectEdge =>
        if (edge.relation == "HAS_SUPER") {
          graph.addEdge(edge.targetId, new InterObjectEdge(s"HAS_SUB", edge.sourceId.asInstanceOf[String]))
        }
        else if (edge.relation == "HAS_TYPE") {
          graph.addEdge(edge.targetId, new InterObjectEdge(s"TYPE_OF_VARIABLE", edge.sourceId.asInstanceOf[String]))
        }
        else if (edge.relation == "ACCESSES") {
          graph.addEdge(edge.targetId, new InterObjectEdge(s"ACCESSED_BY", edge.sourceId.asInstanceOf[String]))
        }
        else if (edge.relation == "CALLS") {
          graph.addEdge(edge.targetId, new InterObjectEdge(s"CALLED_BY", edge.sourceId.asInstanceOf[String]))
        }
        else if (edge.relation == "IMPLEMENTS") {
          graph.addEdge(edge.targetId, new InterObjectEdge(s"IMPLEMENTED_BY", edge.sourceId.asInstanceOf[String]))
        }
      case _ =>
    }
  }
}

class ClassExtractor(cv: ClassVisitor, graph: GraphEditor[Any, Any], revisionRange: RevisionRange, bytecount: Int) extends ClassVisitor(Opcodes.ASM7, cv) {
  // TODO: Implement 'throws' exception connections
  var classPath = ""
  var methodPath = ""

  private def accessToModifiers(access: Int): List[String] = {
    var mods = scala.collection.mutable.ListBuffer.empty[String]
    if (Modifier.isAbstract(access)) { mods += "abstract" }
    if (Modifier.isInterface(access)) { mods += "interface" }
    if (Modifier.isPublic(access)) { mods += "public" }
    return mods.toList
  }

  override def visit(version: Int, access: Int, name: String, signature: String, superName: String, interfaces: Array[String]) {
    classPath = name
    val vertex = new ObjectVertex("class", classPath, 0, revisionRange, classPath, JavaASMGraph)
    val state = vertex.state.rangeStates(revisionRange)
    val modifiers = accessToModifiers(access)
    vertex.state = vertex.state.copy(
      rangeStates = vertex.state.rangeStates + (revisionRange -> (state +
            (TypeLabel(false, "Class")) +
            (AccessModifiers(false, modifiers)) +
            // this causes a range split even if all other stats are the same - consider dropping it?
            (FileMeta(true, bytecount, bytecount)) +
            (Name(true, name))
    )))
    graph.addVertex(vertex)
    graph.addEdge(classPath, new InterObjectEdge(s"HAS_SUPER", superName))
    interfaces.foreach { i =>
      if (modifiers.contains("interface")) {
        graph.addEdge(classPath, new InterObjectEdge(s"HAS_SUPER", i))
      }
      else {
        graph.addEdge(classPath, new InterObjectEdge(s"IMPLEMENTS", i))
      }
    }
  }

  override def visitField(access: Int, name: String, desc: String, signature: String, value: Object): FieldVisitor = {
    val fieldPath = classPath + "." + name + desc
    val vertex = new ObjectVertex("field", fieldPath, 0, revisionRange, fieldPath, JavaASMGraph)
    val state = vertex.state.rangeStates(revisionRange)
    vertex.state = vertex.state.copy(
      rangeStates = vertex.state.rangeStates + (revisionRange -> (state +
            (TypeLabel(false, "Field")) +
            (Name(true, fieldPath)) +
            (Literal(true, Map("name" -> name, "desc" -> desc))))))
    graph.addVertex(vertex)
    graph.addEdge(classPath, new DownwardEdge(s"HAS_FIELD", fieldPath))
    graph.addEdge(fieldPath, new UpwardEdge(s"BELONGS_TO", classPath))
    return cv.visitField(access, name, desc, signature, value)
  }

  override def visitMethod(access: Int, name: String, desc: String, signature: String, exceptions: Array[String]): MethodVisitor = {
    methodPath = classPath + "." + name + desc
    val vertex = new ObjectVertex("method", methodPath, 0, revisionRange, methodPath, JavaASMGraph)
    val state = vertex.state.rangeStates(revisionRange)
    vertex.state = vertex.state.copy(
      rangeStates = vertex.state.rangeStates + (revisionRange -> (state +
            (TypeLabel(false, "Method")) +
            (Name(true, methodPath)) +
            (Literal(true, Map("name" -> name, "desc" -> desc))))))
    graph.addVertex(vertex)
    graph.addEdge(classPath, new DownwardEdge(s"HAS_METHOD", methodPath))
    graph.addEdge(methodPath, new UpwardEdge(s"BELONGS_TO", classPath))
    val mv = cv.visitMethod(access, name, desc, signature, exceptions)
    new MethodExtractor(mv, graph, methodPath)
  }

  class MethodExtractor(mv: MethodVisitor, graph: GraphEditor[Any, Any], methodPath: String) extends MethodVisitor(Opcodes.ASM7, mv) {
    override def visitMethodInsn(opcode: Int, owner: String, name: String, desc: String, itf: Boolean) {
      val targetPath = owner + "." + name + desc
      val Pattern = ".*/[a-zA-Z0-9]*\\$[^\\/]*\\..*".r
      if (Pattern.findFirstIn(targetPath).nonEmpty) return;
      val callPath = methodPath + "->" + targetPath
      val vertex = new ObjectVertex("methodCall", callPath, 0, revisionRange, callPath, JavaASMGraph)
      val state = vertex.state.rangeStates(revisionRange)
      vertex.state = vertex.state.copy(
        rangeStates = vertex.state.rangeStates + (revisionRange -> (state +
              (TypeLabel(false, "MethodCall")) +
              (Name(true, callPath)))))
      graph.addVertex(vertex)
      graph.addEdge(methodPath, new DownwardEdge(s"HAS_CALL", callPath))
      graph.addEdge(callPath, new UpwardEdge(s"BELONGS_TO", methodPath))
      graph.addEdge(callPath, new InterObjectEdge(s"CALLS", targetPath))
    }
    override def visitFieldInsn(opcode: Int, owner: String, name: String, desc: String) {
      val targetPath = owner + "." + name + desc
      val accessPath = methodPath + "->" + targetPath
      val vertex = new ObjectVertex("fieldAccess", accessPath, 0, revisionRange, accessPath, JavaASMGraph)
      val state = vertex.state.rangeStates(revisionRange)
      vertex.state = vertex.state.copy(
        rangeStates = vertex.state.rangeStates + (revisionRange -> (state +
              (TypeLabel(false, "FieldAccess")) +
              (Literal(true, Map("name" -> name, "desc" -> desc))))))
      graph.addVertex(vertex)
      graph.addEdge(methodPath, new DownwardEdge(s"HAS_ACCESS", accessPath))
      graph.addEdge(accessPath, new UpwardEdge(s"BELONGS_TO", methodPath))
      graph.addEdge(accessPath, new InterObjectEdge(s"ACCESSES", targetPath))
    }
    override def visitLocalVariable(name: String, desc: String, signature: String, start: Label, end: Label, index: Int) {
      if (name == "this") { return }
      val variableType = if (desc.startsWith("L")) { desc.drop(1).dropRight(1) } else desc
      val variablePath = classPath + "." + name + desc
      val vertex = new ObjectVertex("variable", variablePath, 0, revisionRange, variablePath, JavaASMGraph)
      val state = vertex.state.rangeStates(revisionRange)
      vertex.state = vertex.state.copy(
        rangeStates = vertex.state.rangeStates + (revisionRange -> (state +
              (TypeLabel(false, "Variable")) +
              (Name(true, variablePath)) +
              (Literal(true, Map("name" -> name, "desc" -> desc))))))
      graph.addVertex(vertex)
      graph.addEdge(methodPath, new ObjectEdge(s"HAS_VARIABLE", variablePath))
      graph.addEdge(variablePath, new UpwardEdge(s"BELONGS_TO", methodPath))
      graph.addEdge(variablePath, new InterObjectEdge(s"HAS_TYPE", variableType))
    }

    // this override appears to be necessary to avoid IllegalStateExceptions
    override def visitFrame(t: Int, nLocal: Int, local: Array[Object], nStack: Int, stack: Array[Object]) {
    }
  }

}

