package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** An analysis which computes McCabe's cyclomatic complexity of any vertex
  *
  * The McCabes complexity is calculated simply by counting all branching
  * statements + 1. This is not correct in the strictest sense of the original
  * conception of cyclomatic complexity or even McCabe's complexity (which can
  * only be computed this way on the machine code level) but this is what most
  * modern tools do.
  */
object MccAnalysis extends Analysis with ChildCountHelper {

  case class Mcc(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 1)
  }

  override def start = { implicit domain => state =>
    // start on any leaf vertex
    if (leaf(state)) state ! new MccPacket(if (state is 'fork) 2 else 1)
    else state
  }

  class MccPacket(val count: Int) extends AnalysisPacket {
    override def collect = { implicit domain => state =>

      val mcc = state[Mcc].n + count - 1

      allChildren[Mcc](state)(
        incomplete = state + Mcc(false, mcc),
        complete = {
          val newCount = if (state is 'fork) mcc + 1 else mcc
          val persist = state is ('class, 'method, 'file)
          state + Mcc(persist, mcc) ! new MccPacket(newCount)
        }
      )

    }
  }
}

