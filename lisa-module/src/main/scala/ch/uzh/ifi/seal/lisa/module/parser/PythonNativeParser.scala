package ch.uzh.ifi.seal.lisa.module.parser

import java.nio.ByteBuffer
import com.typesafe.scalalogging.LazyLogging
import scala.io.Source
import com.signalcollect.GraphEditor
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.core.computation.Revision
import ch.uzh.ifi.seal.lisa.core.computation.FileMeta
import ch.uzh.ifi.seal.lisa.core.public.Parser
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name
import scala.sys.process.Process
import scala.sys.process.ProcessIO
import spray.json._

object PythonNativeParseTree extends Domain {
  override val mapping = Map(
    'class     -> Set("ClassDefbody"),
    'method    -> Set("FunctionDefbody"),
    'name      -> Set("Name"),
    'file      -> Set("META-FILE"),
    'idiomatic -> Set("ListComp", "GeneratorExpargs", "Lambdaargs", "Yield",
                      "Exprfinalbody", "Withbody", "Namedecorator_list",
                      "DictCompargs"),
    'decorator -> Set("Namedecorator_list"),
    'fork      -> Set("Whilebody", "Forbody", "Ifbody", "Trybody", "Withbody",
                      "ListComp", "SetComp", "BoolOp", "IfExp"),
    'block     -> Set("Whilebody", "Forbody", "Ifbody", "Trybody", "Withbody",
                      "ClassDefbody", "FunctionDefbody"),
    'variable  -> Set("Assignbody", "AugAssignbody"),
    'parameter -> Set("argargs")
  )
}


/* Parser for parsing python 2/3 code using python's own native parser
 *
 * This object maintains a pool of parsers, one for each file name. If a new
 * filename is encountered, a parser is added to the pool. The code for each
 * file is then sent to the parser through a socket. The python parser then
 * uses a SocketGraphEditor to add the resulting nodes to the graph.
 */
object PythonNativeParser extends Parser with LazyLogging {
  override val suffixes = List(".py")
  val domain = PythonNativeParseTree

  final val scriptFile = getClass.getResourceAsStream("/python_parser.py")
  final val script = scala.io.Source.fromInputStream(scriptFile).mkString

  def getString(f: String)(implicit fields: Map[String, JsValue]): String = {
    fields.get(f).get.asInstanceOf[JsString].value
  }
  def getNumber(f: String)(implicit fields: Map[String, JsValue]): BigDecimal = {
    fields.get(f).get.asInstanceOf[JsNumber].value
  }

  def getMeta(implicit fields: Map[String, JsValue]): Map[String,String] = {
    fields.get("meta") match {
      case Some(v) => {
        v.asInstanceOf[JsObject].fields.mapValues {
          case v: JsString => v.value
          case v => v.toString
        }
      }
      case None => Map[String,String]()
    }
  }

  override def parse(files: List[(String,ByteBuffer)],
                     graph: GraphEditor[Any, Any],
                     rev: Revision): ParseStats = {
    val revisionRange = RevisionRange(rev, rev)
    var totalLines: Long = 0
    val stats = files.foldLeft((0L,0L,0L)){ case (acc, (filename, codeBytes)) =>
      val rootId = s"/${filename}"
      val jsonString = callPython(codeBytes.array, rootId)
      var res = (0L,0L,0L)
      try {
        val data = jsonString.parseJson.asInstanceOf[JsObject].fields

        val stats = data.get("stats").get.asInstanceOf[JsObject].fields
        res = (stats.get("lineCount").get.asInstanceOf[JsNumber].value.toLong,
               stats.get("nodeAddCount").get.asInstanceOf[JsNumber].value.toLong,
               stats.get("nodeIgnoreCount").get.asInstanceOf[JsNumber].value.toLong)

        val nodes = data.get("nodes").get.asInstanceOf[JsArray].elements
        val root = new AstVertex(rootId, 0, revisionRange, filename, domain)
        val rootState = root.state.rangeStates(revisionRange) +
                         (TypeLabel(false, "META-FILE")) +
                         (FileMeta(true, res._1.toInt, codeBytes.capacity)) +
                         (Name(true, filename))
        root.state = root.state.copy(
          rangeStates = root.state.rangeStates + (revisionRange -> rootState))
        graph.addVertex(root)
        nodes.foreach { t =>
          // gather vertex data
          implicit val fields = t.asInstanceOf[JsObject].fields
          val parent = getString("parent_uri")
          val id = getString("uri")
          val nodeType = getString("type")
          // create vertex
          val vertex = new AstVertex(id, 0, revisionRange, filename, domain)
          val stateBasic = vertex.state.rangeStates(revisionRange) +
                           (TypeLabel(false, nodeType))
          val meta = getMeta
          val state = getMeta.size match {
            case 0 => stateBasic
            case _ => stateBasic + Literal(false, meta)
          }
          vertex.state = vertex.state.copy(
            rangeStates = vertex.state.rangeStates + (revisionRange -> state))
          graph.addVertex(vertex)
          graph.addEdge(id, new UpwardEdge("HAS-AST_PARENT", parent))
          graph.addEdge(parent, new DownwardEdge("HAS-AST_CHILD", id))
        }
      }
      catch {
        case e: JsonParser$ParsingException => {
          logger.error("JsonParser$ParsingException")
        }
        case e: Throwable => logger.error(s"Untreated exception: ${e.getMessage}")
      }
      (acc._1 + res._1, acc._2 + res._2, acc._3 + res._3)
    }
    ParseStats(stats._1, stats._2, stats._3)
  }

  def callPython(code: Array[Byte], prefix: String,
                 python: String = "python3"): String = {
    val proc = Process(Seq(python, "-c", script, "--prefix", prefix,
                                   "--types", domain.labels.mkString(",")))
    var result = ""
    var err = ""
    val io = new ProcessIO(
      stdin  => { stdin.write(code); stdin.close() },
      stdout => result = Source.fromInputStream(stdout).mkString,
      stderr => err = Source.fromInputStream(stderr).mkString
    )
    val run = proc.run(io)
    if (run.exitValue != 0) {
      logger.error(s"Python parser ended with exit code ${run.exitValue}")
    }
    if (err.nonEmpty) {
      if (err.contains("SyntaxError in ") && python == "python3") {
        return callPython(code, prefix, "python2")
      } else {
        logger.error(err)
      }
    }
    result
  }

}

