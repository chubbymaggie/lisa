package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

case class CarryData(persist: Boolean, ds: Map[String,List[String]]) extends Data {
  def this() = this(false, Map[String,List[String]]())
}
/* Generic template for getting data from one vertex and storing it in another
 *
 * Usage example, storing "str" literals from "Name" nodes in "FunctionDefbody"
 * nodes inside the "FunWords" list:
 * object FunWordsAnalysis extends CarryDataAnalysis {
 *   override val literal = "str"
 *   override def extract = { implicit domain => state => state is "Name" }
 *   override val key = "FunWords"
 *   override def store = { implicit domain => state => state is "FunctionDefbody" }
 * }
 */
trait CarryDataAnalysis extends Analysis {

  val literal: String = ""
  val key: String = ""
  def extract = { implicit d: Domain =>
                  implicit state: AnalysisState => false }
  def store = { implicit d: Domain =>
                implicit state: AnalysisState => false }

  override def start = { implicit domain => state =>
    if (extract(domain)(state)) {
      val name = state[Literal].map.getOrElse("str", "<unknown>")
      state ! new CarryDataPacket(name)
    } else state
  }

  class CarryDataPacket(val d: String) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (store(domain)(state)) {
        val old = state[CarryData]
        val existing = old.ds.getOrElse(key, List[String]())
        val update = old.copy(ds = old.ds + (key -> (d :: existing)))
        state + update
      } else state ! this
    }
  }
}

