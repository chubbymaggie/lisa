package ch.uzh.ifi.seal.lisa.module.analysis

import scala.reflect.{ClassTag, classTag}
import ch.uzh.ifi.seal.lisa.core._
import ChildCountAnalysis.ChildCount

trait ChildCountHelper {
  def leaf(state: AnalysisState): Boolean = { state[ChildCount].n == 0 }
  def allChildren[D <: Data : ClassTag](input: AnalysisState)(
      incomplete: => AnalysisState,
      complete: => AnalysisState): AnalysisState = {
    val id = classTag[D]
    val c = input[ChildCount]
    val ac = c.counters.getOrElse(id, 0) + 1
    (if (ac < c.n) incomplete else complete) + c.copy(counters = c.counters + (id -> ac))
  }
}


