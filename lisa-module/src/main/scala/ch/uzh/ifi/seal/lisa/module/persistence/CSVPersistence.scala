package ch.uzh.ifi.seal.lisa.module.persistence

import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.File
import collection.JavaConverters._
import ch.uzh.ifi.seal.lisa.core._
import com.github.tototoshi.csv._
import com.typesafe.scalalogging.LazyLogging

import ch.uzh.ifi.seal.lisa.core.public._

class CSVPersistence(resultDir: String)(implicit uid: String) extends Persistence with LazyLogging with CSVStats {

  val target = if (resultDir endsWith "/") resultDir else resultDir + "/"

  override def persist(c: LisaComputation) {
    logger.info(s"persisting results to $target ...")
    val tempFile = s"${target}temp.csv"
    val outFile = s"${target}data.csv"
    val revFile = s"${target}commits.csv"
    (new File(target)).mkdirs

    // store commit metadata
    var writer = CSVWriter.open(new File(revFile))
    writer.writeRow(List(
      "N",
      "Hash",
      "AuthorName",
      "AuthorEmail",
      "AuthorDate",
      "CommitterName",
      "CommitterEmail",
      "CommitterDate"
    ))
    c.sources.getRevisions.get.foreach { case (n, rev) =>
      writer.writeRow(List[String](
        rev.n.toString,
        rev.rev,
        rev.authorName,
        rev.authorEmail,
        rev.getAuthorDateString(),
        rev.committerName,
        rev.committerEmail,
        rev.getCommitterDateString()
      ))
    }
    writer.close()

    // store metrics for each vertex
    writer = CSVWriter.open(new File(tempFile))
    // keep track of which data goes in which column
    val columns = new java.util.concurrent.ConcurrentHashMap[String, Int]().asScala
    c.computation.graph.foreachVertex {
      case v: BaseVertex =>
        val id = v.id
        v.state.rangeStates.foreach { case (range, state) =>
          // retrieve data map from state
          val data = state.flatData()
          // don't persist empty data or data where only the name is known
          if (data.nonEmpty && !(data.size == 1 && data.head._1.contains("Name.name"))) {
            synchronized {
              var rowElements = Map[Int, String]()
              data.foreach { case (header, value) =>
                // determine column where this field should go
                val column = columns.getOrElse(header, {
                  val newColumn = columns.size
                    columns += (header -> newColumn)
                    newColumn
                  }
                )
                rowElements = rowElements + (column -> value.toString)
              }
              val row = (0 to columns.size - 1).foldLeft(List[String](
                  v.id,
                  range.start.n.toString,
                  range.end.n.toString,
                  range.start.rev,
                  range.end.rev
                )) { case (acc, index) =>
                val cell = rowElements.getOrElse(index, "")
                acc :+ cell
              }
              writer.writeRow(row)
            }
          }
        }
        case _ =>
      }
    writer.close()
    writer = CSVWriter.open(new File(outFile))
    val headerRow = List("ASTPath", "RevisionStartN", "RevisionEndN", "RevisionStartHash", "RevisionEndHash") ++ columns.toList.sortBy(_._2).map(_._1)
    writer.writeRow(headerRow)
    writer.close()
    val in = new FileInputStream(tempFile)
    val out = new FileOutputStream(outFile, true)
    var ch = 0
    while ({ch = in.read; ch != -1}) {
      out.write(ch)
    }
    in.close
    (new File(tempFile)).delete
    out.close
    storeStats(s"${target}stats.csv")
    logger.info("done persisting results!")
  }
}


