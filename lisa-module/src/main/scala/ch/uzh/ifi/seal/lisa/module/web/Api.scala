package ch.uzh.ifi.seal.lisa.core.web

import com.signalcollect._
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name
import ch.uzh.ifi.seal.lisa.module.analysis.VertexCountAnalysis.VertexCount
import ch.uzh.ifi.seal.lisa.module.analysis.AccessModifiers
import ch.uzh.ifi.seal.lisa.core.computation.ObjectVertex
import ch.uzh.ifi.seal.lisa.core.computation.BaseEdge
import ch.uzh.ifi.seal.lisa.core.public._

import ch.uzh.ifi.seal.lisa.module.analysis.WeightedMethodCountAnalysis.CKWMC
import ch.uzh.ifi.seal.lisa.module.analysis.DepthOfInheritanceTreeAnalysis.CKDIT
import ch.uzh.ifi.seal.lisa.module.analysis.NumberOfChildrenAnalysis.CKNOC
import ch.uzh.ifi.seal.lisa.module.analysis.CouplingBetweenObjectClassesAnalysis.CKCBO
import ch.uzh.ifi.seal.lisa.module.analysis.ResponseForClassAnalysis.CKRFC
import ch.uzh.ifi.seal.lisa.module.analysis.LackOfCohesionInMethodsAnalysis.CKLCOM

import spray.json._

class Api(c: LisaComputation) {
  var cached: JsValue = JsNull
  def dispatch(uri: String): JsValue = {
    uri match {
      case "graph.json" =>
        if (cached == JsNull) { cached = graph() }
        cached
      case _ => JsNull
    }
  }

  def graph(): JsValue = {

    var revisions = revisionData()

    // class data
    val classes = c.graph.aggregate(new ObjectAggregator("class"))
    val classIds = classes.map(_.id)

    val inheritanceEdgeData = edgeData(classes, classIds, "HAS_SUPER")
    val implementsEdgeData = edgeData(classes, classIds, "IMPLEMENTS")

    // method data
    val methods = c.graph.aggregate(new ObjectAggregator("method"))
    val methodIds = methods.map(_.id)
    val methodToClass = edges(classes, methodIds, "HAS_METHOD")

    val classNodeData = classData(classes, methods, methodToClass)

    // method call data
    val methodCallsAll = c.graph.aggregate(new ObjectAggregator("methodCall"))
    val methodCallIds = methodCallsAll.map(_.id)
    val methodCallToCalleeAll = edges(methods, methodCallIds, "CALLED_BY")
    val methodCallToCallerAll = edges(methods, methodCallIds, "HAS_CALL")
    val validEdges: Set[String] = methodCallToCalleeAll.keys.toSet intersect methodCallToCallerAll.keys.toSet
    val methodCalls = methodCallsAll.filter{ o => validEdges.contains(o.id) }
    val methodCallToCallee = methodCallToCalleeAll.filterKeys{ id => validEdges.contains(id) }
    val methodCallToCaller = methodCallToCallerAll.filterKeys{ id => validEdges.contains(id) }

    val methodCallData = JsArray(methodCalls.foldLeft(List[JsObject]()) { case (acc, call) =>
      val m1 = methodCallToCaller.get(call.id).get
      val c1 = methodToClass.get(m1).get
      val m2 = methodCallToCallee.get(call.id).get
      val c2 = methodToClass.get(m2).get
      JsObject(
        "m1" -> JsString(m1),
        "c1" -> JsString(c1),
        "m2" -> JsString(m2),
        "c2" -> JsString(c2),
      ) :: acc
    })

    // full graph json
    JsObject(
      "classes" -> classNodeData,
      "revisions" -> revisions,
      "inheritance" -> inheritanceEdgeData,
      "implements" -> implementsEdgeData,
      "calls" -> methodCallData
    )
  }

  import java.time.format.DateTimeFormatter
  import java.time.ZoneOffset
  val dateFormatter = DateTimeFormatter.ISO_DATE_TIME;
  private def revisionData(): JsValue = {
     JsArray(c.sources.getRevisionFiles().get.map { case (i, (r, _)) =>
       JsObject(
         "n" -> JsNumber(i),
         "subject" -> JsString(r.subject),
         "message" -> JsString(r.message),
         "authorDate" -> JsString(r.authorDate.toInstant.atOffset(ZoneOffset.UTC).format(dateFormatter)), // TODO use correct timezone
         "authorName" -> JsString(r.authorName),
         "authorEmail" -> JsString(r.authorEmail),
         "rev" -> JsString(r.rev)
       )
     }.toList)
  }

  private def classData(classes: List[ObjectVertex], methods: List[ObjectVertex], methodToClass: Map[String, String]): JsValue = {
    JsArray(classes.map { c =>
      val name = JsString(c.state.rangeStates.head._2[Name].name)
      val access = JsArray(c.state.rangeStates.head._2[AccessModifiers].modifiers.map(JsString(_)))
      val rangeStates = JsArray(c.state.revisionRanges.values.map { r =>
        val state = c.state.rangeStates.get(r).get
        JsObject(
          "start" -> JsNumber(r.start.n),
          "end" -> JsNumber(r.end.n),
          "rev" -> JsString(r.start.rev),
          "metrics" -> JsObject(
            "size" -> JsNumber(state[VertexCount].n),
            "CKWMC" -> JsNumber(state[CKWMC].n),
            "CKDIT" -> JsNumber(state[CKDIT].n),
            "CKNOC" -> JsNumber(state[CKNOC].n),
            "CKCBO" -> JsNumber(state[CKCBO].efferentCoupledClasses.size),
            "CKRFC" -> JsNumber(state[CKRFC].responseSet.size),
            "CKLCOM" -> JsNumber(state[CKLCOM].n)
          )
        )
      }.toList)
      val classMethods = methodData(methods.filter { method =>
      methodToClass.get(method.id) match {
        case Some(m) => m == c.id
        case None => false
      }
      }, name)
      JsObject(
        "name" -> name,
        "access" -> access,
        "id" -> JsString(c.id),
        "ranges" -> rangeStates,
        "methods" -> classMethods
      )
    })
  }

  private def methodData(nodes: List[ObjectVertex], owner: JsString): JsValue = {
    JsArray(nodes.map { c =>
      val name = JsString(c.state.rangeStates.head._2[Name].name)
      val rangeStates = JsArray(c.state.revisionRanges.values.map { r =>
        val state = c.state.rangeStates.get(r).get
        JsObject(
          "start" -> JsNumber(r.start.n),
          "end" -> JsNumber(r.end.n),
          "rev" -> JsString(r.start.rev),
          "metrics" -> JsObject(
            "size" -> JsNumber(state[VertexCount].n),
          )
        )
      }.toList)
      JsObject(
        "name" -> name,
        "id" -> JsString(c.id),
        "owner" -> owner,
        "ranges" -> rangeStates
      )
    })
  }

  private def edgeData(nodes: List[ObjectVertex], ids: List[String], relation: String): JsValue = {
    JsArray(nodes.foldLeft(List[JsObject]()) { case (acc, node) =>
      node.edges.foldLeft(acc) { 
        case (acc, e: BaseEdge) =>
          if (ids.contains(e.targetId.toString)) {
            if (e.relation == relation) {
              JsObject(
                "source" -> JsString(node.id),
                "target" -> JsString(e.targetId.toString),
                "relation" -> JsString(relation)
              ) :: acc
            } else acc
          } else acc
      }
    })
  }

  private def edges(nodes: List[ObjectVertex], ids: List[String], relation: String): Map[String,String] = {
    nodes.foldLeft(Map[String,String]()) { case (acc, node) =>
      node.edges.foldLeft(acc) {
        case (acc, e: BaseEdge) =>
          if (ids.contains(e.targetId.toString)) {
            if (e.relation == relation) {
              acc + (e.targetId.toString -> node.id)
            } else acc
          } else acc
      }
    }
  }

}

import com.signalcollect.interfaces.ModularAggregationOperation
class ObjectAggregator(kind: String) extends ModularAggregationOperation[List[ObjectVertex]] {
  val neutralElement = List()
  def aggregate(a: List[ObjectVertex], b: List[ObjectVertex]): List[ObjectVertex] = {
    a ++ b
  }
  def extract(x: Vertex[_, _, _, _]): List[ObjectVertex] = {
    x match {
      case v: ObjectVertex if (v.kind == kind) => {
        List(v)
      }
      case _ => neutralElement
    }
  }
}

