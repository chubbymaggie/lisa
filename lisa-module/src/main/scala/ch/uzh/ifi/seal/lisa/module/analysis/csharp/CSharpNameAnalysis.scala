package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** Resolves the names of methods, classes and interfaces
  *
  * Since the identifier vertex can be nested several levels below its entity,
  * the general NameAnalysis won't work with the CSharp AST.
  */
object AntlrCSharpNameAnalysis extends Analysis {
  import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name

  override def start = { implicit domain => state =>
    if (state.is('wrappedIdentifier)) {
      state[Literal].map.get("IDENTIFIER") match {
        case Some(name) =>
          state ! new CSharpNamePacket(name, true, false)
        case _ => state
      }
    }
    else { state }
  }

  class CSharpNamePacket(val s: String, val directChild: Boolean,
                         val relevant: Boolean) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (directChild && state.is('methodName)) {
        state ! new CSharpNamePacket(s, false, true)
      }
      else if ((directChild || relevant) && state.is('class, 'interface, 'method, 'typeArgument, 'extends, 'parameter)) {
        state + Name(true, s)
      }
      else if (relevant) { state ! this }
      else { state }
    }
  }
}
