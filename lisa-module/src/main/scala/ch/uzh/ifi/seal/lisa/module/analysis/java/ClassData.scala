package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

case class AccessModifiers(persist: Boolean, modifiers: List[String]) extends Data

