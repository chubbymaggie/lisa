package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name

/* Represents the difference between the number of method pairs not having
 * instance variables in common, and the number of method pairs having common
 * instance variables. */
// TODO: remove hardcoded matches
object FieldAnalysis extends Analysis {
  case class Fields(persist: Boolean, fields: Set[String]) extends Data {
    def this() = this(false, Set())
  }

  override def start = { implicit domain => state =>
    if (state is ('field)) (
      state[Literal].map.get("name") match {
        case Some(s) if !s.matches("""^(this)?\$.*""") => state ! FieldPacket(s)
        case _ => state
      }
    )
    else state
  }

  case class FieldPacket(name: String) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class)) {
        state + Fields(false, state[Fields].fields + name)
      }
      else state
    }
  }

}

import ch.uzh.ifi.seal.lisa.module.analysis.FieldAnalysis.Fields

object FieldSharingMethodsAnalysis extends Analysis {
  case class FieldAccess(persist: Boolean, fields: Set[String]) extends Data {
    def this() = this(false, Set())
  }

  override def start = { implicit domain => state =>
    if (state is ('fieldAccess)) (
      state[Literal].map.get("name") match {
        case Some(s) if !s.startsWith("this$") => state ! FieldPacket(s)
        case _ => state
      }
    )
    else state
  }

  case class FieldPacket(name: String) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('method)) {
        (state + FieldAccess(false, state[FieldAccess].fields + name)) ! (MethodPacketUp(name, state[Name].name))
      }
      else state
    }
  }

  case class MethodPacketUp(field: String, method: String) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class)) {
        if (state[Fields].fields.contains(field)) {
          state ↓ (MethodPacketDown(field, method))
        }
        else state
      }
      else state
    }
  }

  case class MethodPacketDown(field: String, method: String) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('method)) {
        state[Literal].map.get("name") match {
          case Some(n) if n != method => state ! (FieldSharingPacket(state[FieldAccess].fields.contains(field)))
          case _ => state
        }
      }
      else state
    }
  }

  case class FieldSharing(persist: Boolean, sharing: Int, notSharing: Int) extends Data {
    def this() = this(false, 0, 0)
  }

  case class FieldSharingPacket(sharing: Boolean) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class)) {
        val s = state[FieldSharing].sharing + (if (sharing) 1 else 0)
        val ns = state[FieldSharing].notSharing + (if (sharing) 0 else 1)
        state + FieldSharing(false, s, ns)
      }
      else state
    }
  }
}

import ch.uzh.ifi.seal.lisa.module.analysis.FieldSharingMethodsAnalysis.FieldSharing

object LackOfCohesionInMethodsAnalysis extends Analysis {
  case class CKLCOM(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 0)
  }

  override def start = { implicit domain => state =>
    if (state is ('class)) {
      val s = state[FieldSharing].sharing
      val ns = state[FieldSharing].notSharing
      val diff = math.max(0, ns - s)
      state + CKLCOM(true, diff)
    }
    else state
  }


}

