package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** An analysis which counts the number of methods in a class
  *
  * The analysis simply starts on a METHOD vertex and increments a counter when
  * reaching a class-like vertex.
  */
object MethodCountAnalysis extends Analysis {
  case class MethodCount(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 0)
  }

  override def start = { implicit domain => state =>
    if (state.is('method)) {
      state ! new MethodCountPacket(1)
    }
    else { state }
  }

  class MethodCountPacket(val count: Int) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class, 'file, 'interface)) {
        // increase the local method count by the incoming count
        val n = MethodCount(true, state[MethodCount].n + count)
        state + n
      }
      else { state ! this }
    }
  }
}

