package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** Resolves the names of methods, classes and interfaces
  *
  * Since the identifier vertex can be nested several levels below its entity,
  * the general NameAnalysis won't work with the Python3 AST.
  */
object AntlrPython3NameAnalysis extends Analysis {
  import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name

  override def start = { _ => state =>
    state[Literal].map.get("NAME") match {
      case Some(name) => state + Name(true, name)
      case _ => state
    }
  }
}
