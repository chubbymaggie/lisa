package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name

/* Count of (public) methods in a class and methods directly called by these. */
// TODO: check public modifier
// TODO: remove hardcoded endswith
object ResponseForClassAnalysis extends Analysis {
  case class CKRFC(persist: Boolean, responseSet: Set[String]) extends Data {
    def this() = this(false, Set())
  }

  override def start = { implicit domain => state =>
    if (state is ('method, 'methodCall)) {
      val name = state[Name].name
      if (!name.endsWith("<init>()V")) {
        state ! new CKRFCPacket(state[Name].name)
      }
      else { state }
    }
    else { state }
  }

  class CKRFCPacket(val name: String) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class)) {
        val n = CKRFC(true, state[CKRFC].responseSet + name)
        state + n
      }
      else { state ! this }
    }
  }
}

