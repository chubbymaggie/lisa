package ch.uzh.ifi.seal.lisa.core.web

import scala.language.postfixOps
import com.typesafe.scalalogging.LazyLogging

import java.io.OutputStream
import java.io.FileInputStream
import java.io.File
import java.io.InputStream
import java.io.BufferedInputStream
import java.net.InetSocketAddress
import java.util.concurrent.Executors

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpHandler
import com.sun.net.httpserver.HttpServer

import ch.uzh.ifi.seal.lisa.core.public._

class Server(c: LisaComputation) extends LazyLogging {

  val port = 9080
  val hostname = "localhost"
  val server = HttpServer.create(new InetSocketAddress(hostname, port), 0)

  server.createContext("/", new Handler(c))
  server.setExecutor(Executors.newCachedThreadPool)
  server.start
  logger.info(s"LISA HTTP: server started on http://${server.getAddress.getHostString}:${server.getAddress.getPort}")

  def shutdown(): Unit = {
    logger.info("LISA http: stopping server...")
    server.stop(5)
  }

}


class Handler(c: LisaComputation) extends HttpHandler {

  val api = new Api(c)

  def handle(t: HttpExchange) {

    val folderName = "web"

    var target = t.getRequestURI.getPath.replaceFirst("^[/.]*", "")

    if (List("").contains(target)) {
      target = "index.html"
    }
    val fileType = target match {
      case t if t.matches(".*\\.html$") => "text/html"
      case t if t.matches(".*\\.css$") => "text/css"
      case t if t.matches(".*\\.js$") => "application/javascript"
      case t if t.matches(".*\\.png$") => "image/png"
      case t if t.matches(".*\\.svg$") => "image/svg+xml"
      case t if t.matches(".*\\.ico$") => "image/x-icon"
      case t if t.matches(".*\\.json$") => "application/json"
      case otherwise => "text/plain"
    }

    def os: OutputStream = t.getResponseBody
    t.getResponseHeaders.set("Content-Type", fileType)

    try {

      // API request
      if (target.startsWith("api")) {
        t.sendResponseHeaders(200, 0)
        val res = api.dispatch(target.drop(4))
        os.write(res.toString.getBytes)
      }

      // file request
      else{
        val root = "./lisa-module/src/main/resources/" + folderName
        var inputStream: InputStream = null
        // If the file exists, use it (when using a cloned repository)
        if (!target.startsWith("webjars") && (new File(root)).exists) {
          val targetPath = root + "/" + target
          try {
            inputStream = new FileInputStream(targetPath)
          } catch {
            case e: java.io.FileNotFoundException =>
              t.sendResponseHeaders(404, 0)
              inputStream = new FileInputStream(root + "/html/404.html")
              t.getResponseHeaders.set("Content-Type", "text/html")
          }
        } // If the file doesn't exist, use the resource from the jar file
        else {
          val pathIntoJar = if (target.startsWith("webjars")) {
            "META-INF/resources/" + target
          } else {
            folderName + "/" + target
          }
          inputStream = getClass.getClassLoader
            .getResourceAsStream(pathIntoJar)
          if (inputStream == null) {
            t.sendResponseHeaders(404, 0)
            inputStream = getClass.getClassLoader
              .getResourceAsStream(folderName + "/html/404.html")
            t.getResponseHeaders.set("Content-Type", "text/html")
          }
        }
        val file = new BufferedInputStream(inputStream.asInstanceOf[InputStream])
        t.sendResponseHeaders(200, 0)
        Iterator
          .continually(file.read)
          .takeWhile(-1 !=)
          .foreach(os.write)
        file.close
      }
    } catch {
      case e: Exception => {
        t.getResponseHeaders.set("Content-Type", "text/plain")
        t.sendResponseHeaders(500, 0)
        os.write(("An exception occurred:\n" + e.getMessage + "\n" +
          e.getStackTrace.mkString("\n")).getBytes)
        e.printStackTrace
      }
    } finally {
      os.close
    }
  }
}

