package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.parser.PythonNativeParseTree

object PythonNativeNameAnalysis extends Analysis {
  import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name

  override def start = { implicit domain => state =>
    if ((domain is PythonNativeParseTree) && (state is ('class, 'method))) {
      val name = state[Literal].map.getOrElse("str", "<unknown>")
      state + Name(true, name)
    } else state
  }
}

