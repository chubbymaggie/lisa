package ch.uzh.ifi.seal.lisa.module.parser

import ch.uzh.ifi.seal.lisa.antlr._
import ch.uzh.ifi.seal.lisa.antlr.parser.CSharp4Lexer
import ch.uzh.ifi.seal.lisa.antlr.parser.CSharp4Parser

object AntlrCSharpParseTree extends Domain {
  override val mapping = Map(
    'class     -> Set("Class_definition",
                      "Enum_definition",
                      "Struct_definition",
                      "Delegate_definition"),
    'block     -> Set("Statement_list"),
    'statement -> Set("While_statement",
                      "Do_statement",
                      "For_statement",
                      "Foreach_statement",
                      "Try_statement",
                      "Declaration_statement",
                      "Local_variable_declaration_statement",
                      "Expression_statement",
                      "If_statement",
                      "Switch_statement",
                      "Break_statement",
                      "Continue_statement",
                      "Goto_statement",
                      "Return_statement",
                      "Throw_statement",
                      "Yield_statement"),
    'ifStat    -> Set("If_statement"),
    'whileStat -> Set("While_statement",
                      "Do_statement"),
    'forStat   -> Set("For_statement",
                      "Foreach_statement"),
    'fork      -> Set("If_statement",
                      "While_statement",
                      "Do_statement",
                      "For_statement",
                      "Foreach_statement",
                      "Switch_label",
                      "Basic_conditional_expression"),
    'method    -> Set("Method_declaration2",
                      "Constructor_declaration2"),
    'variable  -> Set("Local_variable_declaration"),
    'field     -> Set("Field_declaration2"),
    'interface -> Set("Enum_declaration"),
    'parameter -> Set("Explicit_anonymous_function_parameter",
                      "Implicit_anonymous_function_parameter",
                      "Fixed_parameter"),
    'methodName        -> Set("Method_member_name2"),
    'wrappedIdentifier -> Set("Identifier"),
    'typeParameterList -> Set("Type_parameters"),
    'typeParameter     -> Set("Type_parameter"),
    'extends           -> Set("Class_type", "Interface_type_list")
  )
}

object AntlrCSharpParser extends AntlrParser[CSharp4Parser](AntlrCSharpParseTree) {
  override val suffixes = List(".cs")
  override def lex(input: ANTLRInputStream) = new CSharp4Lexer(input)
  override def parse(tokens: CommonTokenStream) = new CSharp4Parser(tokens)
  override def enter(parser: CSharp4Parser) = parser.compilation_unit()
}

