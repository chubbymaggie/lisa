package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/* Counts subclasses for each calss */
object NumberOfChildrenAnalysis extends Analysis with ParentCountHelper {
  case class CKNOC(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 0)
  }

  override def start = { implicit domain => state =>
    if (state.is('class)) { state ? (new CKNOCPacket(), "HAS_SUPER") }
    else state
  }

  class CKNOCPacket() extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class)) {
        val n = state[CKNOC].n + 1
        state + CKNOC(true, n)
      }
      else { state }
    }
  }
}

