package ch.uzh.ifi.seal.lisa.module.persistence

import java.io.File
import ch.uzh.ifi.seal.lisa.core._
import com.github.tototoshi.csv._
import com.typesafe.scalalogging.LazyLogging

import ch.uzh.ifi.seal.lisa.core.public._

class CSVSelectiveGlobalPersistence(resultDir: String, fields: Map[String, AnalysisState => Option[Long]])(implicit uid: String) extends Persistence with LazyLogging with CSVStats {
  import com.signalcollect.interfaces.ModularAggregationOperation
  import com.signalcollect.Vertex

  class GlobalAggregation(extractor: AnalysisState => Option[Long]) extends ModularAggregationOperation[Map[Int,Long]] {
    val neutralElement = Map[Int, Long]()
    def aggregate(a: Map[Int, Long], b: Map[Int, Long]): Map[Int, Long] = {
      (a ++ b).map { case (k, v) =>
        // merge those elements which are both in a and b
        if (a.contains(k) && b.contains(k)) {
          (k -> (a(k) + b(k)))
        } else (k -> v)
      }
    }
    def extract(x: Vertex[_, _, _, _]): Map[Int,Long] = {
      x match {
        case v: BaseVertex => {
          v.state.rangeStates.foldLeft(Map[Int,Long]()) { case (acc, (range, state)) =>
            implicit val domain = v.domain
            if (state is 'file) {
              val data = extractor(state)
              data match {
                case Some(d) =>
                  (range.start.n to range.end.n).foldLeft(acc) { case (acc, n) =>
                    acc + (n -> d)
                  }
                case None => acc
              }
            } else acc
          }
        }
        case _ => neutralElement
      }
    }
  }

  val target = if (resultDir endsWith "/") resultDir else resultDir + "/"

  override def persist(c: LisaComputation) {
    val timestamp = System.nanoTime()
    logger.info(s"persisting results to $target ...")
    val tempFile = s"${target}global.csv.temp"
    val globalFile = s"${target}global.csv"
    (new File(target)).mkdirs

    var headers = Set[String]()
    val global = fields.foldLeft(Map[String,Map[Int, Long]]()) { case (acc, (header, extractor)) =>
      logger.info(s"aggregating ${header}")
      val res = c.graph.aggregate(new GlobalAggregation(extractor))
      headers = headers + header
      acc + (header -> res)
    }

    var writer = CSVWriter.open(new File(tempFile))
    c.sources.getRevisions.get.foreach { case (n, rev) =>
      val dataRow = headers.map { header =>
        global.get(header) match {
          case Some(data) => data.get(n) match {
            case Some(value) => value.toString
            case _ => ""
          }
          case _ => ""
        }
      }

      writer.writeRow(List[String](
        rev.n.toString,
        rev.rev,
        rev.authorName,
        rev.authorEmail,
        rev.getAuthorDateString(),
        rev.committerName,
        rev.committerEmail,
        rev.getCommitterDateString()
      ) ++ dataRow)
    }
    writer.close

    // writer headers to final file
    writer = CSVWriter.open(new File(globalFile))
    writer.writeRow(List(
      "N",
      "Hash",
      "AuthorName",
      "AuthorEmail",
      "AuthorDate",
      "CommitterName",
      "CommitterEmail",
      "CommitterDate"
    ) ++ headers.map{ h =>
      val short = h.dropWhile(_ != '$').drop(1)
      if (short.isEmpty) h else short
    })
    writer.close

    // append temp file contents to final file
    import java.io.FileInputStream
    import java.io.FileOutputStream
    val in = new FileInputStream(tempFile)
    val out = new FileOutputStream(globalFile, true)
    var ch = 0
    while ({ch = in.read; ch != -1}) { out.write(ch) }
    in.close
    out.close
    (new File(tempFile)).delete

    val duration = (System.nanoTime - timestamp)/1000000000.0
    stats += ("persist_seconds" -> duration)
    storeStats(s"${target}stats.csv")

    logger.info("done persisting results!")
  }
}


