package ch.uzh.ifi.seal.lisa.module.analysis

import scala.reflect.ClassTag
import ch.uzh.ifi.seal.lisa.core._

object ChildCountAnalysis extends Analysis {
  /** An analysis which counts the number of children of a vertex
    *
    * The analysis starts on any vertex is sent along a single edge and then
    * collected immediately.
    */
  case class ChildCount(persist: Boolean = false, n: Int = 0, counters: Map[ClassTag[_], Int]) extends Data {
    def this() = this(false, 0, Map())
  }

  override def start = { _ => state =>
    state ! new ChildCountPacket(1)
  }

  class ChildCountPacket(val count: Int) extends AnalysisPacket {
    override def collect = { _ => state =>
      val n = ChildCount(false, state[ChildCount].n + count, Map())
      state + n
    }
  }
}

