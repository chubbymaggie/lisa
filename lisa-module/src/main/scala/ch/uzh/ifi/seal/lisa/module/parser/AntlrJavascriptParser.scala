package ch.uzh.ifi.seal.lisa.module.parser

import ch.uzh.ifi.seal.lisa.core.computation.BaseVertex
import ch.uzh.ifi.seal.lisa.core.AnalysisState
import ch.uzh.ifi.seal.lisa.antlr._
import ch.uzh.ifi.seal.lisa.antlr.parser.ECMAScriptLexer
import ch.uzh.ifi.seal.lisa.antlr.parser.ECMAScriptParser

object AntlrJavascriptParseTree extends Domain {
  override val mapping = Map(
    'class      -> Set("Program"),
    'block      -> Set("StatementList"),
    'statement  -> Set("VariableStatement",
                       "ExpressionStatement",
                       "IfStatement",
                       "IterationStatement",
                       "ContinueStatement",
                       "BreakStatement",
                       "ReturnStatement",
                       "WithStatement",
                       "LabelledStatement",
                       "SwitchStatement",
                       "ThrowStatement",
                       "TryStatement",
                       "DebuggerStatement"),
    'ifStat     -> Set("IfStatement"),
    'whileStat  -> Set("WhileStatement",
                       "DoStatement"),
    'forStat    -> Set("ForStatement",
                       "ForVarStatement",
                       "ForInStatement",
                       "ForVarInStatement"),
    'fork       -> Set("IfStatement",
                       "DoStatement",
                       "WhileStatement",
                       "ForStatement",
                       "ForVarStatement",
                       "ForInStatement",
                       "ForVarInStatement",
                       "CaseClause",
                       "LogicalAndExpression",
                       "LogicalOrExpression",
                       "TernaryExpression"),
    'method     -> Set("FunctionDeclaration",
                       "FunctionExpression"),
    'anonMethod -> Set("AnonFunctionExpression"),
    'variable   -> Set("VariableDeclaration",
                       "AssignmentExpression"),
    'parameter  -> Set("Parameter"),
    'identifier -> Set("Identifier",
                       "IdentifierName")
  )
}

object AntlrJavascriptParser extends AntlrParser[ECMAScriptParser](AntlrJavascriptParseTree) {
  override val suffixes = List(".js")
  override val excludes = List(".min.js")
  implicit val d = domain
  import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name
  override def postprocess(state: AnalysisState, vertex: BaseVertex): AnalysisState = {
    if ((state is 'class) && vertex.id.endsWith("Program0")) {
      state + Name(true, vertex.id.dropRight(9))
    } else state
  }
  override def lex(input: ANTLRInputStream) = new ECMAScriptLexer(input)
  override def parse(tokens: CommonTokenStream) = new ECMAScriptParser(tokens)
  override def enter(parser: ECMAScriptParser) = parser.program()
}

