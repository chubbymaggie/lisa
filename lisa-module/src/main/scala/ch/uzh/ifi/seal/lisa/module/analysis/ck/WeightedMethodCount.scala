package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

object WeightedMethodCountAnalysis extends Analysis {
  case class CKWMC(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 0)
  }

  override def start = { implicit domain => state =>
    if (state.is('method)) {
      state ! new CKWMCPacket(1)
    }
    else { state }
  }

  class CKWMCPacket(val count: Int) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class)) {
        // increase the local method count by the incoming count
        val n = CKWMC(true, state[CKWMC].n + count)
        state + n
      }
      else { state ! this }
    }
  }
}

