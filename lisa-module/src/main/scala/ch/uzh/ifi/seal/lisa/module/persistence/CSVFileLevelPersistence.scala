package ch.uzh.ifi.seal.lisa.module.persistence

import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.File
import ch.uzh.ifi.seal.lisa.core._
import com.github.tototoshi.csv._
import com.typesafe.scalalogging.LazyLogging

import ch.uzh.ifi.seal.lisa.core.public._

class CSVFileLevelPersistence(resultDir: String) extends Persistence with LazyLogging {

  val target = if (resultDir endsWith "/") resultDir else resultDir + "/"

  override def persist(c: LisaComputation) {
    logger.info(s"persisting results to $target ...")
    val tempFile = s"${target}temp.csv"
    val outFile = s"${target}data.csv"
    (new File(target)).mkdirs
    var writer = CSVWriter.open(new File(tempFile))
    // keep track of which data goes in which column
    val columns = new scala.collection.concurrent.TrieMap[String, Int]()
    c.computation.graph.foreachVertex {
      case v: BaseVertex =>
        val id = v.id
        v.state.rangeStates.foreach { case (range, state) =>
          if (state[TypeLabel].t == "META-FILE") {
            // retrieve data map from state
            val data = state.flatData()
            if (data.nonEmpty) {
              var currentRev = Option(range.start)
              do {
                var rowElements = Map[Int, String]()
                data.foreach { case (header, value) =>
                  // determine column where this field should go
                  val column = columns.synchronized{ columns.getOrElseUpdate(header, {
                    val c = columns.size
                    val h = header.dropWhile(_!= '$')
                    logger.debug(s"$h in column $c")
                    c
                  }) }
                  rowElements = rowElements + (column -> value.toString)
                }
                val fileSuffix = v.id.reverse.takeWhile(_ != '.').reverse
                val row = (0 to columns.size - 1).foldLeft(List[String](currentRev.get.n.toString, currentRev.get.rev, fileSuffix, v.id, range.toString, range.toCommitsString)) { case (acc, index) =>
                  val cell = rowElements.getOrElse(index, "")
                  acc :+ cell
                }
                writer.writeRow(row)
                currentRev = currentRev.get.next
              } while (currentRev != None && currentRev.get.n <= range.end.n)
            }
          }
        }
        case _ =>
      }
    writer.close()
    writer = CSVWriter.open(new File(outFile))
    val headerRow = List("RevisionNr", "RevisionSha", "suffix", "ASTPath", "RevisionRangeNumerical", "RevisionRange") ++ columns.toList.sortBy(_._2).map(_._1)
    writer.writeRow(headerRow)
    writer.close()
    val in = new FileInputStream(tempFile)
    val out = new FileOutputStream(outFile, true)
    var ch = 0
    while ({ch = in.read; ch != -1}) {
      out.write(ch)
    }
    in.close
    (new File(tempFile)).delete
    out.close
    logger.info("done persisting results!")
  }
}

