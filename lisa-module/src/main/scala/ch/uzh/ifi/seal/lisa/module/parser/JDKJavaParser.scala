package ch.uzh.ifi.seal.lisa.module.parser

import java.nio.ByteBuffer
import scala.collection.JavaConverters._
import scala.language.existentials
import com.sun.source.util.SimpleTreeVisitor
import java.util.{List => JavaList}
import java.util.{Set => JavaSet}
import com.sun.source.tree._
import com.sun.source.util.SimpleTreeVisitor
import com.sun.tools.javac.api.JavacTool
import java.lang.reflect.ParameterizedType
import sun.reflect.generics.reflectiveObjects.WildcardTypeImpl
import com.signalcollect._
import java.io.OutputStream
import java.io.OutputStreamWriter
import com.typesafe.scalalogging.LazyLogging

import java.net.URI
import javax.tools.JavaFileObject.Kind
import com.sun.source.tree.Tree.{Kind => TreeKind}
import javax.tools.SimpleJavaFileObject
import java.lang.reflect.Method
import java.lang.Class
import java.lang.reflect.{Type => ReturnType}

import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.core.computation.FileMeta

object JDKJavaAST extends Domain {
  override val mapping = Map(
    'class     -> Set("CLASS", "ENUM"),
    'block     -> Set("BLOCK", "PARENTHESIZED"),
    'statement -> Set("STATEMENT"),
    'fork      -> Set("IF",
                      "WHILE_LOOP",
                      "FOR_LOOP",
                      "CATCH",
                      "CASE",
                      "CONDITIONAL_AND",
                      "CONDITIONAL_OR",
                      "CONDITIONAL_EXPRESSION",
                      "DO_WHILE_LOOP",
                      "ENHANCED_FOR_LOOP"),
    'method    -> Set("METHOD"),
    'variable  -> Set("VARIABLE"),
    'field     -> Set("VARIABLE"),
    'interface -> Set("INTERFACE"),
    'file      -> Set("COMPILATION_UNIT"),
    'import    -> Set("IMPORT"),
    'parameter -> Set("PARAMETER")
  )
}



class NullOutputStream extends OutputStream {
  override def write(b: Int) = { }
  override def write(b: Array[Byte]) = { }
}

import java.nio.CharBuffer
class JavaSourceFromCharArray(val name: String, val code: CharBuffer)
    extends SimpleJavaFileObject(URI.create("string:///" + name), Kind.SOURCE) {

  override def getCharContent(ignoreEncodingErrors: Boolean = true): CharSequence = {
    return code;
  }
}

object JDKJavaParser extends Parser with LazyLogging {
  override val suffixes = List[String](".java")
  final val newlines = "\r\n|\r|\n".r
  // Calls to java.lang.reflect.Method.getGenericReturnType are very expensive,
  // so we cache the class methods and method return types.
  private val methodCache: Map[Class[_ <: Tree], Array[Method]] = TreeKind.values
              .filter(_ != TreeKind.OTHER)
              .foldLeft(Map[Class[_ <: Tree], Array[Method]]()) {
    case (acc, n) => acc + (n.asInterface -> n.asInterface.getDeclaredMethods)
  }
  private val returnTypesCache: Map[Method, ReturnType] = methodCache.values.flatten
              .foldLeft(Map[Method, ReturnType]()) {
    case (acc, n) => acc + (n -> n.getGenericReturnType)
  }

  override def parse(files: List[(String,ByteBuffer)],
                     graph: GraphEditor[Any, Any], rev: Revision): ParseStats = {
    var totalLines: Long = 0
    val compiler = JavacTool.create
    val fileObjects: List[JavaSourceFromCharArray] = files.map { case (path, codeBytes) =>
      val code = bytesToChars(codeBytes)
      new JavaSourceFromCharArray(path, code)
    }
    val fileSizes = fileObjects.foldLeft(Map[String,(Int,Int)]()) { case (acc, file) =>
      val code = file.getCharContent(true)
      val lineCount = (newlines findAllIn code).size
      totalLines += lineCount
      acc + (s"/${file.name}" -> ((lineCount, code.length)))
    }
    val javac = compiler.getTask(new OutputStreamWriter(new NullOutputStream()),
                                 null, null, null, null, fileObjects.asJava)
    val revisionRange = RevisionRange(rev, rev)
    var stats = (0L,0L)
    try {
      val trees = javac.parse
      stats = trees.asScala.foldLeft((0L,0L)) { case (acc, tree) =>
        val fileName = tree.getSourceFile.getName
        val fileUri = fileName
        val vertex = new AstVertex(fileUri, 0, revisionRange, fileName, JDKJavaAST)
        val fileSize = fileSizes.getOrElse(fileName, (0, 0))
        val state = vertex.state.rangeStates(revisionRange)
        vertex.state = vertex.state.copy(
          rangeStates = vertex.state.rangeStates + (revisionRange -> (state +
                (TypeLabel(false, "META-FILE")) +
                (FileMeta(true, fileSize._1, fileSize._2)))))
        graph.addVertex(vertex)
        val visitor = new UniversalTreeVisitor(fileName, revisionRange, methodCache, returnTypesCache)
        val context = ParsingContext(graph, "CompilationUnit", fileUri, 0)
        val res = tree.accept(visitor, context)
        (acc._1 + res._1, acc._2 + res._2)
      }
    }
    catch { case e: Exception =>
      logger.error("error at rev: " + rev)
      logger.error(e.toString)
      (0, 0)
    }
    Console.flush()
    ParseStats(totalLines, stats._1, stats._2)
  }
}


// vertices being visited use a ParsingContext to retrieve context information and
// pass information to their children
case class ParsingContext(
  graph: GraphEditor[Any, Any],
  relation: String,
  parentUri: String,
  index: Int = 0
)

class UniversalTreeVisitor(fileName: String, revisionRange: RevisionRange, methodCache: Map[Class[_ <: Tree],Array[Method]], returnTypesCache: Map[Method,ReturnType])
  extends SimpleTreeVisitor[(Long,Long), ParsingContext]((0,0)) with LazyLogging {

  override def defaultAction(tree: Tree, c: ParsingContext): (Long,Long) = {
    // retrieve the interface of the specialized Tree object
    val treeInterface = tree.getKind.asInterface
    val treeTypeName = tree.getKind.toString
    val relation = c.relation
    var parentUri = c.parentUri
    var typeString = treeTypeName
    var addCount = 0L

    relation match {
      // parameter VARIABLEs shall be known as PARAMETERs
      case "Parameters" if typeString == "VARIABLE" =>
        typeString = "PARAMETER"
      case "Statements" =>
        val uri = parentUri + "/Statement" + c.index
        val vertex = new AstVertex(uri, c.index, revisionRange, fileName, JDKJavaAST)
        val state = vertex.state.rangeStates(revisionRange)
        vertex.state = vertex.state.copy(
          rangeStates = vertex.state.rangeStates + (revisionRange ->
            (state + (TypeLabel(false, "STATEMENT")))))
        addCount += 1
        c.graph.addVertex(vertex)
        c.graph.addEdge(uri, new UpwardEdge("HAS-AST_PARENT", parentUri))
        c.graph.addEdge(parentUri, new DownwardEdge("HAS-AST_CHILD", uri))
        parentUri = uri
      case _ =>
    }

    val uri = parentUri + "/" + relation + c.index
    val relevantParentUri = if (JDKJavaAST.labels.contains(treeTypeName)) uri else parentUri

    // Create a URL for this vertex and store it and its type in the graph
    val vertex = new AstVertex(uri, c.index, revisionRange, fileName, JDKJavaAST)
    if (uri == relevantParentUri) {
      c.graph.addVertex(vertex)
      addCount += 1
    }
    var literals = Map[String, String]()

    // for each method declared by the Tree vertex, check the return type of the
    // method. If it's a Tree or collection of Trees, descend. Else, call the
    // method and store the return value.
    var treeKindChildCounter = 0
    methodCache.get(treeInterface).get.foreach{ m =>
      returnTypesCache.get(m).get match {

        // if the return type is parameterized (e.g. Foo<? extends T>), then
        // check if it's a List<? extends Tree>
        case t: ParameterizedType => {

          val rawType = t.getRawType
          val typeArguments = t.getActualTypeArguments

          // check if it is a List
          val (isList, isSet) = rawType match {
            case r: Class[_] =>
              if (classOf[JavaList[_]].isAssignableFrom(r)) { (true, false) }
              else if (classOf[JavaSet[_]].isAssignableFrom(r)) { (false, true) }
              else { (false, false) }
            case _ => (false, false)
          }

          // if both are true, traverse the children
          if (isList) {
            // check if it is parameterized as <? extends Tree>
            val containsTrees = typeArguments.headOption match {
              case Some(wildcard: WildcardTypeImpl) =>
                wildcard.getUpperBounds.headOption match {
                  case Some(bound) =>
                    if (bound.isInstanceOf[Class[_ <: Tree]]) { true }
                    else { false }
                  case _ => false
                }
              case _ => false
            }
            if (containsTrees) {

              val children = Option(m.invoke(tree).asInstanceOf[JavaList[_ <: Tree]])
              children match {
                case Some(children) =>
                  children.asScala.zipWithIndex.foreach{ case(child,i) =>
                    val context = c.copy(parentUri = relevantParentUri,
                                         relation = m.getName.drop(3),
                                         index = i)
                    val res = super.visit(child, context)
                    if (res != null) { addCount += res._1 }
                  }
                case None =>
              }
            }
            else {
              logger.warn(s"got a List with non-Tree contents from Tree" +
                      s"'$treeInterface', method '$m': $t")
            }
          }
          else if (isSet) {
            val items = m.invoke(tree).asInstanceOf[JavaSet[_]]
            items.asScala.zipWithIndex.foreach{ case(child,i) =>
              literals = literals + (s"${m.getName.drop(3)}:$child" -> i.toString)
            }
          }
          else {
            logger.warn(s"unknown collection from Tree '$treeInterface' " +
                    s"method '$m': $t")
          }
        }

        // if the return type is a Tree, simply visit it
        case t if classOf[Tree].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val childVertex = m.invoke(tree).asInstanceOf[Tree]
          val context = c.copy(parentUri = relevantParentUri,
                               relation = m.getName.drop(3),
                               index = treeKindChildCounter)
          treeKindChildCounter += 1
          val res = super.visit(childVertex, context)
          if (res != null) { addCount += res._1 }
        }

        // if the return type is a LineMap, do nothing
        case t if classOf[LineMap].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
        }

        // if the return type is an Element, store the method name (minus the
        // "get" prefix) and the return value as a string literal
        case t if classOf[javax.lang.model.element.Name].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val name = Option(m.invoke(tree).asInstanceOf[javax.lang.model.element.Name])
          name.foreach { value =>
            literals = literals + (s"${m.getName.drop(3)}" -> value.toString)
          }
        }

        // if the return type is a TypeKind, store the method name (minus the
        // "get" prefix) and the return value as a string literal
        case t if classOf[javax.lang.model.`type`.TypeKind].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val typeKind = m.invoke(tree).asInstanceOf[javax.lang.model.`type`.TypeKind]
          literals = literals + (s"${m.getName.drop(3)}" -> typeKind.toString)
        }

        // if the return type is a FileObject, store the method name (minus the
        // "get" prefix) and the return value as a string literal
        case t if classOf[javax.tools.JavaFileObject].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val file = m.invoke(tree).asInstanceOf[javax.tools.JavaFileObject]
          literals = literals + (s"${m.getName.drop(3)}" -> file.getName)
        }

        // if the return type is a Boolean, store the method name and the return
        // value as a boolean literal
        case t if classOf[Boolean].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val bool = m.invoke(tree).asInstanceOf[Boolean]
          literals = literals + (s"${m.getName}" -> m.getName)
        }

        // if the return type is an Object, store the method name (minus the
        // "get" prefix) and the return value as a literal of no specific type
        case t if classOf[java.lang.Object].isAssignableFrom(t.asInstanceOf[Class[_]]) => {
          val obj = m.invoke(tree).asInstanceOf[java.lang.Object]
          literals = literals + (s"${m.getName.drop(3)}" ->
                                 (if (obj == null) "null" else obj.toString))
        }

        // if the return type is anything else (which shouldn't typically be),
        // then just log a warning
        case t => {
          logger.warn(s"unknown return type from Tree '$treeInterface' " +
                  s"method '$m': $t")
        }
      }
    }

    if (uri == relevantParentUri) {
      val vertex = new AstVertex(uri, c.index, revisionRange, fileName, JDKJavaAST)
      val state = vertex.state.rangeStates(revisionRange)
      // SimpleName needs to be renamed for compatibility with NameAnalysis
      literals = literals.get("SimpleName") match {
        case Some(name) => literals - "SimpleName" + ("Name" -> name)
        case _ => literals
      }
      vertex.state = vertex.state.copy(
        rangeStates = vertex.state.rangeStates + (revisionRange ->
          (state + (Literal(false,literals))
                 + (TypeLabel(false, typeString)))))
      c.graph.addVertex(vertex)
      addCount += 1
      c.graph.addEdge(uri, new UpwardEdge("HAS-AST_PARENT", parentUri))
      c.graph.addEdge(parentUri, new DownwardEdge("HAS-AST_CHILD", uri))
    }

    val res = super.defaultAction(tree, c)
    if (res != null) { addCount += res._1 }
    (addCount, 0)
  }

}

