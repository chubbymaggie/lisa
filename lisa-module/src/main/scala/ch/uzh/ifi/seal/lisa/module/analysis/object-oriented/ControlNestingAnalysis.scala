package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** An analysis which computes the highest nesting that exists below a vertex
  *
  * The analysis keeps track of how many signals it has received from children
  * and only spawns a new analysis once all child values have been received.
  */
object ControlNestingAnalysis extends Analysis with ChildCountHelper {
  case class ControlNesting(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 0)
  }

  override def start = { _ => state =>
    // start on any leaf vertex
    if (leaf(state)) state ! new ControlNestingPacket(0) else state
  }

  class ControlNestingPacket(val count: Int) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      val d = state[ControlNesting]

      // if this is a fork, add one to the nesting value received from the child
      val cumulativeControlNesting = if (state is 'fork) count + 1 else count
      // the new highest nesting is either the new one or the existing one
      val newValue = math.max(cumulativeControlNesting, d.n)
      val persist = state is ('class, 'method, 'file)
      val newState = state + ControlNesting(persist, newValue)

      // spawn new analysis if all child values have been collected
      allChildren[ControlNesting](state)(
        incomplete = newState,
        complete = newState ! new ControlNestingPacket(newValue)
      )

    }
  }

}
