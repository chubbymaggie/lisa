package ch.uzh.ifi.seal.lisa.module

import ch.uzh.ifi.seal.lisa.core.public._
import ch.uzh.ifi.seal.lisa.core.source.GitAgent
import com.typesafe.config.ConfigFactory
import ch.uzh.ifi.seal.lisa.module.parser.AntlrCSharpParser
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavaParser
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavascriptParser
import ch.uzh.ifi.seal.lisa.module.analysis.UniversalAnalysisSuite
import ch.uzh.ifi.seal.lisa.module.persistence.CSVPersistence

object LisaCli extends App {
  val usage =
"""Usage: LisaCli can be run with the following combination of parameters.
Passing "" instead of a SHA-1 indicates that all analyses (from the start and/or
to the end) should be analyzed. LisaCli supports only Git repositories.
   LisaCli <target-dir> <git-url> <start-sha1> <end-sha1>
   LisaCli <target-dir> <git-url> <start-sha1> ""
   LisaCli <target-dir> <git-url> "" <end-sha1>
   LisaCli <target-dir> <git-url> "" """""
  val arglist = args.toList
  type OptionMap = Map[Symbol, Any]

  arglist match {
    case targetDir :: url :: Nil => run(targetDir, url, None, None)
    case targetDir :: url :: startRevision :: endRevision :: Nil =>
      val start = Option(startRevision).filter(_.trim.nonEmpty)
      val end = Option(endRevision).filter(_.trim.nonEmpty)
      run(targetDir, url, start, end)
    case _ => println(usage)
  }

  def run(targetDir: String, url: String, start: Option[String], end: Option[String]) {
    import java.util.UUID
    val shortname = url.reverse.takeWhile(_ != '/').reverse
    implicit val uid = shortname + UUID.randomUUID().toString
    val config = ConfigFactory.load("lisa.conf")
    val gitLocalDir = config.getConfig("lisa").getString("git.localDir")
    val parsers = List[Parser](AntlrCSharpParser, AntlrJavaParser, AntlrJavascriptParser)
    val analyses = UniversalAnalysisSuite
    val persistence = new CSVPersistence(targetDir)
    val sources = new GitAgent(parsers, url, gitLocalDir, start, end)
    val c = new LisaComputation(sources, analyses, persistence)
    c.execute
  }
}

