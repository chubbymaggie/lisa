package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** An analysis which counts the number of classes on file and root levels
  *
  * The analysis simply starts on a class vertex and increments a counter when
  * reaching a file or root vertex.
  */
object ClassCountAnalysis extends Analysis {
  case class ClassCount(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 0)
  }

  override def start = { implicit domain => state =>
    if (state.is('class)) state ! new ClassCountPacket(1) else state
  }

  class ClassCountPacket(val count: Int) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is ('file)) {
        // increase the local class count by the incoming count
        val n = ClassCount(true, state[ClassCount].n + count)
        // propagate analysis upward
        state + n ! this
      }
      else { state ! this }
    }
  }

}
