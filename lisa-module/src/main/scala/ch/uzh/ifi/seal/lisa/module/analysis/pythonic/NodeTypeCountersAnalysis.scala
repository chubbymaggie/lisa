package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

object NodeTypeCountersAnalysis extends Analysis with ChildCountHelper {

  case class NodeTypeCounters(persist: Boolean, counters: Map[String,Int]) extends Data {
    def this() = this(false, Map[String,Int]())
  }

  override def start = { implicit domain => state =>
    // start on any leaf vertex
    if (leaf(state)) {
      state among 'idiomatic match {
        case Some(nodeType) => state ! new NodeTypeCountersPacket(NodeTypeCounters(false, Map(nodeType -> 1)))
        case _ => state ! new NodeTypeCountersPacket(state[NodeTypeCounters])
      }
    }
    else state
  }

  class NodeTypeCountersPacket(val d: NodeTypeCounters) extends AnalysisPacket {

    private def mergeMaps(a: Map[String,Int], b: Map[String,Int]): Map[String,Int] = {
      (a ++ b).map { case (k, v) =>
        if (a.contains(k)) {
          (k -> (a(k) + b.getOrElse(k, 0)))
        } else (k -> v)
      }
    }

    override def collect = { implicit domain => state =>
      val old = state[NodeTypeCounters]
      val update = old.copy(
        counters = mergeMaps(old.counters, d.counters))

      allChildren[NodeTypeCounters](state)(
        incomplete = state + update,
        complete = {
          val persist = ((state is ('class, 'method, 'file)) && update.counters.size > 0)
          val finalized = (
            state among 'idiomatic match {
              case Some(nodeType) => update.copy(counters = update.counters + (nodeType -> (update.counters.getOrElse(nodeType, 0) + 1)))
              case _ => update
            }
          ).copy(persist = persist)
          state + finalized ! new NodeTypeCountersPacket(finalized)
        }
      )

    }
  }
}

