package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** Resolves the names of methods, classes and interfaces
  *
  * Since the identifier vertex can be nested several levels below its entity,
  * the general NameAnalysis won't work with the Java AST.
  */
object AntlrJavaNameAnalysis extends Analysis {
  import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name

  override def start = { implicit domain => state =>
    if (state.is('class, 'interface, 'method)) {
      state[Literal].map.get("Identifier") match {
        case Some(name) => state + Name(true, name)
        case _ => state
      }
    }
    else if (state.is('variableId)) {
      val identifier = state[Literal].map.get("Identifier")
      if (identifier.nonEmpty) {
        state ! new JavaNamePacket(identifier.get)
      } else state
    }
    else { state }
  }

  class JavaNamePacket(val identifier: String) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state is 'parameter) {
        state + Name(true, identifier)
      } else state
    }
  }

}
