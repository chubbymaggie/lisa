package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name

/* The coupling between object classes (CBO) metric represents the number of
 * classes coupled to a given class (efferent couplings, Ce). This coupling can
 * occur through method calls, field accesses, inheritance, arguments, return
 * types, and exceptions */
object CouplingBetweenObjectClassesAnalysis extends Analysis {
  case class CKCBO(persist: Boolean, efferentCoupledClasses: Set[String]) extends Data {
    def this() = this(false, Set())
  }
  case class BelongsTo(persist: Boolean, name: String) extends Data {
    def this() = this(false, "unknown-owner")
  }

  override def start = { implicit domain => state =>
    if (state.is('class)) ( state
      ? (CKCBOPacket(state[Name].name), "HAS_CHILD")
      ? (CKCBOPacket(state[Name].name), "HAS_SUPER")
      ? (ClassNamePacket(state[Name].name), "TYPE_OF_VARIABLE")
      ↓ (ClassNamePacket(state[Name].name), "HAS_METHOD")
      ↓ (ClassNamePacket(state[Name].name), "HAS_FIELD")
      ↓ (ClassNamePacket(state[Name].name), "HAS_VARIABLE")
    )
    else state
  }

  case class ClassNamePacket(name: String) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('variable)) (
        state ? (CKCBOPacket(name), "HAS_TYPE")
      )
      else if (state.is('method)) (
        (state + BelongsTo(true, name))
          ? (CKCBOPacket(name), "CALLED_BY")
          ↓ (ClassNamePacket(name), "HAS_CALL")
          ↓ (this, "HAS_VARIABLE")
      )
      else if (state.is('methodCall)) (
        state ? (CKCBOPacket(name), "CALLS")
      )
      else if (state.is('field)) (
        (state + BelongsTo(true, name))
          ? (CKCBOPacket(name), "ACCESSED_BY")
          ? (CKCBOPacket(name), "ACCESSES")
      )
      else { state }
    }
  }

  case class CKCBOPacket(name: String) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class) && state[Name].name != name) {
        val s = state[CKCBO].efferentCoupledClasses + name
        state + CKCBO(true, s)
      }
      else {
        state ! (this, "BELONGS_TO")
      }
    }
  }

}

