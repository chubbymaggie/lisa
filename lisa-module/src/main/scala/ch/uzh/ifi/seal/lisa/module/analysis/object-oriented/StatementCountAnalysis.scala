package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** An analysis which counts the number of statements on several levels
  *
  * The analysis starts on any statement vertex and increments a counter when
  * reaching a method, file or class-like vertex.
  */
object StatementCountAnalysis extends Analysis {
  case class StatementCount(persist: Boolean, n: Int) extends Data {
    def this() = this(false, 0)
  }

  override def start = { implicit domain => state =>
    if (state.is('statement)) {
      state ! new StatementCountPacket(1)
    }
    else { state }
  }

  class StatementCountPacket(val count: Int) extends AnalysisPacket {
    override def collect = { implicit domain => state =>
      if (state.is('class, 'file, 'interface, 'method)) {
        // increase the local statement count by the incoming count
        val n = StatementCount(true, state[StatementCount].n + count)
        if (state.is('method)) {
          state + n ! this
        }
        else {
          state + n
        }
      }
      else { state ! this }
    }
  }
}
