package ch.uzh.ifi.seal.lisa.module.persistence

import ch.uzh.ifi.seal.lisa.core.public._

class DummyPersistence(resultsDir: String) extends Persistence {
  override def persist(c: LisaComputation) { }
}

