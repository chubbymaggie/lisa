package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

object CKAnalysisSuite extends AnalysisSuite {
  val phases: List[List[Analysis]] = List(
    List(
      WeightedMethodCountAnalysis,
      DepthOfInheritanceTreeAnalysis,
      NumberOfChildrenAnalysis,
      CouplingBetweenObjectClassesAnalysis,
      ResponseForClassAnalysis,
      FieldAnalysis,
    ),
    List(
      FieldSharingMethodsAnalysis,
    ),
    List(
      LackOfCohesionInMethodsAnalysis,
    ),
  )
}

