package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/** Provides only a data structure for storing the name of named vertices.
  *
  * Individual analyses for different languages need to be implemented.
  */
object NameAnalysis extends Analysis {
  case class Name(persist: Boolean, name: String) extends Data {
    def this() = this(false, "unknown-name")
  }
}

