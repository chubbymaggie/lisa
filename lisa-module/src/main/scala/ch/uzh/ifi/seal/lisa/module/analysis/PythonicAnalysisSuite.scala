package ch.uzh.ifi.seal.lisa.module.analysis

import ch.uzh.ifi.seal.lisa.core._

/* Python native analyses only sure to be compatible if run on Python 3.6.0 */
object PythonicAnalysisSuite extends AnalysisSuite {
  val phases: List[List[Analysis]] = List(
    List(
      ChildCountAnalysis,
      PythonNativeNameAnalysis
    ),
    List(
      MagicMethodsAnalysis,
      NodeTypeCountersAnalysis,
      CallNameCountersAnalysis,
      NestedFunctionAnalysis,
      MethodCountAnalysis,
      ClassCountAnalysis,
      DecoratorAnalysis
    ),
    List(
      NestedFunctionCountAnalysis
    )
  )
}

