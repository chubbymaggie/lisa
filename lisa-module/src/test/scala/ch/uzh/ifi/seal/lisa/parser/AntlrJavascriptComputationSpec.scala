package ch.uzh.ifi.seal.lisa.parser

import scala.collection.mutable._
import java.util.concurrent.ConcurrentLinkedQueue

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavascriptParser
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavascriptParseTree
import ch.uzh.ifi.seal.lisa.module.analysis.MccAnalysis.Mcc
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name
import ch.uzh.ifi.seal.lisa.module.analysis.UniquePathsAnalysis.UniquePaths

class AntlrJavascriptComputationSpec extends LisaSpec("javascriptSpec") {

  implicit override val domain = AntlrJavascriptParseTree
  val c = makeComputation(List[Parser](AntlrJavascriptParser))

  "The small computation should" >> {

    prepare(c)
    compute(c)

    "produce correct number of class aggregates" in {
      classAggregates = getClassAggregates(c.graph)
      logger.debug(s"class aggregates: ${classAggregates.size}")
      assert(classAggregates.size == 3)
      success
    }

    "calculate correct NOM measures for classes" in {
      logger.debug("checking NOM")
      val expectedValues = Map(
        "/src/main.js" ->     Map("[0-0]" -> 0),
        "/src/basics.js" ->   Map("[0-0]" -> 1),
        "/src/advanced.js" -> Map("[0-0]" -> 17)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, nom, _, _, _, _) => (name, range, nom)})
      success
    }

    "calculate correct NumPara measures for classes" in {
      logger.debug("checking NumPara")
      val expectedValues = Map(
        "/src/main.js" ->     Map("[0-0]" -> 0),
        "/src/basics.js" ->   Map("[0-0]" -> 0),
        "/src/advanced.js" -> Map("[0-0]" -> 17)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, numpara, _, _, _) => (name, range, numpara)})
      success
    }

    "calculate correct statement measures for classes" in {
      logger.debug("checking statements")
      val expectedValues = Map(
        "/src/main.js" ->     Map("[0-0]" -> 8),
        "/src/basics.js" ->   Map("[0-0]" -> 4),
        "/src/advanced.js" -> Map("[0-0]" -> 80)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, _, _, stmts) => (name, range, stmts)})
      success
    }

    "calculate correct NOV measures for classes" in {
      logger.debug("checking NOV")
      val expectedValues = Map(
        "/src/main.js" ->     Map("[0-0]" -> 8),
        "/src/basics.js" ->   Map("[0-0]" -> 4),
        "/src/advanced.js" -> Map("[0-0]" -> 53)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, nov, _, _) => (name, range, nov)})
      success
    }

    "calculate correct uniquePaths measures for methods of the ControlFlow class" in {
      logger.debug("checking uniquePaths")
      val measurements = new ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()
      c.graph.foreachVertex {
        case v: AstVertex =>
          var interesting = v.id.contains("advanced.js") &&
            v.state.rangeStates.values.exists { value =>
              value.is('method) && value[Name].name != "unknown-name"
            }
          if (interesting) {
            v.state.rangeStates.foreach { case (range, state) =>
              val name = state[Name].name
              measurements.add((name, range, 0, 0, 0, state[UniquePaths].n, 0))
            }
          }
        case _ =>
      }
      val expectedValues = Map(
        "Types" ->        Map("[0-0]" -> BigInt(1)),
        "Conditions" ->   Map("[0-0]" -> BigInt(3)),
        "ControlFlow" ->  Map("[0-0]" -> BigInt(6)),
        "Functions" ->    Map("[0-0]" -> BigInt(1)),
        "Constructors" -> Map("[0-0]" -> BigInt(1)),
        "Person" ->       Map("[0-0]" -> BigInt(1)),
        "Exceptions" ->   Map("[0-0]" -> BigInt(1)),
        "foo" ->          Map("[0-0]" -> BigInt(1)),
        "bar" ->          Map("[0-0]" -> BigInt(1)),
        "helper" ->       Map("[0-0]" -> BigInt(1)),
        "Misc" ->         Map("[0-0]" -> BigInt(1))
      )
      checkValues(measurements.toArray(Array[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()).toList, expectedValues,
                         { case (name, range, _, _, _, uniquePaths, _) => (name, range, uniquePaths)})
      success
    }

    "calculate correct MCC measures for methods of the ControlFlow class" in {
      logger.debug("checking ControlFlow MCC")
      val measurements = new ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()
      c.graph.foreachVertex {
        case v: AstVertex =>
          var interesting = v.id.contains("advanced.js") &&
            v.state.rangeStates.values.exists { value =>
              value.is('method) && value[Name].name != "unknown-name"
            }
          if (interesting) {
            v.state.rangeStates.foreach { case (range, state) =>
              val name = state[Name].name
              measurements.add((name, range, 0, 0, 0, state[Mcc].n, 0))
            }
          }
        case _ =>
      }
      val expectedValues = Map(
        "Types" ->        Map("[0-0]" -> 1),
        "Conditions" ->   Map("[0-0]" -> 3),
        "ControlFlow" ->  Map("[0-0]" -> 6),
        "Functions" ->    Map("[0-0]" -> 1),
        "Constructors" -> Map("[0-0]" -> 1),
        "Person" ->       Map("[0-0]" -> 1),
        "Exceptions" ->   Map("[0-0]" -> 1),
        "foo" ->          Map("[0-0]" -> 1),
        "bar" ->          Map("[0-0]" -> 1),
        "helper" ->       Map("[0-0]" -> 1),
        "Misc" ->         Map("[0-0]" -> 1)
      )
      checkValuesInt(measurements.toArray(Array[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()).toList, expectedValues,
                         { case (name, range, _, _, _, mcc, _) => (name, range, mcc)})
      success
    }


    "calculate correct MCC measures for classes" in {
      logger.debug("checking class MCC")
      val expectedValues = Map(
        "/src/main.js" ->     Map("[0-0]" -> 1),
        "/src/basics.js" ->   Map("[0-0]" -> 1),
        "/src/advanced.js" -> Map("[0-0]" -> 8)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, _, mcc, _) => (name, range, mcc)})
      success
    }


    persist(c)
    finalize(c)

  }

}

