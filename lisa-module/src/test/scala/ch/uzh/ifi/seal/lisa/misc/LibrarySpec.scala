package ch.uzh.ifi.seal.lisa.misc

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec
import ch.uzh.ifi.seal.lisa.core.Parser
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavaParser

class LibrarySpec extends LisaSpec("javaSpec") {

  val c = makeComputation(List[Parser](AntlrJavaParser))

  basicTest(c)

}

