package ch.uzh.ifi.seal.lisa.misc

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec
import ch.uzh.ifi.seal.lisa.core.Parser
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavaParser
import ch.uzh.ifi.seal.lisa.module.persistence.CSVPerRevisionParallelizedPersistence

class MediumIntegrationSpec extends LisaSpec("mediumIntegrationSpec") {

  override val persistence = new CSVPerRevisionParallelizedPersistence(resultDir)
  val c = makeComputation(List[Parser](AntlrJavaParser), periodicStats=false)

  basicTest(c)

}


