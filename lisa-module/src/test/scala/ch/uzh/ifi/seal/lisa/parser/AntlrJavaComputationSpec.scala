package ch.uzh.ifi.seal.lisa.parser

import scala.collection.mutable._
import java.util.concurrent.ConcurrentLinkedQueue

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavaParser
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavaParseTree
import ch.uzh.ifi.seal.lisa.module.analysis.MccAnalysis.Mcc
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name
import ch.uzh.ifi.seal.lisa.module.analysis.UniquePathsAnalysis.UniquePaths

class AntlrJavaComputationSpec extends LisaSpec("javaSpec") {

  implicit override val domain = AntlrJavaParseTree
  val c = makeComputation(List[Parser](AntlrJavaParser))

  "The small computation should" >> {

    prepare(c)
    compute(c)

    "produce correct number of class aggregates" in {
      classAggregates = getClassAggregates(c.graph)
      logger.debug(s"class aggregates: ${classAggregates.size}")
      assert(classAggregates.size == 28)
      success
    }

    "calculate correct NOM measures for classes" in {
      logger.debug("checking NOM")
      val expectedValues = Map(
        "Main" ->                 Map("[1-5]" -> 1,
                                      "[6-6]" -> 1,
                                      "[7-12]" -> 1,
                                      "[13-16]" -> 1),
        "AbstractClass" ->        Map("[1-16]" -> 1),
        "CustomException" ->      Map("[1-16]" -> 1),
        "ConcreteClass" ->        Map("[1-1]" -> 14,
                                      "[2-2]" -> 16,
                                      "[3-4]" -> 15,
                                      "[5-15]" ->  15,
                                      "[16-16]" -> 15),
        "ConcreteSubclass" ->     Map("[1-16]" -> 1),
        "ControlFlow" ->          Map("[13-13]" -> 24,
                                      "[14-16]" -> 26),
        "InnerClass" ->           Map("[1-15]" -> 0,
                                      "[16-16]" -> 1),
        "InnerInnerClass" ->      Map("[1-15]" -> 0,
                                      "[16-16]" -> 2),
        "GenericPairInterface" -> Map("[1-16]" -> 5),
        "GenericPair" ->          Map("[1-16]" -> 5),
        "SubpackageClass" ->      Map("[1-16]" -> 1),
        "SubsubpackageClass" ->   Map("[1-16]" -> 1),
        "CodeSmell" ->            Map("[7-16]" -> 0),
        "BrainMethod" ->          Map("[7-7]" -> 1,
                                      "[8-8]" -> 1,
                                      "[9-9]" -> 1,
                                      "[10-13]" -> 1,
                                      "[14-16]" -> 1),
        "SiblingClass" ->         Map("[1-16]" -> 1)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, nom, _, _, _, _) => (name, range, nom)})
      success
    }

    "calculate correct NumPara measures for classes" in {
      logger.debug("checking NumPara")
      val expectedValues = Map(
        "Main" ->                 Map("[1-5]" -> 1,
                                      "[6-6]" -> 1,
                                      "[7-12]" -> 1,
                                      "[13-16]" -> 1),
        "AbstractClass" ->        Map("[1-16]" -> 0),
        "CustomException" ->      Map("[1-16]" -> 1),
        "ConcreteClass" ->        Map("[1-1]" -> 10,
                                      "[2-2]" -> 10,
                                      "[3-4]" -> 10,
                                      "[5-15]" ->  10,
                                      "[16-16]" -> 10),
        "ConcreteSubclass" ->     Map("[1-16]" -> 0),
        "ControlFlow" ->          Map("[13-13]" -> 0,
                                      "[14-16]" -> 0),
        "InnerClass" ->           Map("[1-15]" -> 0,
                                      "[16-16]" -> 0),
        "InnerInnerClass" ->      Map("[1-15]" -> 0,
                                      "[16-16]" -> 0),
        "GenericPairInterface" -> Map("[1-16]" -> 0),
        "GenericPair" ->          Map("[1-16]" -> 4),
        "SubpackageClass" ->      Map("[1-16]" -> 0),
        "SubsubpackageClass" ->   Map("[1-16]" -> 0),
        "CodeSmell" ->            Map("[7-16]" -> 0),
        "BrainMethod" ->          Map("[7-7]" -> 0,
                                      "[8-8]" -> 0,
                                      "[9-9]" -> 0,
                                      "[10-13]" -> 0,
                                      "[14-16]" -> 0),
        "SiblingClass" ->         Map("[1-16]" -> 0)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, numpara, _, _, _) => (name, range, numpara)})
      success
    }

    "calculate correct statement measures for classes" in {
      logger.debug("checking statements")
      val expectedValues = Map(
        "Main" ->                 Map("[1-5]" -> 31,
                                      "[6-6]" -> 32,
                                      "[7-12]" -> 33,
                                      "[13-16]" -> 34),
        "AbstractClass" ->        Map("[1-16]" -> 0),
        "CustomException" ->      Map("[1-16]" -> 1),
        "ConcreteClass" ->        Map("[1-1]" -> 13,
                                      "[2-2]" -> 13,
                                      "[3-4]" -> 13,
                                      "[5-15]" ->  11,
                                      "[16-16]" -> 11),
        "ConcreteSubclass" ->     Map("[1-16]" -> 1),
        "ControlFlow" ->          Map("[13-13]" -> 86,
                                      "[14-16]" -> 103),
        "InnerClass" ->           Map("[1-15]" -> 0,
                                      "[16-16]" -> 1),
        "InnerInnerClass" ->      Map("[1-15]" -> 0,
                                      "[16-16]" -> 4),
        "GenericPairInterface" -> Map("[1-16]" -> 0),
        "GenericPair" ->          Map("[1-16]" -> 6),
        "SubpackageClass" ->      Map("[1-16]" -> 1),
        "SubsubpackageClass" ->   Map("[1-16]" -> 1),
        "CodeSmell" ->            Map("[7-16]" -> 0),
        "BrainMethod" ->          Map("[7-7]" -> 1,
                                      "[8-8]" -> 48,
                                      "[9-9]" -> 30,
                                      "[10-13]" -> 30,
                                      "[14-16]" -> 21),
        "SiblingClass" ->         Map("[1-16]" -> 1)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, _, _, stmts) => (name, range, stmts)})
      success
    }

    "calculate correct NOV measures for classes" in {
      logger.debug("checking NOV")
      val expectedValues = Map(
        "Main" ->                 Map("[1-5]" -> 16,
                                      "[6-6]" -> 17,
                                      "[7-12]" -> 18,
                                      "[13-16]" -> 19),
        "AbstractClass" ->        Map("[1-16]" -> 0),
        "CustomException" ->      Map("[1-16]" -> 1),
        "ConcreteClass" ->        Map("[1-1]" -> 17,
                                      "[2-2]" -> 17,
                                      "[3-4]" -> 17,
                                      "[5-15]" ->  17,
                                      "[16-16]" -> 17),
        "ConcreteSubclass" ->     Map("[1-16]" -> 0),
        "ControlFlow" ->          Map("[13-13]" -> 13,
                                      "[14-16]" -> 15),
        "InnerClass" ->           Map("[1-15]" -> 0,
                                      "[16-16]" -> 0),
        "InnerInnerClass" ->      Map("[1-15]" -> 0,
                                      "[16-16]" -> 0),
        "GenericPairInterface" -> Map("[1-16]" -> 6),
        "GenericPair" ->          Map("[1-16]" -> 6),
        "SubpackageClass" ->      Map("[1-16]" -> 2),
        "SubsubpackageClass" ->   Map("[1-16]" -> 2),
        "CodeSmell" ->            Map("[7-16]" -> 0),
        "BrainMethod" ->          Map("[7-7]" -> 1,
                                      "[8-8]" -> 8,
                                      "[9-9]" -> 8,
                                      "[10-13]" -> 9,
                                      "[14-16]" -> 9),
        "SiblingClass" ->         Map("[1-16]" -> 2)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, nov, _, _) => (name, range, nov)})
      success
    }

    "calculate correct uniquePaths measures for methods of the ControlFlow class" in {
      logger.debug("checking uniquePaths")
      val measurements = new ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()
      c.graph.foreachVertex {
        case v: AstVertex =>
          var interesting = v.id.contains("ControlFlow") &&
            v.state.rangeStates.values.exists { value =>
              value.is('method)
            }
          if (interesting) {
            v.state.rangeStates.foreach { case (range, state) =>
              val name = state[Name].name
              measurements.add((name, range, 0, 0, 0, state[UniquePaths].n, 0))
            }
          }
        case _ =>
      }
      val expectedValues = Map(
        "singleIf" ->                               Map("[13-16]" -> BigInt(2)),
        "twoNestedIfs" ->                           Map("[13-16]" -> BigInt(3)),
        "twoConsecutiveIfs" ->                      Map("[13-16]" -> BigInt(4)),
        "simpleIfTree" ->                           Map("[13-16]" -> BigInt(10)),
        "complexIfTree" ->                          Map("[13-16]" -> BigInt(30)),
        "orOperator" ->                             Map("[13-16]" -> BigInt(2)),
        "orOperatorInsideIf" ->                     Map("[13-16]" -> BigInt(3)),
        "ternaryOperator" ->                        Map("[13-16]" -> BigInt(2)),
        "twoOrOperatorsInsideIf" ->                 Map("[13-16]" -> BigInt(4)),
        "ternaryOperatorInsideIf" ->                Map("[13-16]" -> BigInt(3)),
        "switchThreeCases" ->                       Map("[13-16]" -> BigInt(4)),
        "nestedSwitchThreeTwoOneCases" ->           Map("[13-16]" -> BigInt(10)),
        "consecutiveSwitchThreeTwoCases" ->         Map("[13-16]" -> BigInt(12)),
        "consecutiveSwitchThreeTwoCasesNoBreaks" -> Map("[13-16]" -> BigInt(12)),
        "singleFor" ->                              Map("[13-16]" -> BigInt(2)),
        "twoNestedFors" ->                          Map("[13-16]" -> BigInt(3)),
        "twoConsecutiveFors" ->                     Map("[13-16]" -> BigInt(4)),
        "singleForWithContinue" ->                  Map("[13-16]" -> BigInt(2)),
        "singleWhile" ->                            Map("[13-16]" -> BigInt(2)),
        "twoNestedWhiles" ->                        Map("[13-16]" -> BigInt(3)),
        "consecutiveWhiles" ->                      Map("[13-16]" -> BigInt(4)),
        "singleWhileWithBreak" ->                   Map("[13-16]" -> BigInt(2)),
        "singleWhileWithContinue" ->                Map("[13-16]" -> BigInt(2)),
        "switchWithIfWhilesInCasesWithBreaks" ->    Map("[14-16]" -> BigInt(8)),
        // fallthrough cases are not treated seperately
        "switchWithIfWhilesInCasesWithoutBreaks" -> Map("[14-16]" -> BigInt(8)),
        "complexMixedExample" ->                    Map("[13-13]" -> BigInt(252),
                                                        "[14-16]" -> BigInt(252))
      )
      checkValues(measurements.toArray(Array[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()).toList, expectedValues,
                         { case (name, range, _, _, _, uniquePaths, _) => (name, range, uniquePaths)})
      success
    }

    "calculate correct MCC measures for methods of the ControlFlow class" in {
      logger.debug("checking ControlFlow MCC")
      val measurements = new ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()
      c.graph.foreachVertex {
        case v: AstVertex =>
          var interesting = v.id.contains("ControlFlow") &&
            v.state.rangeStates.values.exists { value =>
              value.is('method)
            }
          if (interesting) {
            v.state.rangeStates.foreach { case (range, state) =>
              val name = state[Name].name
              measurements.add((name, range, 0, 0, 0, state[Mcc].n, 0))
            }
          }
        case _ =>
      }
      val expectedValues = Map(
        "singleIf" ->                               Map("[13-16]" -> 2),
        "twoNestedIfs" ->                           Map("[13-16]" -> 3),
        "twoConsecutiveIfs" ->                      Map("[13-16]" -> 3),
        "simpleIfTree" ->                           Map("[13-16]" -> 5),
        "complexIfTree" ->                          Map("[13-16]" -> 7),
        "orOperator" ->                             Map("[13-16]" -> 2),
        "orOperatorInsideIf" ->                     Map("[13-16]" -> 3),
        "twoOrOperatorsInsideIf" ->                 Map("[13-16]" -> 4),
        "ternaryOperator" ->                        Map("[13-16]" -> 2),
        "ternaryOperatorInsideIf" ->                Map("[13-16]" -> 3),
        "switchThreeCases" ->                       Map("[13-16]" -> 4),
        "nestedSwitchThreeTwoOneCases" ->           Map("[13-16]" -> 10),
        "consecutiveSwitchThreeTwoCases" ->         Map("[13-16]" -> 6),
        "consecutiveSwitchThreeTwoCasesNoBreaks" -> Map("[13-16]" -> 6),
        "singleFor" ->                              Map("[13-16]" -> 2),
        "twoNestedFors" ->                          Map("[13-16]" -> 3),
        "twoConsecutiveFors" ->                     Map("[13-16]" -> 3),
        "singleForWithContinue" ->                  Map("[13-16]" -> 2),
        "singleWhile" ->                            Map("[13-16]" -> 2),
        "twoNestedWhiles" ->                        Map("[13-16]" -> 3),
        "consecutiveWhiles" ->                      Map("[13-16]" -> 3),
        "singleWhileWithBreak" ->                   Map("[13-16]" -> 2),
        "singleWhileWithContinue" ->                Map("[13-16]" -> 2),
        "switchWithIfWhilesInCasesWithBreaks" ->    Map("[14-16]" -> 8),
        "switchWithIfWhilesInCasesWithoutBreaks" -> Map("[14-16]" -> 8),
        "complexMixedExample" ->                    Map("[13-13]" -> 16,
                                                        "[14-16]" -> 16)
      )
      checkValuesInt(measurements.toArray(Array[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()).toList, expectedValues,
                         { case (name, range, _, _, _, mcc, _) => (name, range, mcc)})
      success
    }


    "calculate correct MCC measures for classes" in {
      logger.debug("checking class MCC")
      val expectedValues = Map(
        "Main" ->                 Map("[1-5]" -> 5,
                                      "[6-6]" -> 5,
                                      "[7-12]" -> 5,
                                      "[13-16]" -> 5),
        "AbstractClass" ->        Map("[1-16]" -> 1),
        "CustomException" ->      Map("[1-16]" -> 1),
        "ConcreteClass" ->        Map("[1-1]" -> 6,
                                      "[2-2]" -> 6,
                                      "[3-4]" -> 6,
                                      "[5-15]" ->  4,
                                      "[16-16]" -> 5),
        "ConcreteSubclass" ->     Map("[1-16]" -> 1),
        "ControlFlow" ->          Map("[13-13]" -> 75,
                                      "[14-16]" -> 89),
        "InnerClass" ->           Map("[1-15]" -> 1,
                                      "[16-16]" -> 2),
        "InnerInnerClass" ->      Map("[1-15]" -> 1,
                                      "[16-16]" -> 2),
        "GenericPairInterface" -> Map("[1-16]" -> 1),
        "GenericPair" ->          Map("[1-16]" -> 1),
        "SubpackageClass" ->      Map("[1-16]" -> 1),
        "SubsubpackageClass" ->   Map("[1-16]" -> 1),
        "CodeSmell" ->            Map("[7-16]" -> 1),
        "BrainMethod" ->          Map("[7-7]" -> 1,
                                      "[8-8]" -> 17,
                                      "[9-9]" -> 16,
                                      "[10-13]" -> 16,
                                      "[14-16]" -> 7),
        "SiblingClass" ->         Map("[1-16]" -> 1)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, _, mcc, _) => (name, range, mcc)})
      success
    }

    persist(c)
    finalize(c)

  }

}

