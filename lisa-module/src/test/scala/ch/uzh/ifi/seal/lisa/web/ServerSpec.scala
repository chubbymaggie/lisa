package ch.uzh.ifi.seal.lisa.core.web

// TODO High Priority
//  * fix end bug in intro.js

// TODO When supporting method edges
//  * FANIN and FANOUT metrics
//  * Center of method nodes should show abstract/static etc.

// TODO Low Priority
//  * better differentiate 2nd-level nodes
//  * method calls to super overlap has-super inheritance edges
//  * Metrics by delta
//  * add pie to edge tooltip to show revisions where the edge is present?
//  * filter graph by selection of one node, show everything affected by that
//  * Number of methods in sector hover is taking the highest of all revs
//  * highlight focused rev or add clock hand when hovering over revisions in nav
//  * color mode where commit-dense (short ranges) are redder than long-lived commits
//  * When click(-dragg)ing nav sectors, restrict ranges to show only that single commit
//  * package-reselection when already selected doesn't really work
//  * When selecting package, recompute center force to draw towards mouse cursor

import java.nio.file.Files
import java.nio.file.Paths
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.core.public._
import ch.uzh.ifi.seal.lisa.module.parser.JavaASMParser
import ch.uzh.ifi.seal.lisa.module.parser.JavaASMGraph
import ch.uzh.ifi.seal.lisa.module.analysis.UniversalAnalysisSuite
import ch.uzh.ifi.seal.lisa.module.analysis.CKAnalysisSuite
import ch.uzh.ifi.seal.lisa.module.persistence.DummyPersistence

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec

class ServerSpec extends LisaSpec("javaSpec") {

  sequential

  //TODO: Generic ASM spec in place of LisaSpec
  val targetDir = s"/tmp/${id}"
  val parsers = List[Parser](JavaASMParser)

  override val analyses = UniversalAnalysisSuite + CKAnalysisSuite
  override val persistence = new DummyPersistence(targetDir)
  implicit override val domain = JavaASMGraph
  val binpath = "/tmp/git-ant-build-revisions/bin/"
  val sources = new JavaBytecodeAgent(parsers, binpath)
  //val sources = new JavaBytecodeAgent(parsers, "/home/user/squaad/apache-avro-v2/vers/")
  //val sources = new JavaBytecodeAgent(parsers, "/home/user/squaad/apache-commons-pool/vers/")
  //val sources = new JavaBytecodeAgent(parsers, "/home/user/squaad/apache-commons-codec/vers/")
  //val sources = new JavaBytecodeAgent(parsers, "/home/user/squaad/old/bcel/vers/")
  val c = makeComputationWithSources(parsers, sources)//, console = true)

  val bindataExists = Files.exists(Paths.get(binpath))
  if (!bindataExists) {
    logger.error("Binary files do not exist (use lisa-module/src/test/resources/git-ant-build-revisions.bash to create them)")
  }
  skipAllUnless(bindataExists)

  var s: Server = null;
  prepare(c)
  compute(c)

  "The web server should" >> {

    "start" in {
      s = new Server(c)
      success
    }

    "stop" in {
      //scala.io.StdIn.readLine
      s.shutdown()
      success
    }

  }

  step(finalize(c))
}

