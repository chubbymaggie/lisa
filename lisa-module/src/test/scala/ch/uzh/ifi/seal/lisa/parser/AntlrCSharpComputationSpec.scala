package ch.uzh.ifi.seal.lisa.parser

import scala.collection.mutable._
import java.util.concurrent.ConcurrentLinkedQueue

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.parser.AntlrCSharpParser
import ch.uzh.ifi.seal.lisa.module.parser.AntlrCSharpParseTree
import ch.uzh.ifi.seal.lisa.module.analysis.MccAnalysis.Mcc
import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name
import ch.uzh.ifi.seal.lisa.module.analysis.UniquePathsAnalysis.UniquePaths
import ch.uzh.ifi.seal.lisa.module.persistence.CSVPerRevisionParallelizedPersistence

class AntlrCSharpComputationSpec extends LisaSpec("csharpSpec") {

  override val persistence = new CSVPerRevisionParallelizedPersistence(resultDir)
  implicit override val domain = AntlrCSharpParseTree
  val c = makeComputation(List[Parser](AntlrCSharpParser))


  "The small computation should" >> {

    prepare(c)
    compute(c)

    "produce correct number of class aggregates" in {
      classAggregates = getClassAggregates(c.graph)
      logger.debug(s"class aggregates: ${classAggregates.size}")
      assert(classAggregates.size == 27)
      success
    }

    "calculate correct NOM measures for classes" in {
      logger.debug("checking NOM")
      val expectedValues = Map(
        "Hello" ->                Map("[1-1]" -> 1,
                                      "[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "AbstractClass" ->        Map("[2-2]" -> 3,
                                      "[3-3]" -> 3),
        "CustomException" ->      Map("[2-2]" -> 4,
                                      "[3-3]" -> 4),
        "ConcreteClass" ->        Map("[2-2]" -> 15,
                                      "[3-3]" -> 15),
        "ConcreteSubclass" ->     Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "ControlFlow" ->          Map("[3-3]" -> 26),
        "InnerClass" ->           Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "InnerInnerClass" ->      Map("[2-2]" -> 2,
                                      "[3-3]" -> 2),
        "GenericPairInterface" -> Map("[2-2]" -> 5,
                                      "[3-3]" -> 5),
        "GenericPair" ->          Map("[2-2]" -> 5,
                                      "[3-3]" -> 5),
        "SubpackageClass" ->      Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "SubsubpackageClass" ->   Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "CodeSmell" ->            Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "BrainMethod" ->          Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "SiblingClass" ->         Map("[3-3]" -> 1)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, nom, _, _, _, _) => (name, range, nom)})
      success
    }

    "calculate correct NumPara measures for classes" in {
      logger.debug("checking NumPara")
      val expectedValues = Map(
        "Hello" ->                Map("[1-1]" -> 1,
                                      "[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "AbstractClass" ->        Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "CustomException" ->      Map("[2-2]" -> 5,
                                      "[3-3]" -> 5),
        "ConcreteClass" ->        Map("[2-2]" -> 10,
                                      "[3-3]" -> 10),
        "ConcreteSubclass" ->     Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "ControlFlow" ->          Map("[3-3]" -> 0),
        "InnerClass" ->           Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "InnerInnerClass" ->      Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "GenericPairInterface" -> Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "GenericPair" ->          Map("[2-2]" -> 4,
                                      "[3-3]" -> 4),
        "SubpackageClass" ->      Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "SubsubpackageClass" ->   Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "CodeSmell" ->            Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "BrainMethod" ->          Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "SiblingClass" ->         Map("[3-3]" -> 0)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, numpara, _, _, _) => (name, range, numpara)})
      success
    }

    "calculate correct statement measures for classes" in {
      logger.debug("checking statements")
      val expectedValues = Map(
        "Hello" ->                Map("[1-1]" -> 4,
                                      "[2-2]" -> 29,
                                      "[3-3]" -> 34),
        "AbstractClass" ->        Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "CustomException" ->      Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "ConcreteClass" ->        Map("[2-2]" -> 13,
                                      "[3-3]" -> 13),
        "ConcreteSubclass" ->     Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "ControlFlow" ->          Map("[3-3]" -> 103), // or 86 like in Java?
        "InnerClass" ->           Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "InnerInnerClass" ->      Map("[2-2]" -> 4,
                                      "[3-3]" -> 4),
        "GenericPairInterface" -> Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "GenericPair" ->          Map("[2-2]" -> 6,
                                      "[3-3]" -> 6),
        "SubpackageClass" ->      Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "SubsubpackageClass" ->   Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "CodeSmell" ->            Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "BrainMethod" ->          Map("[2-2]" -> 21,
                                      "[3-3]" -> 21),
        "SiblingClass" ->         Map("[3-3]" -> 1)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, _, _, stmts) => (name, range, stmts)})
      success
    }

    "calculate correct NOV measures for classes" in {
      logger.debug("checking NOV")
      val expectedValues = Map(
        "Hello" ->                Map("[1-1]" -> 2,
                                      "[2-2]" -> 15,
                                      "[3-3]" -> 19),
        "AbstractClass" ->        Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "CustomException" ->      Map("[2-2]" -> 5,
                                      "[3-3]" -> 5),
        "ConcreteClass" ->        Map("[2-2]" -> 17,
                                      "[3-3]" -> 17),
        "ConcreteSubclass" ->     Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "ControlFlow" ->          Map("[3-3]" -> 15),
        "InnerClass" ->           Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "InnerInnerClass" ->      Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "GenericPairInterface" -> Map("[2-2]" -> 6,
                                      "[3-3]" -> 6),
        "GenericPair" ->          Map("[2-2]" -> 6,
                                      "[3-3]" -> 6),
        "SubpackageClass" ->      Map("[2-2]" -> 2,
                                      "[3-3]" -> 2),
        "SubsubpackageClass" ->   Map("[2-2]" -> 2,
                                      "[3-3]" -> 2),
        "CodeSmell" ->            Map("[2-2]" -> 0,
                                      "[3-3]" -> 0),
        "BrainMethod" ->          Map("[2-2]" -> 9,
                                      "[3-3]" -> 9),
        "SiblingClass" ->         Map("[3-3]" -> 2)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, nov, _, _) => (name, range, nov)})
      success
    }

    "calculate correct uniquePaths measures for methods of the ControlFlow class" in {
      logger.debug("checking uniquePaths")
      val measurements = new ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()
      c.graph.foreachVertex {
        case v: AstVertex =>
          var interesting = v.id.contains("ControlFlow") &&
            v.state.rangeStates.values.exists { value =>
              value.is('method)
            }
          if (interesting) {
            v.state.rangeStates.foreach { case (range, state) =>
              val name = state[Name].name
              measurements.add((name, range, 0, 0, 0, state[UniquePaths].n, 0))
            }
          }
        case _ =>
      }
      val expectedValues = Map(
        "singleIf" ->                               Map("[3-3]" -> BigInt(2)),
        "twoNestedIfs" ->                           Map("[3-3]" -> BigInt(3)),
        "twoConsecutiveIfs" ->                      Map("[3-3]" -> BigInt(4)),
        "simpleIfTree" ->                           Map("[3-3]" -> BigInt(10)),
        "complexIfTree" ->                          Map("[3-3]" -> BigInt(30)),
        "orOperator" ->                             Map("[3-3]" -> BigInt(2)),
        "orOperatorInsideIf" ->                     Map("[3-3]" -> BigInt(3)),
        "ternaryOperator" ->                        Map("[3-3]" -> BigInt(2)),
        "twoOrOperatorsInsideIf" ->                 Map("[3-3]" -> BigInt(4)),
        "ternaryOperatorInsideIf" ->                Map("[3-3]" -> BigInt(3)),
        "switchThreeCases" ->                       Map("[3-3]" -> BigInt(4)),
        "nestedSwitchThreeTwoOneCases" ->           Map("[3-3]" -> BigInt(10)),
        "consecutiveSwitchThreeTwoCases" ->         Map("[3-3]" -> BigInt(12)),
        "consecutiveSwitchThreeTwoCasesNoBreaks" -> Map("[3-3]" -> BigInt(12)),
        "singleFor" ->                              Map("[3-3]" -> BigInt(2)),
        "twoNestedFors" ->                          Map("[3-3]" -> BigInt(3)),
        "twoConsecutiveFors" ->                     Map("[3-3]" -> BigInt(4)),
        "singleForWithContinue" ->                  Map("[3-3]" -> BigInt(2)),
        "singleWhile" ->                            Map("[3-3]" -> BigInt(2)),
        "twoNestedWhiles" ->                        Map("[3-3]" -> BigInt(3)),
        "consecutiveWhiles" ->                      Map("[3-3]" -> BigInt(4)),
        "singleWhileWithBreak" ->                   Map("[3-3]" -> BigInt(2)),
        "singleWhileWithContinue" ->                Map("[3-3]" -> BigInt(2)),
        "switchWithIfWhilesInCasesWithBreaks" ->    Map("[3-3]" -> BigInt(8)),
        // fallthrough cases are not treated seperately
        "switchWithIfWhilesInCasesWithoutBreaks" -> Map("[3-3]" -> BigInt(8)),
        "complexMixedExample" ->                    Map("[3-3]" -> BigInt(252),
                                                        "[3-3]" -> BigInt(252))
      )
      checkValues(measurements.toArray(Array[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()).toList, expectedValues,
                         { case (name, range, _, _, _, uniquePaths, _) => (name, range, uniquePaths)})
      success
    }

    "calculate correct MCC measures for methods of the ControlFlow class" in {
      logger.debug("checking ControlFlow MCC")
      val measurements = new ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()
      c.graph.foreachVertex {
        case v: AstVertex =>
          var interesting = v.id.contains("ControlFlow") &&
            v.state.rangeStates.values.exists { value =>
              value.is('method)
            }
          if (interesting) {
            v.state.rangeStates.foreach { case (range, state) =>
              val name = state[Name].name
              measurements.add((name, range, 0, 0, 0, state[Mcc].n, 0))
            }
          }
        case _ =>
      }
      val expectedValues = Map(
        "singleIf" ->                               Map("[3-3]" -> 2),
        "twoNestedIfs" ->                           Map("[3-3]" -> 3),
        "twoConsecutiveIfs" ->                      Map("[3-3]" -> 3),
        "simpleIfTree" ->                           Map("[3-3]" -> 5),
        "complexIfTree" ->                          Map("[3-3]" -> 7),
        "orOperator" ->                             Map("[3-3]" -> 2),
        "orOperatorInsideIf" ->                     Map("[3-3]" -> 3),
        "twoOrOperatorsInsideIf" ->                 Map("[3-3]" -> 4),
        "ternaryOperator" ->                        Map("[3-3]" -> 2),
        "ternaryOperatorInsideIf" ->                Map("[3-3]" -> 3),
        "switchThreeCases" ->                       Map("[3-3]" -> 4),
        "nestedSwitchThreeTwoOneCases" ->           Map("[3-3]" -> 10),
        "consecutiveSwitchThreeTwoCases" ->         Map("[3-3]" -> 6),
        "consecutiveSwitchThreeTwoCasesNoBreaks" -> Map("[3-3]" -> 6),
        "singleFor" ->                              Map("[3-3]" -> 2),
        "twoNestedFors" ->                          Map("[3-3]" -> 3),
        "twoConsecutiveFors" ->                     Map("[3-3]" -> 3),
        "singleForWithContinue" ->                  Map("[3-3]" -> 2),
        "singleWhile" ->                            Map("[3-3]" -> 2),
        "twoNestedWhiles" ->                        Map("[3-3]" -> 3),
        "consecutiveWhiles" ->                      Map("[3-3]" -> 3),
        "singleWhileWithBreak" ->                   Map("[3-3]" -> 2),
        "singleWhileWithContinue" ->                Map("[3-3]" -> 2),
        "switchWithIfWhilesInCasesWithBreaks" ->    Map("[3-3]" -> 8),
        "switchWithIfWhilesInCasesWithoutBreaks" -> Map("[3-3]" -> 8),
        "complexMixedExample" ->                    Map("[3-3]" -> 16,
                                                        "[3-3]" -> 16)
      )
      checkValuesInt(measurements.toArray(Array[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()).toList, expectedValues,
                         { case (name, range, _, _, _, mcc, _) => (name, range, mcc)})
      success
    }


    "calculate correct MCC measures for classes" in {
      logger.debug("checking class MCC")
      val expectedValues = Map(
        "Hello" ->                Map("[1-1]" -> 2,
                                      "[2-2]" -> 5,
                                      "[3-3]" -> 5),
        "AbstractClass" ->        Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "CustomException" ->      Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "ConcreteClass" ->        Map("[2-2]" -> 5,
                                      "[3-3]" -> 5),
        "ConcreteSubclass" ->     Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "ControlFlow" ->          Map("[3-3]" -> 89),
        "InnerClass" ->           Map("[2-2]" -> 2,
                                      "[3-3]" -> 2),
        "InnerInnerClass" ->      Map("[2-2]" -> 2,
                                      "[3-3]" -> 2),
        "GenericPairInterface" -> Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "GenericPair" ->          Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "SubpackageClass" ->      Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "SubsubpackageClass" ->   Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "CodeSmell" ->            Map("[2-2]" -> 1,
                                      "[3-3]" -> 1),
        "BrainMethod" ->          Map("[2-2]" -> 7,
                                      "[3-3]" -> 7),
        "SiblingClass" ->         Map("[3-3]" -> 1)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, _, _, _, mcc, _) => (name, range, mcc)})
      success
    }

    persist(c)
    finalize(c)

  }


}

