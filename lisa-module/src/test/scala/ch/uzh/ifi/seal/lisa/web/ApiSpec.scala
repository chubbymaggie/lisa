package ch.uzh.ifi.seal.lisa.core.web

import java.nio.file.Files
import java.nio.file.Paths
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.core.public._
import ch.uzh.ifi.seal.lisa.module.parser.JavaASMParser
import ch.uzh.ifi.seal.lisa.module.parser.JavaASMGraph
import ch.uzh.ifi.seal.lisa.module.analysis.UniversalAnalysisSuite
import ch.uzh.ifi.seal.lisa.module.persistence.DummyPersistence

import spray.json._
import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec

class ApiSpec extends LisaSpec("javaASMSpec") {

  //TODO: Generic ASM spec in place of LisaSpec
  val targetDir = s"/tmp/${id}"
  val parsers = List[Parser](JavaASMParser)

  override val analyses = UniversalAnalysisSuite
  override val persistence = new DummyPersistence(targetDir)
  implicit override val domain = JavaASMGraph
  val binpath = "/tmp/git-ant-build-revisions/bin/"
  val sources = new JavaBytecodeAgent(parsers, binpath)
  //val sources = new JavaBytecodeAgent(parsers, "/home/user/squaad/avro/vers/")
  val c = makeComputationWithSources(parsers, sources)//, console = true)

  val bindataExists = Files.exists(Paths.get(binpath))
  if (!bindataExists) {
    logger.error("Binary files do not exist (use lisa-module/src/test/resources/git-ant-build-revisions.bash to create them)")
  }
  skipAllUnless(bindataExists)

  val api = new Api(c)
  prepare(c)
  compute(c)

  "The API should" >> {
    "load the graph" in {
      val res = api.dispatch("graph.json")
      res match {
        case JsObject(fields) =>
          //fields.get("classes").map { cs => /*println(cs)*/ } // TODO write check
          //fields.get("methods").map { cs => println(cs) } // TODO write check
          //fields.get("n_ranges").map { cs => println(cs) } // TODO write check
        case _ =>
      }
      success
    }
  }

  step(finalize(c))
}

