package ch.uzh.ifi.seal.lisa.parser

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavascriptParser
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavascriptParseTree

class AntlrJavascriptMinifiedSpec extends LisaSpec("javascriptMinifiedSpec") {

  implicit override val domain = AntlrJavascriptParseTree
  val c = makeComputation(List[Parser](AntlrJavascriptParser))

  "The small computation should" >> {

    prepare(c)
    compute(c)
    persist(c)
    finalize(c)

  }

}

