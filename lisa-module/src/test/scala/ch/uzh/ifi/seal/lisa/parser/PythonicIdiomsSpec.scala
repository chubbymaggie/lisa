package ch.uzh.ifi.seal.lisa.parser

import scala.collection.immutable.SortedMap
import scala.collection.mutable.{Map => MMap}
import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.analysis._
import ch.uzh.ifi.seal.lisa.module.parser.PythonNativeParser
import ch.uzh.ifi.seal.lisa.module.persistence.CSVPerRevisionParallelizedPersistence

class PythonicIdiomsSpec extends LisaSpec("pythonicIdiomsSpec") {

  sequential

  override val analyses = PythonicAnalysisSuite + UniversalAnalysisSuite
  override val persistence = new CSVPerRevisionParallelizedPersistence(resultDir)
  val c = makeComputation(List[Parser](PythonNativeParser))
  var data = MMap[String, AnalysisState]()
  var revisions = SortedMap[Int, Revision]()

  "The python native parser should" >> {
    prepare(c)
    compute(c)
    "produce data" in {
      revisions = c.sources.getRevisions.get
      data = getRevisionData(c.graph, revisions.last._2)
      data.size must beGreaterThan(0)
      revisions.size must beGreaterThan(0)
    }

    import ch.uzh.ifi.seal.lisa.module.analysis.NodeTypeCountersAnalysis.NodeTypeCounters
    "find list comprehensions" in {
      data("/src/idioms/list_comprehension.py") must have(_[NodeTypeCounters].counters.get("ListComp") mustEqual Some(1))
    }
    "find dict comprehensions" in {
      data("/src/idioms/dict_comprehension.py") must have(_[NodeTypeCounters].counters.get("DictCompargs") mustEqual Some(1))
    }
    "find generator expressions" in {
      data("/src/idioms/generator_expression.py") must have(_[NodeTypeCounters].counters.get("GeneratorExpargs") mustEqual Some(1))
    }
    "find lambdas" in {
      data("/src/idioms/lambda.py") must have(_[NodeTypeCounters].counters.get("Lambdaargs") mustEqual Some(1))
    }
    "find 'yield' usage" in {
      data("/src/idioms/yield.py") must have(_[NodeTypeCounters].counters.get("Yield") mustEqual Some(1))
    }
    "find 'finally' usage" in {
      data("/src/idioms/finally.py") must have(_[NodeTypeCounters].counters.get("Exprfinalbody") mustEqual Some(1))
    }
    "find 'with' statements" in {
      data("/src/idioms/with.py") must have(_[NodeTypeCounters].counters.get("Withbody") mustEqual Some(1))
    }
    "find decorator usage" in {
      data("/src/idioms/decorator.py") must have(_[NodeTypeCounters].counters.get("Namedecorator_list") mustEqual Some(2))
    }

    import ch.uzh.ifi.seal.lisa.module.analysis.CallNameCountersAnalysis.CallNameCounters
    "find 'namedtuple' usage" in {
      data("/src/idioms/namedtuple.py") must have(_[CallNameCounters].counters.get("namedtuple") mustEqual Some(1))
    }
    "find 'defaultdict' usage" in {
      data("/src/idioms/defaultdict.py") must have(_[CallNameCounters].counters.get("defaultdict") mustEqual Some(1))
    }
    "find 'ordereddict' usage" in {
      data("/src/idioms/ordereddict.py") must have(_[CallNameCounters].counters.get("OrderedDict") mustEqual Some(1))
    }
    "find 'deque' usage" in {
      data("/src/idioms/deque.py") must have(_[CallNameCounters].counters.get("deque") mustEqual Some(1))
    }
    "find 'Counter' usage" in {
      data("/src/idioms/counter.py") must have(_[CallNameCounters].counters.get("Counter") mustEqual Some(1))
    }
    "find 'enumerate' usage" in {
      data("/src/idioms/enumerate.py") must have(_[CallNameCounters].counters.get("enumerate") mustEqual Some(1))
    }
    "find 'zip' usage" in {
      data("/src/idioms/zip.py") must have(_[CallNameCounters].counters.get("zip") mustEqual Some(1))
    }
    "find 'map' usage" in {
      data("/src/idioms/map.py") must have(_[CallNameCounters].counters.get("map") mustEqual Some(1))
    }
    "find 'filter' usage" in {
      data("/src/idioms/filter.py") must have(_[CallNameCounters].counters.get("filter") mustEqual Some(1))
    }
    "find 'zip_longest' usage" in {
      data("/src/idioms/itertools.py") must have(_[CallNameCounters].counters.get("zip_longest") mustEqual Some(1))
    }
    "find 'starmap' usage" in {
      data("/src/idioms/itertools.py") must have(_[CallNameCounters].counters.get("starmap") mustEqual Some(1))
    }
    "find 'tee' usage" in {
      data("/src/idioms/itertools.py") must have(_[CallNameCounters].counters.get("tee") mustEqual Some(1))
    }
    "find 'groupby' usage" in {
      data("/src/idioms/itertools.py") must have(_[CallNameCounters].counters.get("groupby") mustEqual Some(1))
    }


    import ch.uzh.ifi.seal.lisa.module.analysis.DecoratorAnalysis.DecoratorCounters
    "find classmethod usage" in {
      data("/src/idioms/classmethod.py") must have(_[DecoratorCounters].counters.get("classmethod") mustEqual Some(1))
    }
    "find staticmethod usage" in {
      data("/src/idioms/staticmethod.py") must have(_[DecoratorCounters].counters.get("staticmethod") mustEqual Some(1))
    }
    "find total_ordering usage" in {
      data("/src/idioms/total_ordering.py") must have(_[DecoratorCounters].counters.get("total_ordering") mustEqual Some(1))
    }

    import ch.uzh.ifi.seal.lisa.module.analysis.MagicMethodsAnalysis.MagicMethods
    "find magic methods" in {
      data("/src/idioms/magic_methods.py") must have(_[MagicMethods].level1 mustEqual 1)
      data("/src/idioms/magic_methods.py") must have(_[MagicMethods].level2 mustEqual 2)
      data("/src/idioms/magic_methods.py") must have(_[MagicMethods].level3 mustEqual 1)
    }

    import ch.uzh.ifi.seal.lisa.module.analysis.NestedFunctionCountAnalysis.NestedFunctionCount
    "find nested functions" in {
      data("/src/idioms/inner_function.py") must have(_[NestedFunctionCount].n mustEqual 1)
    }

    import ch.uzh.ifi.seal.lisa.module.analysis.ClassCountAnalysis.ClassCount
    "count classes correctly" in {
      data("/src/misc/classes.py") must have(_[ClassCount].n mustEqual 4)
    }

    import ch.uzh.ifi.seal.lisa.module.analysis.MethodCountAnalysis.MethodCount
    "count methods correctly" in {
      data("/src/idioms/magic_methods.py/Module0/ClassDefbody0/") must have(_[MethodCount].n mustEqual 5)
      data("/src/misc/classes.py/Module0/ClassDefbody0/") must have(_[MethodCount].n mustEqual 2)
      data("/src/misc/classes.py/Module0/ClassDefbody1/") must have(_[MethodCount].n mustEqual 2)
      data("/src/misc/classes.py/Module0/ClassDefbody2/") must have(_[MethodCount].n mustEqual 1)
      data("/src/misc/classes.py/Module0/ClassDefbody3/") must have(_[MethodCount].n mustEqual 1)
    }

    persist(c)
    finalize(c)
  }

}

