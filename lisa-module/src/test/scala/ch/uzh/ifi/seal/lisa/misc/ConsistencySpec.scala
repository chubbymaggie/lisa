package ch.uzh.ifi.seal.lisa.misc

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavaParser
import ch.uzh.ifi.seal.lisa.module.parser.AntlrJavaParseTree

class ConsistencySpec extends LisaSpec("consistencySpec") {

  sequential

  implicit override val domain = AntlrJavaParseTree
  val parsers = List[Parser](AntlrJavaParser)

  val c1 = makeComputation(List[Parser](AntlrJavaParser))
  val c2 = makeComputation(List[Parser](AntlrJavaParser))
  val c3 = makeComputation(List[Parser](AntlrJavaParser))

  var classAggregates1 = List[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()
  var classAggregates2 = List[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()
  var classAggregates3 = List[(String,RevisionRange,Int,Int,Int,BigInt,Int)]()

  "The Computation should consistently" >> {

    "analyze multiple commits (run 1)" in {
      prepare(c1)
      compute(c1)
    }

    "re-analyze the same commits, for repetition consistency (run 2) " in {
      prepare(c2)
      compute(c2)
    }

    "analyze only the last of these commits, for incremental consistency (run 3)" in {
      prepare(c3)
      compute(c3)
    }

    "Calcualte aggregates for all runs" in {
      classAggregates1 = getClassAggregates(c1.graph)
      classAggregates2 = getClassAggregates(c2.graph)
      classAggregates3 = getClassAggregates(c3.graph)
      success
    }

    "Calculate the same results for runs 1 & 2" in {
      classAggregates1.sorted == classAggregates2.sorted
      success
    }

    "Calculate the results in run 3 that match those of runs 1 & 2" in {
      val endCommitAggregates = classAggregates1.filter { it =>
        it._2.end.rev == endCommit
      }
      val previousResults = endCommitAggregates.map { it =>
        (it._1, it._3, it._4, it._5)
      }
      val singleResults = classAggregates3.map { it =>
        (it._1, it._3, it._4, it._5)
      }
      previousResults.sorted == singleResults.sorted
      success
    }

    "finalize the test" in {
      finalize(c1)
      finalize(c2)
      finalize(c3)
      success
    }


  }

}

