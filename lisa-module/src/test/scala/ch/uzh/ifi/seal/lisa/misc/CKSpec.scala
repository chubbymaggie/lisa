package ch.uzh.ifi.seal.lisa.module.misc

import scala.collection.mutable._

import java.util.concurrent.ConcurrentLinkedQueue
import java.nio.file.Files
import java.nio.file.Paths
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.core.public._
import ch.uzh.ifi.seal.lisa.module.parser.JavaASMParser
import ch.uzh.ifi.seal.lisa.module.parser.JavaASMGraph
import ch.uzh.ifi.seal.lisa.module.analysis.CKAnalysisSuite
import com.signalcollect._

import ch.uzh.ifi.seal.lisa.module.analysis.NameAnalysis.Name
import ch.uzh.ifi.seal.lisa.module.analysis.WeightedMethodCountAnalysis.CKWMC
import ch.uzh.ifi.seal.lisa.module.analysis.DepthOfInheritanceTreeAnalysis.CKDIT
import ch.uzh.ifi.seal.lisa.module.analysis.NumberOfChildrenAnalysis.CKNOC
import ch.uzh.ifi.seal.lisa.module.analysis.CouplingBetweenObjectClassesAnalysis.CKCBO
import ch.uzh.ifi.seal.lisa.module.analysis.ResponseForClassAnalysis.CKRFC
import ch.uzh.ifi.seal.lisa.module.analysis.LackOfCohesionInMethodsAnalysis.CKLCOM

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec

class CKSpec extends LisaSpec("javaASMSpec") {

  sequential

  val targetDir = s"/tmp/${id}"
  val parsers = List[Parser](JavaASMParser)

  override val analyses = CKAnalysisSuite
  override val persistence = new DummyPersistence(targetDir)
  implicit override val domain = JavaASMGraph
  val binpath = "/tmp/git-ant-build-revisions/bin/"
  val sources = new JavaBytecodeAgent(parsers, binpath)
  //val sources = new JavaBytecodeAgent(parsers, "/home/user/squaad/avro/vers/")
  val c = makeComputationWithSources(parsers, sources)//, console = true)

  val bindataExists = Files.exists(Paths.get(binpath))
  if (!bindataExists) {
    logger.error("Binary files do not exist (use lisa-module/src/test/resources/git-ant-build-revisions.bash to create them)")
  }
  skipAllUnless(bindataExists)

  var classAggregatesASM = List[(String,RevisionRange,Int,Int,Int,Int,Int,Int)]()

  def aggregateVertexASM(v: BaseVertex, aggregate: ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,Int,Int,Int)]) {
    var shouldAggregate = v.state.rangeStates.values.exists { state =>
      state is ('class)
    }
    if (shouldAggregate) {
      v.state.rangeStates.foreach { case (range, state) =>
        val name = state[Name].name
        if (state is 'class) {
          aggregate.add((
            name, range,
            state[CKWMC].n,
            state[CKDIT].n,
            state[CKNOC].n,
            state[CKCBO].efferentCoupledClasses.size,
            state[CKRFC].responseSet.size,
            state[CKLCOM].n))
        }
      }
    }
  }

  def getClassAggregatesASM(graph: Graph[Any,Any]): List[(String,RevisionRange,Int,Int,Int,Int,Int,Int)] = {
    val aggregate = new ConcurrentLinkedQueue[(String,RevisionRange,Int,Int,Int,Int,Int,Int)]()
    graph.foreachVertex {
      case v: AstVertex => aggregateVertexASM(v, aggregate)
      case v: ObjectVertex => aggregateVertexASM(v, aggregate)
      case _ =>
    }
    aggregate.toArray(Array[(String,RevisionRange,Int,Int,Int,Int,Int,Int)]()).toList
  }

  def checkValuesIntASM(
          aggregates: List[(String,RevisionRange,Int,Int,Int,Int,Int,Int)],
          expectedValues: Map[String,Map[String,Int]],
          extractor: ((String, RevisionRange, Int,Int,Int,Int,Int,Int)) =>
                     (String, RevisionRange, BigInt)) = {
    checkValuesASM(aggregates, expectedValues.map{ case(s, m) =>
      (s, m.map { case (ss, mm) => (ss, BigInt(mm)) })
    }, extractor)
  }

  def checkValuesASM(
          aggregates: List[(String,RevisionRange,Int,Int,Int,Int,Int,Int)],
          expectedValues: Map[String,Map[String,BigInt]],
          extractor: ((String, RevisionRange, Int,Int,Int,Int,Int,Int)) => 
                     (String, RevisionRange, BigInt)) = {
    aggregates.foreach { it =>
      val (name, range, actualValue) = extractor(it)
      val expected = expectedValues.get(name)
      expected match {
        case Some(values) =>
          val rangeValue = values.get(range.toString)
          rangeValue match {
            case Some(v) =>
              if (actualValue != v) {
                logger.error(s"$actualValue should be $v for element $name in range $range")
              }
              assert(actualValue == v, s"for element $name in range $range")
            case _ => 
              logger.error(s"missing expected range value for class " +
                              s"$name in range $range (got $actualValue)")
              failure(s"missing expected range value for class " +
                              s"$name in range $range (got $actualValue)")
          }
        case _ => failure(s"missing expected values in test for element $name")
      }
    }
  }




  "The CK Metrics should" >> {

    prepare(c)
    compute(c)

    "produce correct number of class aggregates" in {
      classAggregatesASM = getClassAggregatesASM(c.graph)
      logger.info(s"class aggregates: ${classAggregatesASM.size}")
      assert(classAggregatesASM.size == 40)
      success
    }
    "calculate correct WMC measures for classes" in {
      val expectedValues = Map(
        "org/ifi/seal/example/package-info" ->
          Map("[1-10]" -> 0,
              "[11-11]" -> 0,
              "[12-23]" -> 0),
        "org/ifi/seal/example/Main" ->
          Map("[1-3]" -> 3,
              "[4-5]" -> 3,
              "[6-6]" -> 3,
              "[7-12]" -> 3,
              "[13-16]" -> 3,
              "[17-18]" -> 3,
              "[19-19]" -> 3,
              "[20-20]" -> 3,
              "[21-21]" -> 3,
              "[22-23]" -> 3),
        "org/ifi/seal/example/AbstractClass" ->
          Map("[1-23]" -> 2),
        "org/ifi/seal/example/BaseInterface" ->
          Map("[1-22]" -> 1,
              "[23-23]" -> 1),
        "org/ifi/seal/example/ChildInterface" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/CustomException" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/ConcreteClass" ->
          Map("[1-1]" -> 15,
              "[2-2]" -> 17,
              "[3-13]" -> 16,
              "[14-17]" -> 16,
              "[18-18]" -> 19,
              "[19-21]" -> 20,
              "[22-23]" -> 19),
        "org/ifi/seal/example/ConcreteSubclass" ->
          Map("[1-23]" -> 2),
        "org/ifi/seal/example/ControlFlow" ->
          Map("[13-13]" -> 25,
              "[14-18]" -> 27,
              "[19-21]" -> 27,
              "[22-23]" -> 27),
        "org/ifi/seal/example/ConcreteClass$InnerClass" ->
          Map("[1-15]" -> 1,
              "[16-23]" -> 2),
        "org/ifi/seal/example/ConcreteClass$InnerClass$InnerInnerClass" ->
          Map("[1-15]" -> 1,
              "[16-23]" -> 3),
        "org/ifi/seal/example/Demo" ->
          Map("[17-23]" -> 2),
        "org/ifi/seal/example/GenericPairInterface" ->
          Map("[1-23]" -> 2),
        "org/ifi/seal/example/GenericPair" ->
          Map("[1-23]" -> 5),
        "org/ifi/seal/example/subpackage/SubpackageClass" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/subpackage/subsubpackage/SubsubpackageClass" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/codesmells/CodeSmell" ->
          Map("[7-22]" -> 1,
              "[23-23]" -> 1),
        "org/ifi/seal/example/codesmells/BrainMethod" ->
          Map("[7-7]" -> 2,
              "[8-8]" -> 2,
              "[9-9]" -> 2,
              "[10-22]" -> 2,
              "[23-23]" -> 3),
        "org/ifi/seal/example_sibling/SiblingClass" ->
          Map("[1-23]" -> 1)
      )
      checkValuesIntASM(classAggregatesASM, expectedValues,
                         { case (name, range, ckwmc, _, _, _, _, _) => (name, range, ckwmc)})
      success
    }

    "calculate correct DIT measures for classes" in {
      val expectedValues = Map(
        "org/ifi/seal/example/package-info" ->
          Map("[1-10]" -> 0,
              "[11-11]" -> 0,
              "[12-23]" -> 0),
        "org/ifi/seal/example/Main" ->
          Map("[1-3]" -> 0,
              "[4-5]" -> 0,
              "[6-6]" -> 0,
              "[7-12]" -> 0,
              "[13-16]" -> 0,
              "[17-18]" -> 0,
              "[19-19]" -> 0,
              "[20-20]" -> 0,
              "[21-21]" -> 0,
              "[22-23]" -> 0),
        "org/ifi/seal/example/AbstractClass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/BaseInterface" ->
          Map("[1-22]" -> 0,
              "[23-23]" -> 0),
        "org/ifi/seal/example/ChildInterface" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/CustomException" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/ConcreteClass" ->
          Map("[1-1]" -> 1,
              "[2-2]" -> 1,
              "[3-13]" -> 1,
              "[14-17]" -> 1,
              "[18-18]" -> 1,
              "[19-21]" -> 1,
              "[22-23]" -> 1),
        "org/ifi/seal/example/ConcreteSubclass" ->
          Map("[1-23]" -> 2),
        "org/ifi/seal/example/ControlFlow" ->
          Map("[13-13]" -> 0,
              "[14-18]" -> 0,
              "[19-21]" -> 0,
              "[22-23]" -> 0),
        "org/ifi/seal/example/ConcreteClass$InnerClass" ->
          Map("[1-15]" -> 0,
              "[16-23]" -> 0),
        "org/ifi/seal/example/ConcreteClass$InnerClass$InnerInnerClass" ->
          Map("[1-15]" -> 0,
              "[16-23]" -> 0),
        "org/ifi/seal/example/Demo" ->
          Map("[17-23]" -> 0),
        "org/ifi/seal/example/GenericPairInterface" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/GenericPair" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/subpackage/SubpackageClass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/subpackage/subsubpackage/SubsubpackageClass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/codesmells/CodeSmell" ->
          Map("[7-22]" -> 0,
              "[23-23]" -> 0),
        "org/ifi/seal/example/codesmells/BrainMethod" ->
          Map("[7-7]" -> 1,
              "[8-8]" -> 1,
              "[9-9]" -> 1,
              "[10-22]" -> 1,
              "[23-23]" -> 1),
        "org/ifi/seal/example_sibling/SiblingClass" ->
          Map("[1-23]" -> 0)
      )
      checkValuesIntASM(classAggregatesASM, expectedValues,
                         { case (name, range, _, dit, _, _, _, _) => (name, range, dit)})
      success
    }

    "calculate correct NOC measures for classes" in {
      val expectedValues = Map(
        "org/ifi/seal/example/package-info" ->
          Map("[1-10]" -> 0,
              "[11-11]" -> 0,
              "[12-23]" -> 0),
        "org/ifi/seal/example/Main" ->
          Map("[1-3]" -> 0,
              "[4-5]" -> 0,
              "[6-6]" -> 0,
              "[7-12]" -> 0,
              "[13-16]" -> 0,
              "[17-18]" -> 0,
              "[19-19]" -> 0,
              "[20-20]" -> 0,
              "[21-21]" -> 0,
              "[22-23]" -> 0),
        "org/ifi/seal/example/AbstractClass" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/BaseInterface" ->
          Map("[1-22]" -> 1,
              "[23-23]" -> 1),
        "org/ifi/seal/example/ChildInterface" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/CustomException" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/ConcreteClass" ->
          Map("[1-1]" -> 1,
              "[2-2]" -> 1,
              "[3-13]" -> 1,
              "[14-17]" -> 1,
              "[18-18]" -> 1,
              "[19-21]" -> 1,
              "[22-23]" -> 1),
        "org/ifi/seal/example/ConcreteSubclass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/ControlFlow" ->
          Map("[13-13]" -> 0,
              "[14-18]" -> 0,
              "[19-21]" -> 0,
              "[22-23]" -> 0),
        "org/ifi/seal/example/ConcreteClass$InnerClass" ->
          Map("[1-15]" -> 0,
              "[16-23]" -> 0),
        "org/ifi/seal/example/ConcreteClass$InnerClass$InnerInnerClass" ->
          Map("[1-15]" -> 0,
              "[16-23]" -> 0),
        "org/ifi/seal/example/Demo" ->
          Map("[17-23]" -> 0),
        "org/ifi/seal/example/GenericPairInterface" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/GenericPair" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/subpackage/SubpackageClass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/subpackage/subsubpackage/SubsubpackageClass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/codesmells/CodeSmell" ->
          Map("[7-22]" -> 1,
              "[23-23]" -> 1),
        "org/ifi/seal/example/codesmells/BrainMethod" ->
          Map("[7-7]" -> 0,
              "[8-8]" -> 0,
              "[9-9]" -> 0,
              "[10-22]" -> 0,
              "[23-23]" -> 0),
        "org/ifi/seal/example_sibling/SiblingClass" ->
          Map("[1-23]" -> 0)
      )
      checkValuesIntASM(classAggregatesASM, expectedValues,
                         { case (name, range, _, _, noc, _, _, _) => (name, range, noc)})
      success
    }


    "calculate correct CBO measures for classes" in {
      val expectedValues = Map(
        "org/ifi/seal/example/package-info" ->
          Map("[1-10]" -> 0,
              "[11-11]" -> 0,
              "[12-23]" -> 0),
        "org/ifi/seal/example/Main" ->
          Map("[1-3]" -> 6,
              "[4-5]" -> 6,
              "[6-6]" -> 6,
              "[7-12]" -> 7,
              "[13-16]" -> 8,
              "[17-18]" -> 9,
              "[19-19]" -> 9,
              "[20-20]" -> 9,
              "[21-21]" -> 9,
              "[22-23]" -> 9),
        "org/ifi/seal/example/AbstractClass" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/BaseInterface" ->
          Map("[1-22]" -> 1,
              "[23-23]" -> 1),
        "org/ifi/seal/example/ChildInterface" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/CustomException" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/ConcreteClass" ->
          Map("[1-1]" -> 3,
              "[2-2]" -> 3,
              "[3-13]" -> 3,
              "[14-17]" -> 3,
              "[18-18]" -> 3,
              "[19-21]" -> 4,
              "[22-23]" -> 3),
        "org/ifi/seal/example/ConcreteSubclass" ->
          Map("[1-23]" -> 2),
        "org/ifi/seal/example/ControlFlow" ->
          Map("[13-13]" -> 1,
              "[14-18]" -> 1,
              "[19-21]" -> 2,
              "[22-23]" -> 1),
        "org/ifi/seal/example/ConcreteClass$InnerClass" ->
          Map("[1-15]" -> 1,
              "[16-23]" -> 1),
        "org/ifi/seal/example/ConcreteClass$InnerClass$InnerInnerClass" ->
          Map("[1-15]" -> 1,
              "[16-23]" -> 1),
        "org/ifi/seal/example/Demo" ->
          Map("[17-23]" -> 1),
        "org/ifi/seal/example/GenericPairInterface" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/GenericPair" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/subpackage/SubpackageClass" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/subpackage/subsubpackage/SubsubpackageClass" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/codesmells/CodeSmell" ->
          Map("[7-22]" -> 1,
              "[23-23]" -> 1),
        "org/ifi/seal/example/codesmells/BrainMethod" ->
          Map("[7-7]" -> 2,
              "[8-8]" -> 2,
              "[9-9]" -> 2,
              "[10-22]" -> 2,
              "[23-23]" -> 2),
        "org/ifi/seal/example_sibling/SiblingClass" ->
          Map("[1-23]" -> 1)
      )
      checkValuesIntASM(classAggregatesASM, expectedValues,
                         { case (name, range, _, _, _, cbo, _, _) => (name, range, cbo)})
      success
    }


    "calculate correct RFC measures for classes" in {
      val expectedValues = Map(
        "org/ifi/seal/example/package-info" ->
          Map("[1-10]" -> 0,
              "[11-11]" -> 0,
              "[12-23]" -> 0),
        "org/ifi/seal/example/Main" ->
          Map("[1-3]" -> 15,
              "[4-5]" -> 15,
              "[6-6]" -> 15,
              "[7-12]" -> 15,
              "[13-16]" -> 15,
              "[17-18]" -> 17,
              "[19-19]" -> 18,
              "[20-20]" -> 21,
              "[21-21]" -> 18,
              "[22-23]" -> 17),
        "org/ifi/seal/example/AbstractClass" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/BaseInterface" ->
          Map("[1-22]" -> 1,
              "[23-23]" -> 1),
        "org/ifi/seal/example/ChildInterface" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/CustomException" ->
          Map("[1-23]" -> 2),
        "org/ifi/seal/example/ConcreteClass" ->
          Map("[1-1]" -> 15,
              "[2-2]" -> 17,
              "[3-13]" -> 16,
              "[14-17]" -> 16,
              "[18-18]" -> 19,
              "[19-21]" -> 21,
              "[22-23]" -> 19),
        "org/ifi/seal/example/ConcreteSubclass" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/ControlFlow" ->
          Map("[13-13]" -> 24,
              "[14-18]" -> 26,
              "[19-21]" -> 26,
              "[22-23]" -> 26),
        "org/ifi/seal/example/ConcreteClass$InnerClass" ->
          Map("[1-15]" -> 1,
              "[16-23]" -> 2),
        "org/ifi/seal/example/ConcreteClass$InnerClass$InnerInnerClass" ->
          Map("[1-15]" -> 1,
              "[16-23]" -> 3),
        "org/ifi/seal/example/Demo" ->
          Map("[17-23]" -> 3),
        "org/ifi/seal/example/GenericPairInterface" ->
          Map("[1-23]" -> 2),
        "org/ifi/seal/example/GenericPair" ->
          Map("[1-23]" -> 5),
        "org/ifi/seal/example/subpackage/SubpackageClass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/subpackage/subsubpackage/SubsubpackageClass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/codesmells/CodeSmell" ->
          Map("[7-22]" -> 0,
              "[23-23]" -> 0),
        "org/ifi/seal/example/codesmells/BrainMethod" ->
          Map("[7-7]" -> 1,
              "[8-8]" -> 1,
              "[9-9]" -> 1,
              "[10-22]" -> 1,
              "[23-23]" -> 2),
        "org/ifi/seal/example_sibling/SiblingClass" ->
          Map("[1-23]" -> 0)
      )
      checkValuesIntASM(classAggregatesASM, expectedValues,
                         { case (name, range, _, _, _, _, rfc, _) => (name, range, rfc)})
      success
    }

    "calculate correct LCOM measures for classes" in {
      val expectedValues = Map(
        "org/ifi/seal/example/package-info" ->
          Map("[1-10]" -> 0,
              "[11-11]" -> 0,
              "[12-23]" -> 0),
        "org/ifi/seal/example/Main" ->
          Map("[1-3]" -> 0,
              "[4-5]" -> 0,
              "[6-6]" -> 0,
              "[7-12]" -> 0,
              "[13-16]" -> 0,
              "[17-18]" -> 0,
              "[19-19]" -> 0,
              "[20-20]" -> 0,
              "[21-21]" -> 0,
              "[22-23]" -> 0),
        "org/ifi/seal/example/AbstractClass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/BaseInterface" ->
          Map("[1-22]" -> 0,
              "[23-23]" -> 0),
        "org/ifi/seal/example/ChildInterface" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/CustomException" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/ConcreteClass" ->
          Map("[1-1]" -> 142,
              "[2-2]" -> 170,
              "[3-13]" -> 156,
              "[14-17]" -> 156,
              "[18-18]" -> 198,
              "[19-21]" -> 212,
              "[22-23]" -> 198),
        "org/ifi/seal/example/ConcreteSubclass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/ControlFlow" ->
          Map("[13-13]" -> 0,
              "[14-18]" -> 0,
              "[19-21]" -> 0,
              "[22-23]" -> 0),
        "org/ifi/seal/example/ConcreteClass$InnerClass" ->
          Map("[1-15]" -> 0,
              "[16-23]" -> 0),
        "org/ifi/seal/example/ConcreteClass$InnerClass$InnerInnerClass" ->
          Map("[1-15]" -> 0,
              "[16-23]" -> 0),
        "org/ifi/seal/example/Demo" ->
          Map("[17-23]" -> 0),
        "org/ifi/seal/example/GenericPairInterface" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/GenericPair" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/subpackage/SubpackageClass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/subpackage/subsubpackage/SubsubpackageClass" ->
          Map("[1-23]" -> 0),
        "org/ifi/seal/example/codesmells/CodeSmell" ->
          Map("[7-22]" -> 0,
              "[23-23]" -> 0),
        "org/ifi/seal/example/codesmells/BrainMethod" ->
          Map("[7-7]" -> 0,
              "[8-8]" -> 0,
              "[9-9]" -> 0,
              "[10-22]" -> 0,
              "[23-23]" -> 0),
        "org/ifi/seal/example_sibling/SiblingClass" ->
          Map("[1-23]" -> 0)
      )
      checkValuesIntASM(classAggregatesASM, expectedValues,
                         { case (name, range, _, _, _, _, _, lcom) => (name, range, lcom)})
      success
    }






    "shutdown the computation and cleanup the source" in {
      c.shutdown
      c.cleanup
      success
    }
  }

}

// TODO: remove clones
class DummyPersistence(resultsDir: String) extends Persistence {
  override def persist(c: LisaComputation) { }
}


