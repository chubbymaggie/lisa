package ch.uzh.ifi.seal.lisa.misc

import org.specs2.mutable.SpecificationLike

class ReadmeExampleSpec extends SpecificationLike {

  sequential

  "The README.md example should" >> {

    "work properly" in {
      ReadmeExample.main(args=Array[String]())
      success
    }
  }

}

