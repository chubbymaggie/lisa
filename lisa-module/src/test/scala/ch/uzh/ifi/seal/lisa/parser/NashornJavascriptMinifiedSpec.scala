package ch.uzh.ifi.seal.lisa.parser

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.module.parser.NashornJavascriptParser
import ch.uzh.ifi.seal.lisa.module.parser.NashornJavascriptParseTree
import ch.uzh.ifi.seal.lisa.module.persistence.CSVPerRevisionParallelizedPersistence

class NashornJavascriptMinifiedSpec extends LisaSpec("javascriptMinifiedSpec") {

  implicit override val domain = NashornJavascriptParseTree
  override val persistence = new CSVPerRevisionParallelizedPersistence(resultDir)
  val c = makeComputation(List[Parser](NashornJavascriptParser))

  "The small computation should" >> {

    prepare(c)
    compute(c)
    persist(c)
    finalize(c)

  }

}

