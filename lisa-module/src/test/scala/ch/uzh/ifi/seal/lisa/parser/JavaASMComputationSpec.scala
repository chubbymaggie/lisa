package ch.uzh.ifi.seal.lisa.parser

import scala.collection.mutable._

import java.nio.file.Files
import java.nio.file.Paths
import ch.uzh.ifi.seal.lisa.core._
import ch.uzh.ifi.seal.lisa.core.public._
import ch.uzh.ifi.seal.lisa.module.parser.JavaASMParser
import ch.uzh.ifi.seal.lisa.module.parser.JavaASMGraph
import ch.uzh.ifi.seal.lisa.module.analysis.UniversalAnalysisSuite

import test.scala.ch.uzh.ifi.seal.lisa.LisaSpec

class JavaASMComputationSpec extends LisaSpec("javaASMSpec") {

  sequential

  val targetDir = s"/tmp/${id}"
  val parsers = List[Parser](JavaASMParser)

  override val analyses = UniversalAnalysisSuite
  override val persistence = new DummyPersistence(targetDir)
  implicit override val domain = JavaASMGraph
  val binpath = "/tmp/git-ant-build-revisions/bin/"
  val sources = new JavaBytecodeAgent(parsers, binpath)
  //val sources = new JavaBytecodeAgent(parsers, "/home/user/squaad/avro/vers/")
  val c = makeComputationWithSources(parsers, sources)//, console = true)

  val bindataExists = Files.exists(Paths.get(binpath))
  if (!bindataExists) {
    logger.error("Binary files do not exist (use lisa-module/src/test/resources/git-ant-build-revisions.bash to create them)")
  }
  skipAllUnless(bindataExists)

  "The ASM Loader should" >> {

    prepare(c)
    compute(c)

    "produce correct number of class aggregates" in {
      classAggregates = getClassAggregates(c.graph)
      logger.info(s"class aggregates: ${classAggregates.size}")
      assert(classAggregates.size == 38)
      success
    }
    "calculate correct NOM measures for classes" in {
      val expectedValues = Map(
        "org/ifi/seal/example/package-info" ->
          Map("[1-10]" -> 0,
              "[11-11]" -> 0,
              "[12-23]" -> 0),
        "org/ifi/seal/example/Main" ->
          Map("[1-3]" -> 3,
              "[4-5]" -> 3,
              "[6-6]" -> 3,
              "[7-12]" -> 3,
              "[13-16]" -> 3,
              "[17-18]" -> 3,
              "[19-19]" -> 3,
              "[20-20]" -> 3,
              "[21-21]" -> 3,
              "[22-23]" -> 3),
        "org/ifi/seal/example/AbstractClass" ->
          Map("[1-23]" -> 2),
        "org/ifi/seal/example/BaseInterface" ->
          Map("[1-22]" -> 1,
              "[23-23]" -> 1),
        "org/ifi/seal/example/ChildInterface" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/CustomException" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/ConcreteClass" ->
          Map("[1-1]" -> 15,
              "[2-2]" -> 17,
              "[3-13]" -> 16,
              "[14-17]" -> 16,
              "[18-18]" -> 19,
              "[19-21]" -> 20,
              "[22-23]" -> 19),
        "org/ifi/seal/example/ConcreteSubclass" ->
          Map("[1-23]" -> 2),
        "org/ifi/seal/example/ControlFlow" ->
          Map("[13-13]" -> 25,
              "[14-23]" -> 27),
        "org/ifi/seal/example/ConcreteClass$InnerClass" ->
          Map("[1-4]" -> 1,
              "[5-15]" -> 1,
              "[16-23]" -> 2),
        "org/ifi/seal/example/ConcreteClass$InnerClass$InnerInnerClass" ->
          Map("[1-4]" -> 1,
              "[5-15]" -> 1,
              "[16-23]" -> 3),
        "org/ifi/seal/example/Demo" ->
          Map("[17-23]" -> 2),
        "org/ifi/seal/example/GenericPairInterface" ->
          Map("[1-23]" -> 2),
        "org/ifi/seal/example/GenericPair" ->
          Map("[1-23]" -> 5),
        "org/ifi/seal/example/subpackage/SubpackageClass" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/subpackage/subsubpackage/SubsubpackageClass" ->
          Map("[1-23]" -> 1),
        "org/ifi/seal/example/codesmells/CodeSmell" ->
          Map("[7-22]" -> 1,
              "[23-23]" -> 1),
        "org/ifi/seal/example/codesmells/BrainMethod" ->
          Map("[7-7]" -> 2,
              "[8-8]" -> 2,
              "[9-9]" -> 2,
              "[10-22]" -> 2,
              "[23-23]" -> 3),
        "org/ifi/seal/example_sibling/SiblingClass" ->
          Map("[1-23]" -> 1)
      )
      checkValuesInt(classAggregates, expectedValues,
                         { case (name, range, nom, _, _, _, _) => (name, range, nom)})
      success
    }
    "shutdown the computation and cleanup the source" in {
      c.shutdown
      c.cleanup
      success
    }
  }

}

class DummyPersistence(resultsDir: String) extends Persistence {
  override def persist(c: LisaComputation) { }
}


