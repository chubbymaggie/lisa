#!/bin/bash

# Script for transforming different source data to the directory structure
# expected by LISA.

source "$(dirname "${BASH_SOURCE%}")/lib.bash"

check_for_command jq
[[ -z "$1" ]] && echo "First argument should indicate source type" && exit 1

sourcetype="$1"

case "$sourcetype" in
  "squaad")

    shift
    [[ "$#" -ne 2 ]] && echo "Requires 2 arguments: SQUAAD revs directory and git URL" && exit 1
    squaaddir="$1"
    giturl="$2"

    cd "$squaaddir"/..
    rm -Rf repo
    echo "cloning $giturl"
    git clone "$giturl" repo &> /dev/null
    cd repo

    seqwidth=$(($(git rev-list HEAD | wc -l | wc -c)-1))
    revcount=0
    versdir="../vers"

    for f in "${versdir}"/*; do

      rev="$(basename "$f")"
      # ensure script runs even when directory already moved
      moved="$(echo "$rev" | awk -F '_' '{print $2}')"
      [[ -n "$moved" ]] && rev="$moved"
      git log -n 1 --pretty="tformat:%at %H" "$rev"

    done | sort -g -k 1 | while read chronorev; do

      # move directory if necessary
      rev="$(echo "$chronorev" | awk '{print $2}')"
      seq="$(printf "%0${seqwidth}d" $revcount)"
      from="${versdir}/${rev}"
      to="${versdir}/${seq}_${rev}"
      if [[ -d "$from" ]]; then
        echo -en "storing git metadata; moving $from to $to\r"
        mv "$from" "$to"
      elif [[ -d "$to" ]]; then
        echo -en "storing git metadata; already moved $from to $to\r"
      else
        echo "something's wrong with the existing directory structure"
        exit 1
      fi

      # extract git metadata
      metaFile="$to/git-metadata.txt"
      jq -n --arg authorName     "$(git log  --format='%an' -n 1 "$rev")" \
            --arg committerName  "$(git log  --format='%cn' -n 1 "$rev")" \
            --arg authorEmail    "$(git log  --format='%ae' -n 1 "$rev")" \
            --arg committerEmail "$(git log  --format='%ce' -n 1 "$rev")" \
            --arg authorDate     "$(git log  --format='%aI' -n 1 "$rev")" \
            --arg committerDate  "$(git log  --format='%cI' -n 1 "$rev")" \
            --arg subject        "$(git log  --format='%s' -n 1 "$rev")" \
            --arg message        "$(git log  --format='%b' -n 1 "$rev")" \
            "$metaTemplate" > "$metaFile"

      ((revcount++))
    done
    echo

  ;;
  *)
  echo "Unknown source type $sourcetype" && exit 1
  ;;
esac

