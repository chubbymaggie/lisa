#!/bin/bash

check_for_command () {
  command -v "$1" >/dev/null 2>&1 || {
    echo >&2 "Must have $1 installed, aborting."; exit 1;
  }
}

read -r -d '' metaTemplate << "EOF"
{
  authorName: $authorName,
  committerName: $committerName,
  authorEmail: $authorEmail,
  committerEmail: $committerEmail,
  authorDate: $authorDate,
  committerDate: $committerDate,
  subject: $subject,
  message: $message
}
EOF

