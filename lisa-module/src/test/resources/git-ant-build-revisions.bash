#!/bin/bash

# This script clones a repo and tries to compile every revision shown by
# git rev-list HEAD using ant. A folder is created for each revision, even if a
# build fails. Each folder contains a file 'git-metadata.txt' with the relevant
# Git info.
#
# Example:
# ./lisa-module/src/test/resources/git-ant-build-revisions.bash "https://bitbucket.org/sealuzh/lisa-java-example-repository.git" "/tmp/git-ant-build-revisions/"

source "$(dirname "${BASH_SOURCE%}")/lib.bash"

check_for_command jq
[[ "$#" -ne 2 ]] && echo "Requires 2 arguments: Git repository URL and output directory" && exit 1

repo="$1"
out="$2"

mkdir -p "$out"
cd "$out"
rm -Rf "bin"
mkdir "bin"
rm -Rf "repo"
git clone "$repo" repo &> /dev/null
cd repo
seqwidth=$(($(git rev-list HEAD | wc -l | wc -c)-1))
revcount=0
git rev-list HEAD | tac | while read rev; do
  seq="$(printf "%0${seqwidth}d" $revcount)"
  echo -n "${seq}_${rev}... "
  git checkout "$rev" 2>/dev/null | grep -e '^HEAD is now'
  ant | grep  -iE '^build ' ;
  target="../bin/${seq}_${rev}"
  mkdir "$target"
  if [[ -d bin ]]; then
    mv bin/* "$target"
    rm -R bin
  fi

  metaFile="$target/git-metadata.txt"
  jq -n --arg authorName     "$(git log  --format='%an' -n 1 "$rev")" \
        --arg committerName  "$(git log  --format='%cn' -n 1 "$rev")" \
        --arg authorEmail    "$(git log  --format='%ae' -n 1 "$rev")" \
        --arg committerEmail "$(git log  --format='%ce' -n 1 "$rev")" \
        --arg authorDate     "$(git log  --format='%aI' -n 1 "$rev")" \
        --arg committerDate  "$(git log  --format='%cI' -n 1 "$rev")" \
        --arg subject        "$(git log  --format='%s' -n 1 "$rev")" \
        --arg message        "$(git log  --format='%b' -n 1 "$rev")" \
        "$metaTemplate" > "$metaFile"

  ((revcount++))
done
cd ..
rm -Rf "repo"


