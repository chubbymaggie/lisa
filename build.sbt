name := "lisa"

version := "0.2.5"

organization in ThisBuild := "ch.uzh.ifi.seal"

scalaVersion in ThisBuild := "2.12.6"

cancelable in Global := true

scalacOptions in ThisBuild ++= Seq(
  "-Ywarn-unused-import",
  "-feature",
  "-unchecked",
  "-deprecation",
  "-Xlint:-adapted-args"
)

lazy val root = Project("lisa", file(".")) aggregate(core, antlr, module)
lazy val core = Project(id = "lisa-core", base = file("lisa-core"))
lazy val antlr = Project(id = "lisa-antlr", base = file("lisa-antlr")) dependsOn(core)
lazy val module = Project(id = "lisa-module", base = file("lisa-module")) dependsOn(core, antlr)

