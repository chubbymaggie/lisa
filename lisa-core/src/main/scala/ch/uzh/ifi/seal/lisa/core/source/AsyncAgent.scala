package ch.uzh.ifi.seal.lisa.core.source

import akka.actor._
import akka.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.Await
import scala.language.postfixOps
import scala.language.existentials
import com.typesafe.scalalogging.LazyLogging
import com.signalcollect.Graph
import ch.uzh.ifi.seal.lisa.core.computation._
import ch.uzh.ifi.seal.lisa.core.public.Deleted
import ch.uzh.ifi.seal.lisa.core.public.Unmodified
import scala.collection.mutable.HashMap
import ch.uzh.ifi.seal.lisa.core.misc.RuntimeStats
import ch.uzh.ifi.seal.lisa.core.public.ParseStats

/** Asynchronous parsing for SourceAgents supporting direct file access */
trait AsyncAgent extends LazyLogging with RuntimeStats {
  def parseAsync(graph: Graph[Any,Any], sources: SourceAgent)(implicit uid: String) = {

    val timestamp = System.nanoTime()
    logger.info("parsing source code")
    implicit val timeout = Timeout(30 days)

    // start up an actor system with a ParsingSupervisor in control
    val system = ActorSystem("ParsingActorSystem", classLoader=Option(getClass.getClassLoader))
    import ParsingSupervisor._
    val props = Props(classOf[ParsingSupervisor], graph, sources, uid)
    val supervisor = system.actorOf(props, name = "ParsingSupervisor")

    // start parsing and wait for the system to shutdown
    supervisor ! Start()
    Await.result(system.whenTerminated, 30 days)

    val duration = (System.nanoTime - timestamp)/1000000000.0
    logger.info(f"parse all duration: ${duration}%3.2f")
    stats += ("parse_all_seconds" -> duration)
  }
}


object ParsingActor {
  case class Parse(f: FileRevision, rev: Revision)
}
import ParsingActor._

object ParsingSupervisor {
  case class Start()
  case class ChildDone(uri: String, rev: Revision, charCount: Long, parseStats: ParseStats)
}
import ParsingSupervisor._

class ParsingActor(graph: Graph[Any,Any], sources: SourceAgent) extends Actor {
  def receive = {
    case Parse(f, rev) =>
      var parseStats = ParseStats()
      val byteCount = if (!List(Deleted, Unmodified).contains(f.changeType)) {
        sources.readFileRevision(f) match {
          case Some(fileBytes) =>
            sources.parsers.foreach { parser =>
              if (parser.willRead(f.uri)) {
                parseStats = parseStats.add(parser.parse(List((f.uri, fileBytes)), graph, rev))
              }
            }
            fileBytes.capacity
          case _ => 0
        }
      } else 0
      //println(s"sending EndRangeSignal to ${f.fileId} ${f}")
      if (f.changeType != Unmodified) {
        graph.sendSignal(new EndRangeSignal(List(rev.prev)), f.fileId, None, true)
      }
      sender ! ChildDone(f.uri, rev, byteCount, parseStats)
    case _ =>
  }
}

class ParsingSupervisor(graph: Graph[Any,Any], sources: SourceAgent)(implicit uid: String) extends Actor with LazyLogging with RuntimeStats {

  val childActors = new HashMap[String, ActorRef]()
  var childTasks = 0
  var totalBytes: Long = 0
  var totalParseStats = ParseStats()
  val timestamp = System.nanoTime()

  import akka.actor.SupervisorStrategy._
  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = 0, withinTimeRange = 1 minute) {
      case e: Throwable =>
        context.system.terminate
        logger.error(s"ParsingSupervisor terminating because of ${e.printStackTrace()}")
        Escalate
    }

  def terminate = {
    // finally, all unclosed ranges need to be closed in the last revision
    logger.info("Finalizing initial graph")
    graph.awaitIdle
    graph.recalculateScores
    graph.awaitIdle
    val lastRevision = sources.getRevisionFiles.get.last._2._1
    graph.foreachVertex {
      case v: BaseVertex => v.compressRanges(Some(lastRevision))
      case _ =>
    }
    graph.awaitIdle
    graph.recalculateScores
    graph.awaitIdle
    /*import ch.uzh.ifi.seal.lisa.core.computation.Computation.TopKMemoryConsumers
    import ch.uzh.ifi.seal.lisa.core.computation.Computation.TotalMemoryConsumption
    val consumption = graph.aggregate(new TotalMemoryConsumption())
    val consumers = graph.aggregate(new TopKMemoryConsumers(10))
    import org.github.jamm.MemoryMeter
    val meter = new MemoryMeter()
    def bp(n: Long) = TopKMemoryConsumers.humanByteCount(n)
    val graphSize = meter.measureDeep(graph)
    logger.info(s"Graph size measurement: ${bp(graphSize)}: $graphSize, sum of nodes: ${bp(consumption)}")
    consumers.foreach { case (s, v) =>
      val id = v.id
      logger.info(s"${bp(s)} : $id -> ${v.state.rangeStates.size}")
      v.state.rangeStates.foreach { case (range, state) =>
        logger.info(s"${range}: ${bp(meter.measureDeep(state))}")
        logger.info(s"${range}: ${bp(meter.measureDeep(state.data))}")
        state.data.foreach { case (key, value) =>
          logger.info(s"${key}: ${bp(meter.measureDeep(key))}")
          logger.info(s"${key}: ${bp(meter.measureDeep(value))}")
        }
      }
    }*/
    val t = totalParseStats
    stats += ("parse_bytes" -> totalBytes)
    stats += ("parse_lines" -> t.lineCount)
    stats += ("parse_nodes_added" -> t.nodeAddCount)
    stats += ("parse_nodes_ignored" -> t.nodeIgnoreCount)
    val filterRatio = t.nodeAddCount.toDouble / (t.nodeAddCount + t.nodeIgnoreCount)
    stats += ("parse_filter_ratio" -> filterRatio)
    context.system.terminate
  }

  def receive = {
    case Start() =>
      logger.debug(s"Parsing ${sources.getRevisions.size} commits")
      var numberOfFiles = 0
      sources.getRevisionFiles.get.foreach { case (i, revisionData) =>
        val (rev, files) = (revisionData._1, revisionData._2)
        logger.debug(s"Queuing files in revision ${rev.n} (${rev.rev.substring(0,7)}) for parsing")
        files.filter { f =>
          // if an appropriate parser exists
          sources.parsers.exists(_.willRead(f.uri))
        }.map { f =>
          numberOfFiles += 1
          // get existing actor for this fileId or create one if necessary
          val childActor = childActors.getOrElseUpdate(f.fileId, {
              val props = Props(classOf[ParsingActor], graph, sources)
                  .withDispatcher("lisa.parsing-dispatcher")
              context.actorOf(props, name = s"ParsingActor-${childActors.size}")
            }
          )
          // Send a new task to the actor
          childActor ! Parse(f, rev)
          childTasks += 1
        }
      }
      if (numberOfFiles == 0) {
        logger.error("No files available for parsing! Parsed nothing!")
        terminate
      }
      logger.debug(s"Queued a total of $numberOfFiles files")
    case ChildDone(uri, rev, byteCount, parseStats) =>
      import ch.uzh.ifi.seal.lisa.core.computation.Computation.VertexCompressionRatio
      totalBytes += byteCount
      totalParseStats = totalParseStats.add(parseStats)
      childTasks -= 1
      if (childTasks % 10 == 0) {
        logger.info(s"files remaining: $childTasks")
      }
      if (RuntimeStats.periodicStats && childTasks % 50 == 0) {
        val (vertexCount, rangeCount, revisionCount) = graph.aggregate(new VertexCompressionRatio())
        val rangeCompression = rangeCount.toDouble / revisionCount
        listStat(s"compression_vertices_periodic", rangeCompression)
        listStat(s"vertex_count_periodic", vertexCount)
        listStat(s"range_count_periodic", rangeCount)
        listStat(s"range_count_uncompressed_periodic", revisionCount)
        val duration = (System.nanoTime - timestamp)/1000000000.0
        listStat("runtime_periodic", duration)
        logger.info(f"vertex range compression ratio: ${rangeCount}%.3f / $revisionCount = ${rangeCompression}%1.4f")
      }
      //logger.debug(s"[${childActors.size}] files remaining: $childTasks (done with $fileId of $rev)")
      if (childTasks == 0) { terminate }
    case _ =>
  }
}


