package ch.uzh.ifi.seal.lisa.core.computation

import com.typesafe.scalalogging.LazyLogging
import scala.collection.immutable.SortedMap
import scala.reflect.{ClassTag, classTag}

trait Data extends Product {
  def apply[D <: Data]() = this.asInstanceOf[D]
}

case class Literal(persist: Boolean, map: Map[String, String]) extends Data {
  def this() = this(false, Map())
}
case class TypeLabel(persist: Boolean, t: String) extends Data {
  def this() = this(false, "unknown")
}
case class FileMeta(persist: Boolean, loc: Int, bytes: Int) extends Data {
  def this() = this(false, 0, 0)
}

abstract class GraphEdit
case class VertexAddition(id: String, domain: Domain) extends GraphEdit
case class VertexDeletion(id: String) extends GraphEdit
case class EdgeAddition(src: String, edge: BaseEdge) extends GraphEdit


/** Container which stores analyses data and new analyses for a single range
  *
  * Analyses store their data as well as any newly spawned analyses in this
  * data structure. A vertex will store one of these for each RevisionRange
  * where the vertex is relevant. The analyses list is emptied after each
  * signaling step.
  */
case class AnalysisState(data: DataMap,
                         analysesUp: List[(AnalysisPacket, String)],
                         analysesDown: List[(AnalysisPacket, String)],
                         analysesAny: List[(AnalysisPacket, String)],
                         analysesInterObject: List[(AnalysisPacket, String)],
                         edits: List[GraphEdit]) extends LazyLogging {
  def gotAnalyses = List(analysesUp, analysesDown, analysesAny) exists (_.nonEmpty)
  def apply[D <: Data : ClassTag](): D = {
    val id = classTag[D].toString
    data.get(AnalysisState.id(id)) match {
      case Some(d) => d.asInstanceOf[D]
      case _ => AnalysisState.base[D].asInstanceOf[D]
    }
  }
  def +[D <: Data : ClassTag](d: D): AnalysisState = {
    val id = classTag[D].toString
    this.copy(data = data + (AnalysisState.id(id) -> d))
  }
  def -[D <: Data : ClassTag](): AnalysisState = {
    val id = classTag[D].toString
    this.copy(data = data - AnalysisState.id(id))
  }
  def ?(a: AnalysisPacket, relation: String = ""): AnalysisState = {
    this.copy(analysesInterObject = (a, relation) :: analysesInterObject)
  }
  def !(a: AnalysisPacket, relation: String = ""): AnalysisState = ↑(a)
  def ↑(a: AnalysisPacket, relation: String = ""): AnalysisState = {
    this.copy(analysesUp = (a, relation) :: analysesUp)
  }
  def ↓(a: AnalysisPacket, relation: String = ""): AnalysisState = {
    this.copy(analysesDown = (a, relation) :: analysesDown)
  }
  def *(a: AnalysisPacket, relation: String = ""): AnalysisState = {
    this.copy(analysesAny = (a, relation) :: analysesAny)
  }
  def %(e: GraphEdit): AnalysisState = {
    this.copy(edits = e :: edits)
  }
  def is(m: Symbol*)(implicit domain: Domain): Boolean = {
    domain.lookup(apply[TypeLabel].t, m:_*)
  }
  def is(m: String*): Boolean = {
    m.exists(_ == apply[TypeLabel].t)
  }
  def among(m: Symbol*)(implicit domain: Domain): Option[String] = {
    m.foreach { s => 
      val r = domain.reflect(apply[TypeLabel].t, m:_*)
      if (r.nonEmpty) { return r }
    }
    return None
  }
  def among(m: String*): Option[String] = {
    m.foreach { s => if (s == apply[TypeLabel].t) return Some(s) }
    return None
  }
  /* Returns a map where data from all data structures (case classes) is merged
   *
   * A single Map[String,Any] is populated with data from all analysis data case
   * classes. For arbitrary field data types, the available string
   * representation is used. For Map[_,_] fields, the individual map elements
   * are included in the final result. If selective persistence is enabled, the
   * persist element of each data instance determines whether the data should
   * be included or not.
   */
  def flatData(selective: Boolean = true): Map[String,Any] = {
    data.foldLeft(Map[String, Any]()) { case (acc, (ct, d)) =>
      val headerPrefix = d.getClass.toString

      def caseClassToMap(fields: Array[java.lang.reflect.Field], iterator: Iterator[Any]): Map[String, Any] = {
        fields.foldLeft(Map[String, Any]()) { case (acc, field) =>
          val dataKey = s"${headerPrefix}.${field.getName}"
          iterator.next match {
            case m: Map[_, _] => {
              acc ++ m.map{ case (k, v) => (s"${dataKey}.${k}" -> v) }
            }
            case n => acc + (dataKey -> n)
          }
        }
      }

      var persistBoolean: Option[Boolean] = None
      var persistSet: Option[Set[String]] = None
      // iterate through the fields of a data structure
      val i = d.productIterator
      var fields = d.getClass.getDeclaredFields
      // if the case class is empty (for some reason), there's nothing to do
      var tempMap = fields.headOption match {
        case None => acc
        case Some(f) if f.getName == "persist" =>
          fields = fields.tail
          i.next match {
            case b: Boolean => persistBoolean = Some(b)
            case s: Set[_] => persistSet = Some(s.asInstanceOf[Set[String]])
            case _ => logger.error(s"Analysis data type '${headerPrefix}' is invalid: persist field must be a Boolean or Set")
          }
          caseClassToMap(fields, i)
        case Some(f) =>
          caseClassToMap(fields, i)
      }
      if (selective && persistSet.nonEmpty) {
        // store values selected for persistence
        val selected = persistSet.get
        acc ++ tempMap.filter { case (k, v) => selected.contains(k.reverse.takeWhile(_ != '.').reverse) }
      }
      else if (selective && !persistBoolean.getOrElse(false)) { acc }
      else { acc ++ tempMap }
    }
  }
}
/* manages the cache of data keys to integers used in actual data maps */
object AnalysisState {
  val keyCache = scala.collection.concurrent.TrieMap[String, Int]()
  def id(key: String) = {
    keyCache.get(key) match {
      case Some(k) => k
      case _ => synchronized { keyCache.getOrElseUpdate(key, keyCache.size) }
    }
  }
  private def newInstance[D <: Data : ClassTag] = {
    implicitly[ClassTag[D]].runtimeClass.newInstance.asInstanceOf[D]
  }
  val typeCache = scala.collection.concurrent.TrieMap[ClassTag[_], Data]()
  def base[D <: Data : ClassTag] = {
    val ct = classTag[D]
     typeCache.get(ct) match {
      case Some(instance) => instance
      case _ => synchronized { typeCache.getOrElseUpdate(ct, newInstance[D]) }
    }

  }
}

/** A vertex state consists of multiple ranges with individual AnalysisStates
  *
  * The revisionRanges and rangeStates maps must be edited simultaneously, since
  * revisionRanges is a map of RevisionRange start numbers to RevisionRanges and
  * rangeStates is a map of RevisionRanges to AnalysisStates.
  *
  * @see Revision
  * @see RevisionRange
  * @see AnalysisState
  */
case class State (
  revisionRanges: SortedMap[Int, RevisionRange] = SortedMap(),
  rangeStates: Map[RevisionRange, AnalysisState] = Map()
)
object State {
  final def neutralState = AnalysisState(DataMap, List(), List(), List(), List(), List())
  /** create a new State with an initial RevisionRange and vertex type
    *
    * @param revisionRange the initial range this State is created for
    */
  def apply(revisionRange: RevisionRange): State = {
    State(SortedMap(revisionRange.start.n -> revisionRange),
          Map(revisionRange -> neutralState))
  }

  /** expose the vertex State data to the S/C console
    *
    * @param s the State to expose
    */
  def expose(s: State): Map[String, Any] = {
    Map("revisionRanges" -> s.revisionRanges.map(_.toString),
        "rangeStates" -> s.rangeStates)
  }

  /** ensure that the State contains data for the entire given RevisionRange
    *
    * A State may contains data on multiple non-adjacent RevisionRanges. This
    * method will fill the gaps between these ranges if they are located within
    * the given range. The function will ignore leading gaps, as these are
    * already covered by the intersectRange method. An illustration:
    *
    *    Existing ranges:   [         ]   [   ][   ]   [              ]
    *    range to check:                  [               ]
    *    resulting ranges:  [         ]   [   ][   ][ ][              ]
    *
    * @param range the RevisionRange within gaps should be checked
    * @param state the State to check for gaps
    */
  def makeContiguous(range: RevisionRange, state: State): State = {
    var newState = state
    if (state.rangeStates.contains(range)) { newState }
    else {
      val statesInRange = state.revisionRanges.range(range.start.n, range.end.n)
      statesInRange.sliding(2).foreach { map =>
        if (map.size == 2) {
          val first = map.head._2
          val second = map.last._2
          if (first.end.n + 1 != second.start.n) {
            val newRange = RevisionRange(first.end.next.get, second.start.prev.get)
            newState = newState.copy(
              revisionRanges = (newState.revisionRanges
                + (newRange.start.n -> newRange)),
              rangeStates = (newState.rangeStates
                + (newRange -> AnalysisState(DataMap, List(), List(), List(), List(), List())))
            )
          }
        }
      }
    }
    newState
  }

  /** intersect the given range with the ranges in the given State
    *
    * When an Analysis (which is valid only for a specific range) arrives at a
    * Vertex, it may well be that the Vertex' State does not contain that
    * particular range, but only a larger range of which this range is part of.
    * In this case, the Vertex' State ranges need to be split. This method can
    * handle cases where the given range overlaps into empty space, in which
    * case a new empty State is created for the previously empty range, but it
    * cannot handle gaps within the given range. For those cases, makeContiguous
    * can be used to fill the gaps in a second step.
    *
    * An illustration:
    *    Existing ranges:   [        ]  [        ]
    *    range to check:        [                    ]
    *    resulting ranges:  [  ][    ]  [     ][ ][    ]
    *
    * The return value is a tuple containing the modified State as well as a map
    * of affected ranges. In the example above, the two middle ranges are
    * affected.
    *
    * @param range the RevisionRange that needs a matching range in the State
    * @param state the State containing existing ranges
    */
  def intersectRange(range: RevisionRange, state: State, createIfMissing: Boolean = false):
      (State, SortedMap[Int, RevisionRange]) = {
    var newState = state
    // we need to find existing ranges which...
    //   1) are entirely within range -> affected
    //   2) start but do not end within range -> split, first part affected
    //   3) end but not start within range -> split, second part affected
    // alternatively, the given range may not overlap with any ranges -> create it or do nothing

    // if range exactly encompasses all existing ranges, nothing is split and
    // simply all ranges are affected
    val existingStart = state.revisionRanges.head._2.start
    val existingEnd = state.revisionRanges.last._2.end
    if (existingStart == range.start
        && existingEnd == range.end) {
      (newState, state.revisionRanges)
    }
    else {
      // find existing ranges within and overlapping the end of range
      val simpleRangeSearch = state.revisionRanges.range(range.start.n, range.end.n + 1)
      val (within, after) = simpleRangeSearch.values match {
        case rs if rs.size == 0 =>
          (None, None)
        case rs if rs.size == 1 && rs.head.end.n > range.end.n  =>
          (None, Some(simpleRangeSearch.head))
        case rs if rs.size == 1 =>
          (Some(simpleRangeSearch), None)
        case rs if rs.last.end.n > range.end.n =>
          (Some(simpleRangeSearch.init), Some(simpleRangeSearch.last))
        case rs =>
          (Some(simpleRangeSearch), None)
      }
      // find existing overlapping the start of range
      val startRangeSearch = state.revisionRanges.until(range.start.n)
      val before = startRangeSearch.values match {
        case r if r.size == 0 =>
          None
        case r if r.last.end.n >= range.start.n =>
          Some(startRangeSearch.last)
        case _ =>
          None
      }

      val affectedWithinRanges = within

      // split the range overlapping the end of the range (if any)
      val affectedEndRange = after match {
        case Some(r) =>
          val affectedRange = RevisionRange(r._2.start, range.end)
          val unaffectedRange = RevisionRange(range.end.next.get, r._2.end)
          val existingAnalysisState = state.rangeStates(r._2)
          newState = newState.copy(
            revisionRanges = (newState.revisionRanges
              + (unaffectedRange.start.n -> unaffectedRange)
              + (affectedRange.start.n -> affectedRange)),
            rangeStates = (newState.rangeStates
              + (unaffectedRange -> existingAnalysisState)
              + (affectedRange -> existingAnalysisState)
              - (r._2))
          )
          Some((affectedRange.start.n -> affectedRange))
        case _ => None
      }

      // split the range overlapping the start of the range (if any)
      val affectedStartRange = before match {
        case Some(r) =>
          val unaffectedRange = RevisionRange(r._2.start, range.start.prev.get)
          val affectedRange = RevisionRange(range.start, r._2.end)
          val existingAnalysisState = state.rangeStates(r._2)
          newState = newState.copy(
            revisionRanges = (newState.revisionRanges
              + (unaffectedRange.start.n -> unaffectedRange)
              + (affectedRange.start.n -> affectedRange)),
            rangeStates = (newState.rangeStates
              + (unaffectedRange -> existingAnalysisState)
              + (affectedRange -> existingAnalysisState)
              - (r._2))
          )
          Some((affectedRange.start.n -> affectedRange))
        case _ => None
      }

      // construct the map of affected ranges from the splits above
      val affectedRanges = affectedWithinRanges match {
        case Some(rs) => affectedStartRange match {
          case Some(rStart) => affectedEndRange match {
            case Some(rEnd) => rs + rStart + rEnd
            case None => rs + rStart
          }
          case None => affectedEndRange match {
            case Some(rEnd) => rs + rEnd
            case None => rs
          }
        }
        case None => affectedStartRange match {
          case Some(rStart) => SortedMap(rStart)
          case None => affectedEndRange match {
            case Some(rEnd) => SortedMap(rEnd)
            case None => SortedMap[Int, RevisionRange]()
          }
        }
      }

      // if the range had been entirely outside existing ranges, either create
      // a new empty range or do nothing depending on createIfMissing
      if (affectedRanges.isEmpty) {
        if (createIfMissing) {
          newState = newState.copy(
            revisionRanges = (newState.revisionRanges
              + (range.start.n -> range)),
            rangeStates = (newState.rangeStates
              + (range -> AnalysisState(DataMap, List(), List(), List(), List(), List())))
          )
          (newState, SortedMap((range.start.n -> range)))
        }
        else { (state, affectedRanges) }
      }
      else { (newState, affectedRanges) }
    }
  }

  /** merge adjacent ranges if they contain exactly the same state
    *
    * This method checks whether directly adjacent AnalysisStates inside this
    * State are equal and if yes, it merges them into one range. This basically
    * reduces the overall number of ranges and keeps the number of necessary
    * splits when running analyses to a minimum.
    *
    * @param state the State to be defragmented
    */
  def defragmentRanges(state: State): State =  {
    val firstRange = state.revisionRanges.head._2
    val firstState = state.rangeStates(firstRange)
    val newRangeStates = SortedMap(firstRange -> firstState)
    var newState = state.copy(rangeStates = state.revisionRanges.tail
         .foldLeft(newRangeStates) { (acc, next)  =>
      val nextRange = next._2
      val nextState = state.rangeStates(nextRange)
      val previousRange = acc.last._1
      val previousState = acc.last._2
      if (previousState == nextState && previousRange.end.n + 1 == nextRange.start.n) {
        acc.init + (RevisionRange(previousRange.start, nextRange.end) -> previousState)
      }
      else {
        acc + (nextRange -> nextState)
      }
    })
    if (newState != state) {
      newState = newState.copy(revisionRanges = newState.rangeStates
              .foldLeft(SortedMap[Int, RevisionRange]()) { (acc, next) =>
        val range = next._1
        acc + (range.start.n -> range)
      })
    }
    newState
  }
}


