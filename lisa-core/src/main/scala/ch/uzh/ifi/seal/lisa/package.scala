package ch.uzh.ifi.seal.lisa

package object core {
  type DataMap = ch.uzh.ifi.seal.lisa.core.computation.DataMap
  type Data = ch.uzh.ifi.seal.lisa.core.computation.Data
  type Domain = ch.uzh.ifi.seal.lisa.core.computation.Domain
  type Mapping = ch.uzh.ifi.seal.lisa.core.computation.Mapping
  type TypeLabel= ch.uzh.ifi.seal.lisa.core.computation.TypeLabel
  type Literal = ch.uzh.ifi.seal.lisa.core.computation.Literal
  val TypeLabel= ch.uzh.ifi.seal.lisa.core.computation.TypeLabel
  val Literal= ch.uzh.ifi.seal.lisa.core.computation.Literal
  type Parser = ch.uzh.ifi.seal.lisa.core.public.Parser
  type ParseStats = ch.uzh.ifi.seal.lisa.core.public.ParseStats
  val ParseStats = ch.uzh.ifi.seal.lisa.core.public.ParseStats
  type Persistence = ch.uzh.ifi.seal.lisa.core.public.Persistence
  type Analysis = ch.uzh.ifi.seal.lisa.core.computation.Analysis
  type AnalysisPacket = ch.uzh.ifi.seal.lisa.core.computation.AnalysisPacket
  type AnalysisState = ch.uzh.ifi.seal.lisa.core.computation.AnalysisState
  type AnalysisSuite = ch.uzh.ifi.seal.lisa.core.computation.AnalysisSuite
  type VertexContext = ch.uzh.ifi.seal.lisa.core.computation.VertexContext
  type Computation = ch.uzh.ifi.seal.lisa.core.computation.Computation
  type BaseVertex = ch.uzh.ifi.seal.lisa.core.computation.BaseVertex
  type Revision = ch.uzh.ifi.seal.lisa.core.computation.Revision
  type RevisionRange = ch.uzh.ifi.seal.lisa.core.computation.RevisionRange
  val RevisionRange = ch.uzh.ifi.seal.lisa.core.computation.RevisionRange
  type AstVertex = ch.uzh.ifi.seal.lisa.core.computation.AstVertex
  type MetaVertex = ch.uzh.ifi.seal.lisa.core.computation.MetaVertex
  type ObjectVertex = ch.uzh.ifi.seal.lisa.core.computation.ObjectVertex
  type UpwardEdge = ch.uzh.ifi.seal.lisa.core.computation.UpwardEdge
  type DownwardEdge = ch.uzh.ifi.seal.lisa.core.computation.DownwardEdge
  type ObjectEdge = ch.uzh.ifi.seal.lisa.core.computation.ObjectEdge
  type InterObjectEdge = ch.uzh.ifi.seal.lisa.core.computation.InterObjectEdge
  type VertexAddition = ch.uzh.ifi.seal.lisa.core.computation.VertexAddition
  val VertexAddition = ch.uzh.ifi.seal.lisa.core.computation.VertexAddition
  type EdgeAddition = ch.uzh.ifi.seal.lisa.core.computation.EdgeAddition
  val EdgeAddition = ch.uzh.ifi.seal.lisa.core.computation.EdgeAddition
  type GitAgent = ch.uzh.ifi.seal.lisa.core.source.GitAgent
  type JavaBytecodeAgent = ch.uzh.ifi.seal.lisa.core.source.JavaBytecodeAgent

  val FileTools = ch.uzh.ifi.seal.lisa.core.misc.FileTools
}
