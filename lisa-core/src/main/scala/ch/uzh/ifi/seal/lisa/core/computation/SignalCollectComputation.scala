package ch.uzh.ifi.seal.lisa.core.computation

import com.typesafe.scalalogging.LazyLogging
import com.signalcollect._
import ch.uzh.ifi.seal.lisa.core.misc._

abstract class AnalysisSuite {
  val phases: List[List[Analysis]]
  final def +(other: AnalysisSuite): AnalysisSuite = {
    val merged: List[List[Analysis]] = this.phases.zipAll(
        other.phases, List[Analysis](), List[Analysis]()
      ).map {
        t => List(t._1,t._2).flatten.distinct
      }
    new AnalysisSuite {
      val phases = merged
    }
  }
}

/** Wrapper around GraphEditor for abstracting ranges and graph modifications
  *
  * Analayses do not need to know about the RevisionRange they are applied to,
  * hence when they modify the graph, applying the modifications for the correct
  * range needs to happen transparently.
  *
  * @param graphEditor the GraphEditor to wrap
  * @param revisionRange the range this wrapper will use in its modifications
  * @param vertexId the id of the Vertex on which analyses are using this wrapper
  */
case class GraphEditorWrapper(graphEditor: GraphEditor[Any, Any],
                              vertexId: String) {
  /** add a new edge using the local vertex as the source */
  def addEdge(edge: BaseEdge) {
    graphEditor.addEdge(vertexId, edge)
  }
  /** add a new edge, using the given vertex id as the source */
  def addEdge(edge: BaseEdge, sourceId: String) {
    graphEditor.addEdge(sourceId, edge)
  }
}

/** Instantiates S/C graph and runs Analyses
  *
  */
class Computation(analyses: AnalysisSuite, console: Boolean)(implicit uid: String) extends LazyLogging with RuntimeStats {
  // an empty graph that needs to be populated by a code parser
  val graph = GraphBuilder
      .withActorNamePrefix(s"lisa-${uid}")
      .withConsole(console)
      //.withEagerIdleDetection(true)
      .withExistingVertexHandlerFactory(new MergeRangeVertexHandlerFactory[Any, Any])
      .withUndeliverableSignalHandlerFactory(new IgnoringUndeliverableSignalHandlerFactory[Any, Any])
      .withEdgeAddedToNonExistentVertexHandlerFactory(new IgnoringEdgeAddedToNonExistentVertexHandlerFactory[Any, Any])
      .build

  // time-keeping and other stats
  var previousSignalStepCount: Long = 0
  var previousCollectStepCount: Long = 0
  var previousVertexCount: Long = 0
  var previousEdgeCount: Long = 0
  var lastExecInfo: Option[ExecutionInformation[Any,Any]] = None

  import com.signalcollect.configuration.ExecutionMode
  val execConfig = ExecutionConfiguration.withExecutionMode(ExecutionMode.ContinuousAsynchronous)
  graph.execute(execConfig)

  /** run the computation on the graph */
  def run() {
    graph.foreachVertex { case v: BaseVertex => v.updateScore }
    graph.recalculateScores
    graph.awaitIdle

    var timestamp = System.nanoTime()
    var phaseCount = 0

    analyses.phases.foreach { phaseAnalyses =>
      val phaseStartTime = System.nanoTime()
      phaseCount += 1

      // start the analyses for the current phase
      logger.info(s"phase ${phaseCount}")
      val (_, startTime) = Profiling.time( {
        graph.foreachVertex {
          case v: BaseVertex =>
            val editor = GraphEditorWrapper(graph, v.id)
            phaseAnalyses.foreach(analysis => analysis.launch(v, analysis))
          case _ =>
        }
      })

      // execute computation and start processing analyses
      val (execInfo, execTime) = Profiling.time( {
        graph.foreachVertex { case v: BaseVertex => v.updateScore }
        graph.recalculateScores
        graph.execute
      })
      graph.awaitIdle

      // defragment the ranges of all vertices (merge adjacent equal ranges)
      graph.foreachVertex {
        case v: BaseVertex =>
          v.state = State.defragmentRanges(v.state)
      }

      // calcualte and print statistics for this phase
      lastExecInfo = Some(execInfo)
      val execStats = execInfo.executionStatistics
      val workerStats = execInfo.aggregatedWorkerStatistics
      graph.awaitIdle
      val phaseSignalStepCount =
          workerStats.signalOperationsExecuted - previousSignalStepCount
      val phaseCollectStepCount =
          workerStats.collectOperationsExecuted - previousCollectStepCount
      val phaseVertexCount =
          workerStats.numberOfVertices - previousVertexCount
      val phaseEdgeCount =
          workerStats.numberOfOutgoingEdges - previousEdgeCount
      val phaseDuration = System.nanoTime() - phaseStartTime
      //import java.util.concurrent.TimeUnit
      //logger.info(
      //        s"finished phase ${phaseCount}: \n" +
      //        f"   ${startTime/1000000}%10d ms phase start duration \n" +
      //        f"   ${execTime/1000000}%10d ms phase execution duration \n" +
      //        f"   ${phaseDuration/1000000}%10d ms phase total duration \n" +
      //        f"   ${execStats.computationTime.toUnit(TimeUnit.MILLISECONDS)
      //               .toInt}%10d ms computation duration\n" +
      //        f"   ${execStats.jvmCpuTime.toUnit(TimeUnit.MILLISECONDS)
      //               .toInt}%10d ms jvm cpu time\n" +
      //        f"   ${phaseSignalStepCount}%10d signal ops\n" +
      //        f"   ${phaseCollectStepCount}%10d collect ops\n" +
      //        f"   ${phaseVertexCount}%+10d vertices\n" +
      //        f"   ${phaseEdgeCount}%+10d edges\n" +
      //        f"   ${workerStats.numberOfVertices}%10d total vertex count\n" +
      //        f"   ${workerStats.numberOfOutgoingEdges}%10d total edge count")
      stats += (s"computation_phase_seconds_${phaseCount}" -> phaseDuration/1000000000.0)
      previousSignalStepCount = workerStats.signalOperationsExecuted
      previousCollectStepCount = workerStats.collectOperationsExecuted
      previousVertexCount = workerStats.numberOfVertices
      previousEdgeCount = workerStats.numberOfOutgoingEdges
    }


    val execStats = lastExecInfo.get.executionStatistics
    val workerStats = lastExecInfo.get.aggregatedWorkerStatistics
    val signalOps = workerStats.signalOperationsExecuted
    val collectOps = workerStats.collectOperationsExecuted
    var duration = (System.nanoTime - timestamp)/1000000000.0
    val nVertices = workerStats.numberOfVertices
    val nEdges = workerStats.numberOfOutgoingEdges
    logger.info(f"duration: ${duration}%3.2fs [#v: ${nVertices}, #e: ${nEdges}]")
    stats += ("computation_seconds" -> duration)
    stats += ("graph_vertex_count" -> nVertices)
    stats += ("graph_edge_count" -> nEdges)
    stats += ("computation_signal_ops" -> signalOps)
    stats += ("computation_collect_ops" -> collectOps)

    logger.info("computation finished")

    // debugging - calculate vertices where most time is spent
    /*val topTimeSpentVertices = graph.aggregate(new TopActivityAggregator(10))
    topTimeSpentVertices.foreach { v =>
      logger.debug(v)
      graph.forVertexWithId[BaseVertex, Unit](v._2, { v =>
        logger.debug(v.state.rangeStates.mapValues{ d =>
          (d.data.typeLabel.typeLabel, d.data.typeResolution.map.size)
        })
      })
    }*/

  }

  def shutdown() = {
    if (console) { scala.io.StdIn.readLine }
    graph.shutdown
  }

  def computeVertexCompressionRatio(): (Long, Long, Long, Double) = {
    logger.info(s"computing vertex compression ratio")
    val (vertexCount, rangeCount, revisionCount) = graph.aggregate(new Computation.VertexCompressionRatio())
    val rangeCompression = rangeCount.toDouble / revisionCount
    logger.info(f"vertex range compression ratio: ${rangeCount}%.3f / $revisionCount = ${rangeCompression}%1.4f")
    (vertexCount, rangeCount, revisionCount, rangeCompression)
  }

  def computeParseCompressionRatios(): (Double, Double, Long, Long) = {
    logger.info(s"computing file compression ratios")
    val counters = graph.aggregate(new Computation.VirtualFileAndLocCount())
    val presentFiles = counters._1
    val presentLines = counters._2
    val parsedFiles = stats.get("source_file_count")
    val parsedLines = stats.get("parse_lines")
    if (parsedFiles.isEmpty || parsedLines.isEmpty) {
      logger.warn(s"Tried to compute ParseCompression, but necessary stats are unavailable. parsedFiles: ${parsedFiles}, parsedLines: ${parsedLines}")
      (0, 0, 0, 0)
    } else {
      val fileRatio = parsedFiles.get.asInstanceOf[Long].toDouble / presentFiles
      val lineRatio = parsedLines.get.asInstanceOf[Long].toDouble / presentLines
      logger.info(f"file parse compression ratio: ${parsedFiles.get} / ${presentFiles} = $fileRatio")
      logger.info(f"line parse compression ratio: ${parsedLines.get} / ${presentLines} = $lineRatio")
      (fileRatio, lineRatio, presentFiles, presentLines)
    }
  }

}

object Computation {
  import com.signalcollect.interfaces.ModularAggregationOperation

  /* Computes the ratio of necessary ranges vs. actually represented revisions */
  class VertexCompressionRatio extends ModularAggregationOperation[(Long, Long, Long)] {
    val neutralElement: (Long, Long, Long) = (0L, 0L, 0L)
    def aggregate(a: (Long, Long, Long), b: (Long, Long, Long)): (Long, Long, Long) = {
      (a._1 + b._1, a._2 + b._2, a._3 + b._3)
    }
    def extract(x: Vertex[_, _, _, _]): (Long, Long, Long) = {
      x match {
        case v: BaseVertex => {
          val revisionCount = v.state.revisionRanges.foldLeft(0) { case (acc, (n, range)) =>
            acc + range.size
          }
          val rangeCount = v.state.revisionRanges.size
          (1, rangeCount, revisionCount)
        }
        case _ => neutralElement
      }
    }
  }

  /* Computes the ratio of parsed files/lines vs. actually represented ones */
  class VirtualFileAndLocCount extends ModularAggregationOperation[(Long, Long)] {
    val neutralElement = (0L,0L)
    def aggregate(a: (Long, Long), b: (Long, Long)): (Long, Long) = {
      (a._1 + b._1, a._2 + b._2)
    }
    def extract(x: Vertex[_, _, _, _]): (Long, Long) = {
      x match {
        case v: BaseVertex => { // TODO: This filter should be nicer or somewhere else
          if (v.state.rangeStates.values.exists(_ is "META-FILE") ||
              v.state.rangeStates.values.exists(_ is "Class")) {
            val counters = v.state.rangeStates.foldLeft(neutralElement) { case (acc, (range, state)) =>
              val multiplier = range.size
              val loc = state[FileMeta].loc
              (acc._1 + multiplier, acc._2 + multiplier * loc)
            }
            counters
          } else neutralElement
        }
        case _ => neutralElement
      }
    }
  }

  import scala.collection.SortedMap
  import org.github.jamm.MemoryMeter
  object TopKMemoryConsumers {
    // adapted for scala from https://stackoverflow.com/a/3758880
    def humanByteCount(bytes: Long, si: Boolean = false): String = {
      val unit = if (si) 1000 else 1024
      if (bytes < unit) { bytes + " B" } else {
        val exp = (Math.log(bytes) / Math.log(unit)).toInt
        val pre = (if (si) "kMGTPE" else "KMGTPE").charAt(exp-1) + (if (si) "" else "i")
        f"${bytes / Math.pow(unit, exp)}%.1f ${pre}%sB"
      }
    }
  }
  class TopKMemoryConsumers(k: Int) extends ModularAggregationOperation[SortedMap[Long,BaseVertex]] {
    implicit val Ord = implicitly[Ordering[Long]]
    val neutralElement = SortedMap.empty[Long,BaseVertex](Ord.reverse)
    def aggregate(a: SortedMap[Long,BaseVertex], b: SortedMap[Long,BaseVertex]): SortedMap[Long,BaseVertex] = {
      (a ++ b) take k
    }
    def extract(x: Vertex[_, _, _, _]): SortedMap[Long,BaseVertex] = {
      x match {
        case v: BaseVertex => {
          try {
            val meter = new MemoryMeter()
            val res = v.state.rangeStates.foldLeft(0L) { case (acc, (range, state)) =>
              acc + meter.measureDeep(state)
            }
            neutralElement + (res -> v)
          } catch {
            case e: Throwable => 
              println(e)
              neutralElement
          }
        }
        case _ => neutralElement
      }
    }
  }
  class TotalMemoryConsumption extends ModularAggregationOperation[Long] {
    val neutralElement = 0L
    def aggregate(a: Long, b: Long): Long = {
      a + b
    }
    def extract(x: Vertex[_, _, _, _]): Long = {
      x match {
        case v: BaseVertex => {
          try {
            val meter = new MemoryMeter()
            v.state.rangeStates.foldLeft(neutralElement) { case (acc, (range, state)) =>
              acc + meter.measureDeep(state)
            }
          } catch {
            case e: Throwable => 
              println(e)
              neutralElement
          }
        }
        case _ => neutralElement
      }
    }
  }
 
}

