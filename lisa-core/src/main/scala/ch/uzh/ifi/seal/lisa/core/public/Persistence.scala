package ch.uzh.ifi.seal.lisa.core.public

/** Trivial interface for Persistence classes */
abstract class Persistence() {
  /** persist the current computation data.
    *
    * @param c the LisaComputation to be persisted.
    * @see LisaComputation for available members
    * @see CSVPersistence for an example implementation
    */
  def persist(c: LisaComputation)
}

