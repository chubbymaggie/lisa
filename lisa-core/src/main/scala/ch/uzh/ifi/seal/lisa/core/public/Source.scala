package ch.uzh.ifi.seal.lisa.core.public

import java.nio.ByteBuffer
import scala.collection.immutable.SortedMap
import com.signalcollect.Graph
import ch.uzh.ifi.seal.lisa.core._

/** Implement this trait to use LISA with arbitrary data sources
  *
  * Data sources such as Git, SVN, simple files or database sources can be
  * integrated by implementing this trait. Note that to take advantage of
  * asynchonous parsing, LISA expects the agent to be able to load arbitrary
  * revisions of a file at any time, using the readFileRevision method.
  *
  * Any source-specific parameters should be expected in the constructor, like
  * which branch, directory or revisions to read from.
  *
  * SourceAgents are simple state machines. @see AgentState
  *
  * @param parsers a reference to the parsers for filtering and parsing
  * @see GitAgent for an example implementation.
  */
abstract class SourceAgent(val parsers: List[Parser])(implicit val uid: String) {
  /** fetch the project (e.g. clone) */
  def fetch()
  /** prepare the lists of files and revisions */
  def load()
  /** parse whatever should be parsed into the provided graph */
  def parse(graph: Graph[Any,Any])
  /** read the contents of a specific revision of a file
    * @see FileRevision */
  def readFileRevision(f: FileRevision): Option[ByteBuffer]
  /** get a map of revision numbers to revision metadata and files
    * @see Revision, @see FileRevision */
  def getRevisionFiles(): Option[SortedMap[Int,(Revision, Set[_ <: FileRevision])]]
  /** already implemented: get a map of revision numbers to revision metadata */
  final def getRevisions(): Option[SortedMap[Int, Revision]] = {
    Some(getRevisionFiles.get.map { case (i, r) => i -> r._1 })
  }
  /** delete temporary files (such as the cloned repository etc.) */
  def cleanup()
}

/** Change types concerning a file in a specific revision. For example, a file
  * could have been newly created, deleted, renamed or modified. The special
  * "Preexisting" type is used when the first selected revision is not equal to
  * the very initial revision stored in the repository. In that case, all files
  * that exist in the first selected revision, but were not actually created or
  * otherwise modified in that revision, are considered pre-existing.
  * @see FileRevision */
sealed abstract class ChangeType
// for files created before this revision
case object Preexisting extends ChangeType
// for files that did not exist in the previous revision
case object Created extends ChangeType
// for files that existed in the previous revision, but not anymore
case object Deleted extends ChangeType
// for files that were modified
case object Modified extends ChangeType
// for files that were renamed
case object Renamed extends ChangeType
// for files that were not modified (and can be ignored in parsing)
case object Unmodified extends ChangeType

/** Classes implementing FileRevision hold information on a specific revision
  * (uniquely identified, e.g. by a hash) of a specific file (uniquely
  * identified in the context of the source, e.g. by the path) and the change
  * type that the file underwent in that specific revision (@see ChangeType).
  * They may hold additional, source-specific information that the corresponding
  * agent requires to read the file revision. @see GitFileRevision for an
  * an example implementation.
  *
  * fileId: the identifier of the file (e.g. Demo.java)
  * revId: the revision identifier (e.g. a SHA or source-specific id)
  * uri: where to actually read the file from (e.g. rev9/src/Demo.java)
  */
trait FileRevision {
  def fileId: String
  def uri: String
  def revId: String
  def changeType: ChangeType
  override def toString(): String = s"[$fileId ($uri)] $revId: $changeType"
}

