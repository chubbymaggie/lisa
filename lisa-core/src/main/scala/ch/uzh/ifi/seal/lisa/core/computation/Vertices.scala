package ch.uzh.ifi.seal.lisa.core.computation

import scala.collection.immutable.SortedMap
import com.signalcollect._
import com.signalcollect.interfaces.ExistingVertexHandlerFactory
import com.signalcollect.interfaces.ExistingVertexHandler
import com.signalcollect.interfaces.UndeliverableSignalHandlerFactory
import com.signalcollect.interfaces.UndeliverableSignalHandler
import com.signalcollect.interfaces.EdgeAddedToNonExistentVertexHandlerFactory
import com.signalcollect.interfaces.EdgeAddedToNonExistentVertexHandler
import com.typesafe.scalalogging.LazyLogging

trait Domain {
  val mapping = Map[Symbol,Set[String]]()
  lazy val labels = mapping.values.toSeq.flatten.toSet
  def is(domain: Domain) = this == domain
  def lookup(typeLabel: String, candidates: Symbol*): Boolean = {
    candidates.exists { c =>
      mapping.get(c) match {
        case Some(labels) => labels.contains(typeLabel)
        case _ => c match {
          case 'file if typeLabel == "META-FILE" => true
          case _ => false
        }
      }
    }
  }
  def reflect(typeLabel: String, candidates: Symbol*): Option[String] = {
    candidates.foreach { c =>
      mapping.get(c) match {
        case Some(labels) => if (labels.contains(typeLabel)) return Some(typeLabel)
        case _ => c match {
          case 'file if typeLabel == "META-FILE" => return Some(typeLabel)
          case _ =>
        }
      }
    }
    return None
  }
}
object Meta extends Domain
object NoneDomain extends Domain
object AllDomain extends Domain

/** Container for limited info given to analyses' start/collectEdit/collect*/
case class VertexContext(id: String, implicit val domain: Domain, sourceId: Option[String] = None, range: String = "")

/** The basic abstract vertex which all concrete vertices are inheriting.
  *
  * All vertices have practically the same functionality, so subclasses inherit
  * most of their behavior from BaseVertex. BaseVertex provides the mechanisms
  * for applying analyses to particular ranges and performs tasks such as
  * intersecting analysis ranges with existing data ranges.
  *
  * @param id the id of the vertex (inherited from S/C)
  */ //TODO: revisionRange as a property is not really relevant and a bit confusing because it's only correct at time of creation. remove it?
abstract class BaseVertex(id: String, revisionRange: RevisionRange, 
                          val domain: Domain)
      extends DataFlowVertex[String,State](id, State(revisionRange)) with LazyLogging {

  type Signal = GraphSignal

  /** markedRange is the first range which is still a candidate for extension */
  var markedRange: Option[RevisionRange] = None
  var signalEnd = List[Option[Revision]]()

  def compressRanges(endOption: Option[Revision]) {

    // Case 1:
    // No End
    //  → Mark very first range
    if (endOption.isEmpty) {
      markedRange = Some(state.revisionRanges.head._2)
    }
    else {
      val end = endOption.get
      val next = state.revisionRanges.get(end.n + 1)

      // Case 2:
      // No marked range and no content at end + 1
      //  → No need to act
      if (markedRange.isEmpty && next.isEmpty) {
      }

      // Case 3:
      // No marked range, but content at end + 1
      //  → Mark range at end + 1
      else if (markedRange.isEmpty && next.nonEmpty) {
        markedRange = next
      }

      // Case 4:
      // Marked range and no content at end + 1
      //  → Extend marked range until end (if markedRange.end != end) and unmark
      else if (markedRange.nonEmpty && next.isEmpty) {
        if (markedRange.get.end.n != end.n) {
          val markedState = state.rangeStates.get(markedRange.get).get
          val extendedRange = markedRange.get.copy(end = end)
          state = state.copy(
            revisionRanges = (state.revisionRanges
              + (markedRange.get.start.n -> extendedRange)),
            rangeStates = (state.rangeStates
              - markedRange.get
              + (extendedRange -> markedState)
              )
          )
        }
        markedRange = None
      }
      // Case 5:
      // Marked range and content at end + 1 with equal content
      //  → Merge marked range and next range, mark the merged range
      else if (markedRange.nonEmpty && next.nonEmpty) {
        val markedState = state.rangeStates.get(markedRange.get).get
        val nextState = state.rangeStates.get(next.get).get
        if (markedState == nextState) {
          val mergedRange = markedRange.get.copy(end = end)
          state = state.copy(
            revisionRanges = (state.revisionRanges
              - next.get.start.n
              + (markedRange.get.start.n -> mergedRange)),
            rangeStates = (state.rangeStates
              - next.get
              - markedRange.get
              + (mergedRange -> markedState)
              )
          )
          markedRange = Some(mergedRange)
        }
        // Case 6:
        // Marked range and content at end + 1 with different content
        //  → Extend marked range until end (if markedRange.end != end) and mark range at end + 1
        else {
          if (markedRange.get.end.n != end.n) {
            val markedState = state.rangeStates.get(markedRange.get).get
            val extendedRange = markedRange.get.copy(end = end)
            state = state.copy(
              revisionRanges = (state.revisionRanges
                + (markedRange.get.start.n -> extendedRange)),
              rangeStates = (state.rangeStates
                - markedRange.get
                + (extendedRange -> markedState)
                )
            )
          }
          markedRange = next
        }
      } else {
        logger.error("Invalid range state (this is a bug)")
      }
    }
  }

  /** expose the vertex information (eventually used by S/C Console) */
  override def expose = (State.expose(state) + ("edges" -> outgoingEdges))

  /** are there ranges to be closed in related vertices? */
  def signalParsing = signalEnd.nonEmpty

  /** are there upward-flowing analyses to be sent? */
  var signalAnalysisUp = false

  /** are there downward-flowing analyses to be sent? */
  var signalAnalysisDown = false

  /** are there analyses to be sent along specific edge types? */
  var signalAnalysisInterObject = false

  /** are there multi-directional signals to be sent? */
  var signalAny = false

  /** signal only if there are analyses waiting to be transmitted */
  override def scoreSignal = if (signalParsing || signalAnalysisUp || signalAnalysisDown || signalAnalysisInterObject) 1 else 0

  // TODO: get rid of signalAny? Make Up and Down special cases of InterObject?
  def clearScore = {
    signalAnalysisUp = false
    signalAnalysisDown = false
    signalAny = false
    signalAnalysisInterObject = false
  }
  def updateScore = {
    signalAnalysisUp = state.rangeStates.values.exists(_.analysesUp.nonEmpty)
    signalAnalysisDown = state.rangeStates.values.exists(_.analysesDown.nonEmpty)
    signalAny = state.rangeStates.values.exists(_.analysesAny.nonEmpty)
    signalAnalysisInterObject = state.rangeStates.values.exists(_.analysesInterObject.nonEmpty)
  }
  def updateScore(r: RevisionRange) = {
    signalAnalysisUp = signalAnalysisUp || state.rangeStates.get(r).map(_.analysesUp.nonEmpty).getOrElse(false)
    signalAnalysisDown = signalAnalysisDown || state.rangeStates.get(r).map(_.analysesDown.nonEmpty).getOrElse(false)
    signalAny = signalAny || state.rangeStates.get(r).map(_.analysesAny.nonEmpty).getOrElse(false)
    signalAnalysisInterObject = signalAnalysisInterObject || state.rangeStates.get(r).map(_.analysesInterObject.nonEmpty).getOrElse(false)
  }

  override def doSignal(graphEditor: GraphEditor[Any, Any]) {
    outgoingEdges.values.foreach {
      case edge: UpwardEdge if signalAnalysisUp => edge.executeSignalOperation(this, graphEditor)
      case edge: DownwardEdge if (signalParsing || signalAnalysisDown) => edge.executeSignalOperation(this, graphEditor)
      case edge: ObjectEdge if signalParsing => edge.executeSignalOperation(this, graphEditor)
      case edge: InterObjectEdge if signalAnalysisInterObject => edge.executeSignalOperation(this, graphEditor)
      case _ =>
    }
  }

  /** Run the collectEdit function on all analyses transmitted with the signal
    *
    * The vertex state is not changed during this step, but analyses may inspect
    * the present state and modify the graph.
    *
    * @see Signal/Collect documentation on deliverSignal
    * @see Analysis.collectEdit for more information on the semantics of collectEdit
    * @see State.intersectRange, State.makeContiguous and State.defragmentRanges
    * @see applyAnalyses for how analysis ranges and local ranges are processed
    */
  override def deliverSignalWithSourceId(signal: Any, sourceId: Any,
                             graphEditor: GraphEditor[Any, Any]): Boolean = {
    signal match {
      case analysisSignal: AnalysisSignal =>
        def f(a: AnalysisPacket, r: RevisionRange, s: AnalysisState, c: VertexContext) {
          val editor = GraphEditorWrapper(graphEditor, id)
          state = state.copy(rangeStates =
            state.rangeStates + (r -> a.collectEdit(c.domain)(s))
          )
        }
        applyAnalyses(signal, f)
        flushGraphEdits(graphEditor)
      case _ =>
    }
    super.deliverSignalWithSourceId(signal, sourceId, graphEditor)
  }

  /** Run the collect function on all analyses contained in this signal
    *
    * The typical use of collect is changing the state of the local vertex as
    * well as sending out new analyses along the edges.
    *
    * @see Signal/Collect documentation on collect
    * @see Analysis.collect for more information on the semantics of collect
    * @see State.intersectRange, State.makeContiguous and State.defragmentRanges
    * @see applyAnalyses for how analysis ranges and local ranges are processed
    */
  def collect(signal: Signal): State = {
    signal match {
      case analysisSignal: AnalysisSignal =>
        def f(a: AnalysisPacket, r: RevisionRange, s: AnalysisState, c: VertexContext) {
          state = state.copy(rangeStates = {
            state.rangeStates + (r -> a.collect(c.domain)(s))
          }
          )
        }
        applyAnalyses(signal, f)
      case endRangeSignal: EndRangeSignal =>
        signalEnd = signalEnd ++ endRangeSignal.end
        endRangeSignal.end.foreach { end =>
          compressRanges(end)
        }
      case _ =>
    }
    state
  }

  /** Empty the analysis list once all edges have signalled.
    *
    * The edge takes a copy of the analysis list, hence the local list is
    * cleared after every signal step. The references to analyses live on inside
    * the signal.
    *
    * @see Signal/Collect documentation on executeSignalOperation
    */
  override def executeSignalOperation(graphEditor: GraphEditor[Any, Any]) {
    edgesModifiedSinceSignalOperation = false
    doSignal(graphEditor)
    state = state.copy(rangeStates = state.rangeStates.mapValues { it =>
      it.copy(analysesUp = List(), analysesDown = List(), analysesAny = List(), analysesInterObject = List())
    })
    clearScore
    signalEnd = List[Option[Revision]]()
  }

  /** apply the analyses from the signal to the current vertex in some fashion
    *
    * This is common code for deliverSignal and collect. It enables the vertex
    * to process analyses transmitted via incoming signals, which may or may not
    * apply to existing ranges. If an analysis range does not exist, existing
    * ranges may need to be split or possibly created from previously empty
    * range space. After intersecting the ranges, the analyses are applied to
    * one or more affected ranges using the given function f.
    *
    * @param signal the incoming signal
    * @param f a function that does something with a particular range state
    */
  def applyAnalyses(signal: Any, f: (AnalysisPacket, RevisionRange, AnalysisState, VertexContext) => Unit) {
    signal match {
      case analysisSignal: AnalysisSignal =>
        analysisSignal.rangeAnalyses.foreach {
          case (analysisRange: RevisionRange, analyses: List[AnalysisPacket]) =>
            val affectedRanges = if (state.rangeStates.contains(analysisRange)) {
              SortedMap((analysisRange.start.n -> analysisRange))
            }
            else {
              val (updatedState, rs) = State.intersectRange(analysisRange, state)
              state = updatedState
              rs
            }
            affectedRanges.foreach { case (start: Int, revisionRange: RevisionRange) =>
              analyses.foreach { a =>
                val analysisState = state.rangeStates(revisionRange)
                val context = VertexContext(id, domain, Some(analysisSignal.sourceVertexId), revisionRange.toString)
                f(a, revisionRange, analysisState, context)
              }
              updateScore(revisionRange)
            }
        }
      case _ =>
    }
  }

  /** apply any edits of the graph performed by analyses
    *
    * Edits performed by analyses are not applied immediately to the graph but
    * are formulated as vertex/edge additions or removals. After the analyses
    * are done for a vertex, the edits are applied all at once.
    */
  def flushGraphEdits(graph: GraphEditor[Any, Any]) {
    state = state.copy(rangeStates =
      state.rangeStates.map { case (r, s) =>
        s.edits.foreach {
          case VertexAddition(id, domain) => {
            val vertex = new MetaVertex(id, r, domain)
            graph.addVertex(vertex)
          }
          case VertexDeletion(id) => graph.removeVertex(id)
          case _ =>
        }
        s.edits.foreach {
          case EdgeAddition(src, edge) => {
            try {
              graph.addEdge(src, edge)
            }
            catch {
              case e: Exception => logger.error(s"Could not add edge $edge (do the vertices involved exist?)")
            }
          }
          case _ =>
        }
        (r -> s.copy(edits = List()))
      }
    )
  }

}

class MergeRangeVertexHandlerFactory[Id, Signal] extends ExistingVertexHandlerFactory[Id, Signal] {
  def createInstance: ExistingVertexHandler[Id, Signal] =
  new MergeRangeVertexHandler[Id, Signal]
  override def toString = "MergeRangeVertexHandlerFactory"
}

class MergeRangeVertexHandler[Id, Signal] extends ExistingVertexHandler[Id, Signal] {
  /** call the appropriate existingVertexHandler for the given vertex type */
  def mergeVertices(_existingVertex: Vertex[Id, _, Id, Signal],
                    _newVertex: Vertex[Id, _, Id, Signal],
                    graphEditor:GraphEditor[Id, Signal]): Unit = {
    (_existingVertex, _newVertex) match {
      case (existingVertex: AstVertex, newVertex: AstVertex) =>
        existingVertex.existingVertexHandler(existingVertex, newVertex, graphEditor)
      case (existingVertex: ObjectVertex, newVertex: ObjectVertex) =>
        existingVertex.existingVertexHandler(existingVertex, newVertex, graphEditor)
      case (existingVertex: MetaVertex, newVertex: MetaVertex) =>
        existingVertex.existingVertexHandler(existingVertex, newVertex, graphEditor)
      case _ =>
    }
  }
}

class IgnoringUndeliverableSignalHandlerFactory[Id, Signal] extends UndeliverableSignalHandlerFactory[Id, Signal] {
  def createInstance: UndeliverableSignalHandler[Id, Signal] =
  new IgnoringUndeliverableSignalHandler[Id, Signal]
  override def toString = "IgnoringUndeliverableSignalHandlerFactory"
}

class IgnoringUndeliverableSignalHandler[Id, Signal] extends UndeliverableSignalHandler[Id, Signal] {
  def vertexForSignalNotFound(s: Signal, inexistentTargetId: Id, senderId: Option[Id], ge: GraphEditor[Id, Signal]) { }
}

class IgnoringEdgeAddedToNonExistentVertexHandlerFactory[Id, Signal] extends EdgeAddedToNonExistentVertexHandlerFactory[Id, Signal] {
  def createInstance: EdgeAddedToNonExistentVertexHandler[Id, Signal] =
  new IgnoringEdgeAddedToNonExistentVertexHandler[Id, Signal]
  override def toString = "IgnoringEdgeAddedToNonExistentVertexHandlerFactory"
}


class IgnoringEdgeAddedToNonExistentVertexHandler[Id, Signal] extends EdgeAddedToNonExistentVertexHandler[Id, Signal] {
    def handleImpossibleEdgeAddition(edge: Edge[Id], vertexId: Id, graphEditor: GraphEditor[Id, Signal]): Option[Vertex[Id, _, Id, Signal]] = {
      None
    }
}

/** Routing of calls to existingVertexHandler depending on subclass type */
object BaseVertex { }


trait IntersectingVertex {
  /* Classes using IntersectingVertex can be added to the graph in arbitrary
   * order. Ranges of existing and new vertices are always merged correctly.
   * This is slower than using SequentialVertex.
   *
   * A MetaVertex can be created for any revision range and this means that the
   * range of the new vertex may or may not overlap existing ranges in any kind
   * of fashion. For this reason, it is necessary both to intersect the new
   * range with the existing ones and to make sure that the ranges within the
   * new range are contiguous (no gaps between ranges).
   */
  def existingVertexHandler(existingVertex: BaseVertex,
                            newVertex: BaseVertex,
                            graphEditor:GraphEditor[_, _]): Unit = {
    val newRange = newVertex.state.revisionRanges.head._2
    // intersect the new range into the old ones - this only covers cases where
    // the new range's start or end are within an existing range, which has to
    // be split
    val (intersectedState, rs) = State.intersectRange(newRange, existingVertex.state)
    // it is also necessary to check wether the existing ranges within the new
    // ranges's start and end are contiguous. If there are gaps, they are
    // initialized
    existingVertex.state = State.makeContiguous(newRange, intersectedState)
  }
}

/* Classes using SequentialVertex MUST be added to the graph in sequential
 * (e.g. chronological) order for range calculations to be correct. For
 * vertices added out of order, use IntersectingVertex instead!
 */
trait SequentialVertex {
  /** instead of adding a new vertex, create a new range within the an vertex */
  def existingVertexHandler(existingVertex: BaseVertex,
                            newVertex: BaseVertex,
                            graphEditor:GraphEditor[_, _]): Unit = {
    val newState = newVertex.state
    val newRange = newState.revisionRanges.head._2
    assert(newRange.start == newRange.end)
    val newAnalysisState = newState.rangeStates.get(newRange).get
    val existingState = existingVertex.state
    existingVertex.state = existingState.copy(
      revisionRanges = (existingState.revisionRanges
        + (newRange.start.n -> newRange)),
      rangeStates = (existingState.rangeStates
        + (newRange -> newAnalysisState))
    )
  }
}

/** A vertex storing meta data of some kind (not belonging to the AST) */
class MetaVertex(id: String, revisionRange: RevisionRange, domain: Domain)
      extends BaseVertex(id, revisionRange, domain) with IntersectingVertex

/** A vertex representing an AST vertex */
class AstVertex(id: String, val index: Int, revisionRange: RevisionRange,
                val fileName: String, domain: Domain)
      extends BaseVertex(id, revisionRange, domain) with SequentialVertex

/** A vertex representing a class object model vertex */
class ObjectVertex(val kind: String, id: String, val index: Int, revisionRange: RevisionRange,
                   val fileName: String, domain: Domain)
      extends BaseVertex(id, revisionRange, domain) with SequentialVertex

