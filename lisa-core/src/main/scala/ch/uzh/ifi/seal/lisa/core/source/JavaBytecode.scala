package ch.uzh.ifi.seal.lisa.core.source

import java.io.File
import java.nio.ByteBuffer
import java.io.FileInputStream
import java.io.DataInputStream
import com.typesafe.scalalogging.LazyLogging
import scala.collection.immutable.SortedMap
import com.signalcollect.Graph
import spray.json._
import org.objectweb.asm.ClassReader

import ch.uzh.ifi.seal.lisa.core.computation._
import ch.uzh.ifi.seal.lisa.core.misc._
import ch.uzh.ifi.seal.lisa.core.public.Parser
import ch.uzh.ifi.seal.lisa.core.public.Deleted
import ch.uzh.ifi.seal.lisa.core.public.Created
import ch.uzh.ifi.seal.lisa.core.public.Modified
import ch.uzh.ifi.seal.lisa.core.public.Unmodified


case class JavaBytecodeFileRevision (fileId: String, uri: String, revId: String,
                                     changeType: ChangeType)
                                     extends FileRevision

/** SourceAgent implementation for Java class files; supports async parsing
  * @param parsers a list of selected parsers
  * @param path to the directory containing the revision folders
  * @see SourceAgent
  * @see AsyncAgent
  */
class JavaBytecodeAgent(parsers: List[Parser], path: String)
              (implicit override val uid: String)
               extends SourceAgent(parsers)(uid) with AsyncAgent with LazyLogging with RuntimeStats {

   private def getFileTree(f: File): Stream[File] = {
     f #:: (if (f.isDirectory) { f.listFiles().toStream.flatMap(getFileTree) }
         else Stream.empty)
   }

  var revisions: Option[SortedMap[Int, (Revision, Set[JavaBytecodeFileRevision])]] = None

  private def getString(f: String)(implicit fields: Map[String, JsValue]): String = {
    fields.get(f).get.asInstanceOf[JsString].value
  }

  def fetch() = {}
  def load() = {

    // find revisions
    val revDirs = new File(path).listFiles.map(_.toString).sorted
    val revCount = revDirs.size

    // extract class names and paths for all class files in all revisions
    var previousRevision: Option[Revision] = None
    val revisionData = revDirs.zipWithIndex.foldLeft(SortedMap[Int, (Revision, Set[JavaBytecodeFileRevision])]()) { case (acc, (dir, i)) =>
      val sha = dir.split("_")(1)
      var fileCount: Long = 0
      // load revision metadata
      val metaJson = scala.io.Source.fromFile(s"${dir}/git-metadata.txt").mkString
      implicit val meta = metaJson.parseJson.asInstanceOf[JsObject].fields
      import java.util.Date
      import java.util.TimeZone
      import java.time.ZonedDateTime
      import java.time.ZoneOffset
      val tz = TimeZone.getTimeZone("UTC")
      val authorDate = Date.from(ZonedDateTime.parse(getString("authorDate"))
                                              .withZoneSameInstant(ZoneOffset.UTC).toInstant())
      val committerDate = Date.from(ZonedDateTime.parse(getString("committerDate"))
                                              .withZoneSameInstant(ZoneOffset.UTC).toInstant())
      val revision = Revision(i, sha, authorDate, tz, getString("authorName"), getString("authorEmail"),
                                      committerDate, tz, getString("committerName"), getString("committerEmail"),
                                      previousRevision, None,
                                      subject = getString("subject"), message = getString("message"))
      previousRevision match {
        case Some(r) => r.next = Some(revision)
        case None =>
      }
      previousRevision = Some(revision)

      // find class files (naively filtering test classes)
      // TODO: add customization options to class file finder (also for jars)
      val Pattern = ".*/[a-zA-Z0-9]*\\$[^\\/]*\\.class$".r
      val classFiles = getFileTree(new File(dir)).filter { fname =>
        val n = fname.getPath
        (n.endsWith(".class")
         && !n.contains("/Test")
         && !n.contains("/test/")
         && !n.endsWith("package-info.class")
         && Pattern.findFirstIn(n).isEmpty)
      }.toList

      /*
      val jarFiles = getFileTree(new File(dir)).filter(_.getName.endsWith(".jar")).toList
      import java.util.zip.ZipFile
      import java.util.zip.ZipEntry
      import scala.collection.JavaConverters._
      val jarredClassFiles = jarFiles.foldLeft(List[(ZipFile, ZipEntry)]()) { case (acc, jar) =>
        val zip = new ZipFile(jar)
        zip.entries.asScala.foldLeft(acc) { case (acc, entry) =>
          if (entry.getName.endsWith(".class")) { (zip, entry) :: acc } else acc
        }
      }
      */

      // construct file revision data
      val fileRevisions = classFiles.map { f =>
        val is = new FileInputStream(f.getPath)
        // TODO: would be nice if class would not be read twice (here and later in JavaASMParser)
        val cr = new ClassReader(is)
        val fileId = cr.getClassName()
        is.close()
        JavaBytecodeFileRevision(fileId, f.getPath, sha, Created)
      }.toSet
      acc + (i -> (revision, fileRevisions))
    }

    // detect duplicate classes
    // TODO?

    // detect deleted classes
    .foldLeft(SortedMap[Int, (Revision, Set[JavaBytecodeFileRevision])]()) { case (acc, (i, (rev, files))) =>
      if (acc.isEmpty) { acc + (i -> (rev, files)) }
      else {
        val currentClasses = files.foldLeft(Map[String,String]()) { case (acc, f) => acc + (f.fileId -> f.uri) }
        acc + (i -> (rev, acc.last._2._2.foldLeft(files) { case (acc, f) =>
          if (f.changeType != Deleted && !currentClasses.contains(f.fileId)) {
            acc + f.copy(changeType = Deleted)
          } else acc
        }))
      }
    }

    // detect modified and unmodified classes
    .foldLeft(SortedMap[Int, (Revision, Set[JavaBytecodeFileRevision])]()) { case (acc, (i, (rev, files))) =>
      if (acc.isEmpty) { acc + (i -> (rev, files)) }
      else {
        val prevClasses = acc.last._2._2.foldLeft(Map[String,String]()) { case (acc, f) => 
          // don't consider files deleted in the previous revision
          if (f.changeType != Deleted) acc + (f.fileId -> f.uri) else acc
        }
        acc + (i -> (rev, files.foldLeft(Set[JavaBytecodeFileRevision]()) { case (acc, f) =>
          // keep files deleted in this revision as deleted
          if (f.changeType == Deleted) { acc + f }
          else if (prevClasses.contains(f.fileId)) {
            // prune non-modified class
            if (FileTools.contentEquals(f.uri, prevClasses.get(f.fileId).get)) {
              acc + f.copy(changeType = Unmodified)
            }
            // or adjust the changeType if the class was modified
            else acc + f.copy(changeType = Modified)
          } else acc + f // otherwise it's just a newly created file
        }))
      }
    }

    // prune unmodified classes
    .foldLeft(SortedMap[Int, (Revision, Set[JavaBytecodeFileRevision])]()) { case (acc, (i, (rev, files))) =>
      acc + (i -> (rev, files.foldLeft(Set[JavaBytecodeFileRevision]()) { case (acc, f) =>
        if (f.changeType != Unmodified) { acc + f } else acc
      }))
    }

    val fileCount = revisionData.foldLeft(0) { case (acc, (i, (rev, files))) =>
      acc + files.count { f => !List(Deleted, Unmodified).contains(f.changeType) }
    }
    stats += ("source_file_count" -> fileCount.asInstanceOf[Long])

    revisions = Some(revisionData)
  }
  def parse(graph: Graph[Any,Any]) = {
    parseAsync(graph, this)
    graph.foreachVertex( {
      case v: ObjectVertex => parsers.foreach(_.postprocess(graph, v))
      case _ =>
    } )
  }
  def readFileRevision(f: FileRevision): Option[ByteBuffer] = {
    f match {
      case gf: JavaBytecodeFileRevision =>
        val is = new FileInputStream(f.uri)
        val size = is.available()
        val buff = new Array[Byte](size)
        val in = new DataInputStream(is)
        in.readFully(buff)
        in.close()
        is.close()
        Some(ByteBuffer.wrap(buff))
      case _ => None
    }
  }
  def getRevisionFiles() = {
    revisions
  }
  def cleanup() = {}
}


