package ch.uzh.ifi.seal.lisa.core

package object computation {
  type Mapping = Map[Domain, List[String]]
  type DataMap = scala.collection.immutable.HashMap[Int, Product]
  val DataMap = new DataMap
  type Analysis = ch.uzh.ifi.seal.lisa.core.public.Analysis
  type AnalysisPacket = ch.uzh.ifi.seal.lisa.core.public.AnalysisPacket
  type SourceAgent = ch.uzh.ifi.seal.lisa.core.public.SourceAgent
  type FileRevision = ch.uzh.ifi.seal.lisa.core.public.FileRevision
  type ChangeType = ch.uzh.ifi.seal.lisa.core.public.ChangeType
}
