package ch.uzh.ifi.seal.lisa.core.public

import ch.uzh.ifi.seal.lisa.core.computation._
import ch.uzh.ifi.seal.lisa.core.misc.RuntimeStats

/** The main instantiation point for running computations in LISA
  *
  * @param sources a SourceAgent instance providing access to files and metadata
  * @param analyses the AnalysisSuite to be executed
  * @param persistence a Persistence instance used to store analysis results
  * @param console boolean to enable the Signal/Collect console
  */
class LisaComputation(val sources: SourceAgent, analyses: AnalysisSuite,
                      persistence: Persistence, console: Boolean = false,
                      periodicStats: Boolean = false)
                     (implicit val uid: String) extends RuntimeStats {
  clearStats()
  RuntimeStats.periodicStats = periodicStats

  val computation = new Computation(analyses, console)

  // possible operations, could be called individually
  def fetch = sources.fetch
  def load = sources.load
  def parse = sources.parse(computation.graph)
  def compute = computation.run
  def persist = persistence.persist(this)
  def shutdown = computation.shutdown
  def cleanup = sources.cleanup

  // executing a computation executes the sequence of operations
  def execute = {
    fetch
    load
    parse
    statCompressionRatios("ante_computation")
    compute
    statCompressionRatios("post_computation")
    persist
    shutdown
    cleanup
  }

  // provide direct access to the Signal/Collect graph (use with caution)
  def graph = computation.graph

  def statCompressionRatios(suffix: String): (Double, Double, Double) = {
    val (vertexCount, rangeCount, revisionCount, vertexRatio) = computation.computeVertexCompressionRatio
    stats += (s"compression_vertices_${suffix}" -> vertexRatio)
    stats += (s"vertex_count_${suffix}" -> vertexCount)
    stats += (s"range_count_${suffix}" -> rangeCount)
    stats += (s"range_count_uncompressed_${suffix}" -> revisionCount)

    val (fileRatio, lineRatio, fileCount, lineCount) = computation.computeParseCompressionRatios
    stats += (s"compression_parsed_files" -> fileRatio)
    stats += (s"compression_parsed_lines" -> lineRatio)
    stats += (s"represented_files" -> fileCount)
    stats += (s"represented_lines" -> lineCount)

    (vertexRatio, fileRatio, lineRatio)
  }

}

